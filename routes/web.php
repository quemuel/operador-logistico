<?php

use App\Http\Controllers\DadosImportacaoController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EscalaController;
use App\Http\Controllers\FaturamentoController;
use App\Http\Controllers\FinanceiroController;
use App\Http\Controllers\MigrationController;
use App\Http\Controllers\PeriodoEscalaController;
use App\Http\Controllers\VagasController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('/home', [HomeController::class, 'home'])->name('home');
Route::get('/admin/home', [HomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');

Route::get('/realizar-cadastro', [EscalaController::class, 'index'])->name('realizar-cadastro');

Route::get('dados-importacao', [DadosImportacaoController::class, 'index']);
Route::get('importar-arquivo-financeiro', [DadosImportacaoController::class, 'importarArquivoFinanceiro'])->name('dados-importacao.importar-arquivo-financeiro')->middleware('auth');
Route::post('dados-importacao', [DadosImportacaoController::class, 'importarArquivo'])->name('importar-arquivo');
Route::post('store-importar-arquivo-financeiro', [DadosImportacaoController::class, 'storeImportarArquivoFinanceiro'])->name('store-importar-arquivo-financeiro')->middleware('auth');

Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard')->middleware('auth');
Route::get('/dashboard/dados', [DashboardController::class, 'dados'])->name('dashboard.dados')->middleware('auth');

Route::get('financeiro/dados', [FinanceiroController::class, 'dados'])->name('financeiro.dados')->middleware('auth');
Route::resource('financeiro', FinanceiroController::class);
Route::get('/financeiro/{id}', [FinanceiroController::class, 'show'])->name('financeiro.show');

Route::get('faturamento/dados', [FaturamentoController::class, 'dados'])->name('faturamento.dados')->middleware('auth');
Route::resource('faturamento', FaturamentoController::class)->middleware('auth');
Route::get('/faturamento/{id}', [FaturamentoController::class, 'show'])->name('faturamento.show');
Route::get('/select2-dados-ajax', [FaturamentoController::class, 'select2DadosAjax'])->name('faturamento.select2-dados-ajax')->middleware('auth');
Route::post('/associar-entregador', [FaturamentoController::class, 'associarEntregador'])->name('faturamento.associar-entregador')->middleware('auth');

Route::get('/migration/v1', [MigrationController::class, 'v1']);

Route::get('/agendar', [EscalaController::class, 'agendar'])->name('escala.agendar');
Route::get('/agendar/{codigo}/{areaEntregaIdGuid}', [EscalaController::class, 'agendar']);
Route::get('/agendar/{codigo}/{areaEntregaIdGuid}/{cpf}', [EscalaController::class, 'agendar']);
Route::post('/agendar', [EscalaController::class, 'storeAgendar'])->name('escala.store-agendar');
Route::post('/atualizar-agendamento', [EscalaController::class, 'storeAtualizarAgendamento'])->name('escala.store-atualizar-agendamento');

/*Route::get('/escala/cadastrar-salvador-regular', [EscalaController::class, 'cadastrarSalvadorRegular'])->name('escala.cadastrar-salvador-regular');
Route::get('/escala/cadastrar-salvador-regular/{codigo}', [EscalaController::class, 'cadastrarSalvadorRegular']);
Route::get('/escala/cadastrar-salvador-regular/{codigo}/{cpf}', [EscalaController::class, 'cadastrarSalvadorRegular']);
Route::post('/escala/store-cadastrar-salvador-regular', [EscalaController::class, 'storeCadastrarSalvadorRegular'])->name('escala.store-cadastrar-salvador-regular');
Route::post('/escala/store-atualizar-salvador-regular', [EscalaController::class, 'storeAtualizarSalvadorRegular'])->name('escala.store-atualizar-salvador-regular');
*/
Route::get('/escala', [EscalaController::class, 'index'])->middleware('auth');
Route::get('/escala/por-entregador', [EscalaController::class, 'porEntregador'])->name('escala.por-entregador')->middleware('auth');
Route::get('/escala/por-area', [EscalaController::class, 'porArea'])->name('escala.por-area')->middleware('auth');
Route::get('/escala/dados', [EscalaController::class, 'dados'])->name('escala.dados')->middleware('auth');
Route::get('/escala/dados-por-area', [EscalaController::class, 'dadosPorArea'])->name('escala.dados-por-area')->middleware('auth');

Route::get('/escala/{escalaId}', [EscalaController::class, 'visualizar'])->name('escala.visualizar')->middleware('auth');
Route::get('/escala/{pracaId}/{periodoEscalaId}/{areaEntregaId}/{data?}', [EscalaController::class, 'visualizarPorArea'])->name('escala.visualizar-por-area')->middleware('auth');
Route::get('/escala/editar/{escala}', [EscalaController::class, 'editar'])->name('escala.editar')->middleware('auth');
Route::put('/escala/{escala}', [EscalaController::class, 'atualizar'])->name('escala.atualizar')->middleware('auth');
Route::delete('/escala/{escalaId}', [EscalaController::class, 'excluir'])->name('escala.excluir')->middleware('auth');

Route::get('/periodo-escala', [PeriodoEscalaController::class, 'index'])->name('periodoEscala.index')->middleware('auth');
Route::get('/periodo-escala/cadastrar', [PeriodoEscalaController::class, 'cadastrar'])->name('periodoEscala.cadastrar')->middleware('auth');
Route::post('/periodo-escala', [PeriodoEscalaController::class, 'guardar'])->name('periodoEscala.guardar');
Route::get('/periodo-escala/{periodoEscala}/editar', [PeriodoEscalaController::class, 'editar'])->name('periodoEscala.editar');
Route::put('/periodo-escala/{periodoEscala}', [PeriodoEscalaController::class, 'atualizar'])->name('periodoEscala.atualizar')->middleware('auth');
Route::get('/periodo-escala/dados', [PeriodoEscalaController::class, 'dados'])->name('periodoEscala.dados')->middleware('auth');
Route::delete('/periodo-escala/{periodoEscala}', [PeriodoEscalaController::class, 'excluir'])->name('periodoEscala.excluir')->middleware('auth');
Route::get('/periodo-escala/{periodoEscala}', [PeriodoEscalaController::class, 'visualizar'])->name('periodoEscala.visualizar')->middleware('auth');

Route::get('/vagas/geral/{periodoEscalaId?}/{data?}', [VagasController::class, 'geral'])->name('vagas.geral')->middleware('auth');
Route::get('/vagas/listagem/{periodoEscalaId?}', [VagasController::class, 'listagem'])->name('vagas.listagem')->middleware('auth');
Route::get('/vagas/editar/{periodoEscalaId}/{areaEntregaId}', [VagasController::class, 'editar'])->name('vagas.editar')->middleware('auth');
Route::put('/vagas/{periodoEscalaId}/{areaEntregaId}', [VagasController::class, 'atualizar'])->name('vagas.atualizar')->middleware('auth');
Route::get('/vagas/dados/{periodoEscalaId?}', [VagasController::class, 'dados'])->name('vagas.dados')->middleware('auth');

/*
GET           /users/{user}               show    users.show

Verb          Path                        Action  Route Name
GET           /users                      index   users.index
GET           /users/create               create  users.create
POST          /users                      store   users.store
GET           /users/{user}/edit          edit    users.edit
PUT|PATCH     /users/{user}               update  users.update
DELETE        /users/{user}               destroy users.destroy
*/
/*
$routeView = route('escala.visualizar', $row['id']);
$routeEdit = route('escala.atualizar', $row['id']);
$routeDelete = route('escala.excluir', $row['id']);
*/
