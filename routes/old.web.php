<?php

use App\Http\Controllers\DadosImportacaoController;
use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EscalaController;
use App\Http\Controllers\FinanceiroController;
use App\Http\Controllers\MigrationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('/realizar-cadastro', [EscalaController::class, 'index'])->name('realizar-cadastro');
//Route::get('/home', 'HomeController@index')->name('home');


Route::get('dados-importacao', [DadosImportacaoController::class, 'index']);
Route::post('dados-importacao', [DadosImportacaoController::class, 'importarArquivo'])->name('importar-arquivo');

Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::get('dashboard/dados', [DashboardController::class, 'dados'])->name('dashboard.dados');

Route::get('financeiro/dados', [FinanceiroController::class, 'dados'])->name('financeiro.dados');
Route::resource('financeiro', FinanceiroController::class);
Route::get('/financeiro/{id}', [FinanceiroController::class, 'show'])->name('financeiro.show');

Route::get('/migration/v1', [MigrationController::class, 'v1']);