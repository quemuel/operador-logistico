<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTurnoVagasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turno_vagas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('periodo_escala_id');
            $table->foreign('periodo_escala_id')
                    ->references('id')
                    ->on('periodo_escala');
            $table->unsignedInteger('turno_id');
            $table->foreign('turno_id')
                    ->references('id')
                    ->on('turno');
            $table->unsignedInteger('dia_id');
            $table->foreign('dia_id')
                    ->references('id')
                    ->on('dia');
            $table->integer('qtd_vagas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('turno_vagas');
    }
}
