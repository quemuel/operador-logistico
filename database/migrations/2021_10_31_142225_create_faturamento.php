<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaturamento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faturamento', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('data_turno')->index();
            $table->string('turno', 100);
            $table->string('praca', 100)->index();
            $table->string('entregador', 500)->nullable()->index();
            $table->string('tipo', 100);
            $table->decimal('valor', 18, 2);
            $table->string('descricao', 800)->nullable();
            $table->unsignedInteger('dados_bancario_id')->nullable();
            $table->foreign('dados_bancario_id')
                    ->references('id')
                    ->on('dados_bancario');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faturamento');
    }
}
