<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RecreateTurnoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('turno');
        Schema::create('turno', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descricao', 200);
            $table->unsignedInteger('praca_id');
            $table->foreign('praca_id')
                    ->references('id')
                    ->on('praca');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('turno');
    }
}
