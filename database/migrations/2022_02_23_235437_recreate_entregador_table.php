<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RecreateEntregadorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('entregador');
        Schema::create('entregador', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome_completo', 200);
            $table->string('cpf', 11)->unique();
            $table->string('email', 150)->unique()->nullable();
            $table->string('telefone', 50)->nullable();
            $table->unsignedInteger('praca_id')->index();
            $table->foreign('praca_id')
                    ->references('id')
                    ->on('praca');
            $table->boolean('ativo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entregador');
    }
}
