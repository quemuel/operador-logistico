<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTurnoVagas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('turno_vagas', function (Blueprint $table) {
            $table->unsignedInteger('area_entrega_id')->nullable()
                ->after('periodo_escala_id');
            $table->foreign('area_entrega_id')
                    ->references('id')
                    ->on('area_entrega');
            $table->dropForeign(['dia_id']);
            $table->dropColumn('dia_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('turno_vagas', function (Blueprint $table) {
            $table->dropForeign(['area_entrega_id']);
            $table->dropColumn('area_entrega_id');
            /*$table->unsignedInteger('dia_id');
            $table->foreign('dia_id')
                    ->references('id')
                    ->on('dia');*/

            /*
            $table->dropForeign('turno_vagas_area_entrega_id_foreign');
            $table->dropColumn('area_entrega_id');
            */
        });
    }
}
