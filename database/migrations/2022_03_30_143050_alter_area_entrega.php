<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterAreaEntrega extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('area_entrega', function (Blueprint $table) {
            $table->boolean('is_dedicado') // Nome da coluna
                ->after('modalidade'); // Ordenado após a coluna "modalidade"
            $table->string('codigo', 200);
            $table->boolean('ativo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('area_entrega', function (Blueprint $table) {
            $table->dropColumn('is_dedicado');
            $table->dropColumn('codigo');
            //$table->dropColumn('ativo');
        });
    }
}
