<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEscalaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('escala', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('entregador_id');
            $table->foreign('entregador_id')
                    ->references('id')
                    ->on('entregador');
            $table->unsignedInteger('periodo_escala_id');
            $table->foreign('periodo_escala_id')
                    ->references('id')
                    ->on('periodo_escala');
            $table->unsignedInteger('area_entrega_id');
            $table->foreign('area_entrega_id')
                    ->references('id')
                    ->on('area_entrega');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('escala');
    }
}
