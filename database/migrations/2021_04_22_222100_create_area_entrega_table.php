<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAreaEntregaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Schema::dropIfExists('area_entrega');
        Schema::create('area_entrega', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descricao', 100);
            $table->foreignId('praca_id')->index();
            $table->string('modalidade', 100)->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('area_entrega');
    }
}
