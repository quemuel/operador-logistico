<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEscalaTurnoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('escala_turno', function (Blueprint $table) {
            $table->increments('id');
            $table->morphs('et_morph');
            $table->unsignedInteger('turno_id');
            $table->foreign('turno_id')
                    ->references('id')
                    ->on('turno');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('escala_turno');
    }
}
