<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableTurnoVagas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('turno_vagas', function (Blueprint $table) {
            $table->unsignedInteger('dia_id')
                ->nullable()
                ->after('turno_id');
            $table->foreign('dia_id')
                    ->references('id')
                    ->on('dia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('turno_vagas', function (Blueprint $table) {
            $table->dropForeign(['dia_id']);
            $table->dropColumn('dia_id');
        });
    }
}
