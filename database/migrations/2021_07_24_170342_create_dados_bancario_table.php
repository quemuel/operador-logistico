<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDadosBancarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dados_bancario', function (Blueprint $table) {
            $table->increments('id');
            $table->string('municipio', 100);
            $table->string('nome', 500);
            $table->string('cpf', 100);
            $table->text('pix');
            $table->unsignedInteger('banco_id');
            $table->foreign('banco_id')
                    ->references('id')
                    ->on('banco');
            $table->string('agencia', 200);
            $table->string('conta', 200);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dados_bancario');
    }
    /**
        Schema::table('dados_bancario', function (Blueprint $table) {
            $foreignKeyName = "dados_bancario_banco_id_foreign";
            $table->dropForeign($foreignKeyName);
            $table->dropColumn('banco_id');
        });
     */
}
