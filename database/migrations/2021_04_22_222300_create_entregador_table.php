<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntregadorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Schema::dropIfExists('entregador');
        Schema::create('entregador', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome_completo', 200);
            $table->string('email', 150)->unique();
            $table->string('telefone', 50);
            $table->foreignId('praca_id')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entregador');
    }
}
