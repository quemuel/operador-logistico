<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableEntregador extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('entregador', function (Blueprint $table) {
            $table->integer('id_entregador_ifood') // Nome da coluna
                        ->after('ativo'); // Ordenado após a coluna "ativo"
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entregador', function (Blueprint $table) {
            $table->dropColumn('id_entregador_ifood');
        });
    }
}
