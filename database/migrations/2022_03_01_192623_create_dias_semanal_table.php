<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiasSemanalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dias_semanal', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('escala_turno_id');
            $table->foreign('escala_turno_id')
                ->references('id')
                ->on('escala_turno');
            $table->unsignedInteger('dia_id');
            $table->foreign('dia_id')
                ->references('id')
                ->on('dia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dias_semanal');
    }
}
