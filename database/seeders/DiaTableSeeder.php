<?php

namespace Database\Seeders;

use App\Models\Dia;
use Illuminate\Database\Seeder;

class DiaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dias = [
            [
                'nome' => 'Domingo',
                'codigo' => 0
            ],
            [
                'nome' => 'Segunda',
                'codigo' => 1
            ],
            [
                'nome' => 'Terça',
                'codigo' => 2
            ],
            [
                'nome' => 'Quarta',
                'codigo' => 3
            ],
            [
                'nome' => 'Quinta',
                'codigo' => 4
            ],
            [
                'nome' => 'Sexta',
                'codigo' => 5
            ],
            [
                'nome' => 'Sábado',
                'codigo' => 6
            ],
        ];
        
        foreach ($dias as $value) {
            Dia::create($value);
        }
    }
}
