<?php

namespace Database\Seeders;

use App\Models\Banco;
use Illuminate\Database\Seeder;

class CreateBancosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $params = [
            0 =>
            [
                'id' => '69',
                'codigo' => '1',
                'nome' => 'Banco Do Brasil S.A (BB)',
                'codigo_ispb' => '0',
            ],
            1 =>
            [
                'id' => '70',
                'codigo' => '237',
                'nome' => 'Bradesco S.A.',
                'codigo_ispb' => '60746948',
            ],
            2 =>
            [
                'id' => '71',
                'codigo' => '335',
                'nome' => 'Banco Digio S.A.',
                'codigo_ispb' => '27098060',
            ],
            3 =>
            [
                'id' => '72',
                'codigo' => '260',
                'nome' => 'Nu Pagamentos S.A (Nubank)',
                'codigo_ispb' => '18236120',
            ],
            4 =>
            [
                'id' => '73',
                'codigo' => '290',
                'nome' => 'PagSeguro Internet S.A.',
                'codigo_ispb' => '8561701',
            ],
            5 =>
            [
                'id' => '74',
                'codigo' => '323',
                'nome' => 'Mercado Pago – Conta Do Mercado Livre',
                'codigo_ispb' => '10573521',
            ],
            6 =>
            [
                'id' => '75',
                'codigo' => '237',
                'nome' => 'Next Bank',
                'codigo_ispb' => '60746948',
            ],
            7 =>
            [
                'id' => '76',
                'codigo' => '637',
                'nome' => 'Banco Sofisa S.A (Sofisa Direto)',
                'codigo_ispb' => '60889128',
            ],
            8 =>
            [
                'id' => '77',
                'codigo' => '77',
                'nome' => 'Banco Inter S.A.',
                'codigo_ispb' => '416968',
            ],
            9 =>
            [
                'id' => '78',
                'codigo' => '341',
                'nome' => 'Itaú Unibanco S.A.',
                'codigo_ispb' => '60701190',
            ],
            10 =>
            [
                'id' => '79',
                'codigo' => '104',
                'nome' => 'Caixa Econômica Federal (CEF)',
                'codigo_ispb' => '360305',
            ],
            11 =>
            [
                'id' => '80',
                'codigo' => '33',
                'nome' => 'Banco Santander Brasil S.A.',
                'codigo_ispb' => '90400888',
            ],
            12 =>
            [
                'id' => '81',
                'codigo' => '212',
                'nome' => 'Banco Original S.A.',
                'codigo_ispb' => '92894922',
            ],
            13 =>
            [
                'id' => '82',
                'codigo' => '756',
                'nome' => 'Bancoob – Banco Cooperativo Do Brasil S.A.',
                'codigo_ispb' => '2038232',
            ],
            14 =>
            [
                'id' => '83',
                'codigo' => '655',
                'nome' => 'Banco Votorantim S.A.',
                'codigo_ispb' => '59588111',
            ],
            15 =>
            [
                'id' => '84',
                'codigo' => '655',
                'nome' => 'Neon Pagamentos S.A',
                'codigo_ispb' => '59588111',
            ],
            16 =>
            [
                'id' => '85',
                'codigo' => '41',
                'nome' => 'Banrisul – Banco Do Estado Do Rio Grande Do Sul S.A.',
                'codigo_ispb' => '92702067',
            ],
            17 =>
            [
                'id' => '86',
                'codigo' => '389',
                'nome' => 'Banco Mercantil Do Brasil S.A.',
                'codigo_ispb' => '17184037',
            ],
            18 =>
            [
                'id' => '87',
                'codigo' => '422',
                'nome' => 'Banco Safra S.A.',
                'codigo_ispb' => '58160789',
            ],
            19 =>
            [
                'id' => '88',
                'codigo' => '70',
                'nome' => 'BRB – Banco De Brasília S.A.',
                'codigo_ispb' => '208',
            ],
            20 =>
            [
                'id' => '89',
                'codigo' => '136',
                'nome' => 'Unicred Cooperativa LTDA',
                'codigo_ispb' => '315557',
            ],
            21 =>
            [
                'id' => '90',
                'codigo' => '741',
                'nome' => 'Banco Ribeirão Preto S.A.',
                'codigo_ispb' => '517645',
            ],
            22 =>
            [
                'id' => '91',
                'codigo' => '739',
                'nome' => 'Banco Cetelem S.A.',
                'codigo_ispb' => '558456',
            ],
            23 =>
            [
                'id' => '92',
                'codigo' => '743',
                'nome' => 'Banco Semear S.A.',
                'codigo_ispb' => '795423',
            ],
            24 =>
            [
                'id' => '93',
                'codigo' => '100',
                'nome' => 'Planner Corretora De Valores S.A.',
                'codigo_ispb' => '806535',
            ],
            25 =>
            [
                'id' => '94',
                'codigo' => '96',
                'nome' => 'Banco B3 S.A.',
                'codigo_ispb' => '997185',
            ],
            26 =>
            [
                'id' => '95',
                'codigo' => '747',
                'nome' => 'Banco Rabobank Internacional Do Brasil S.A.',
                'codigo_ispb' => '1023570',
            ],
            27 =>
            [
                'id' => '96',
                'codigo' => '748',
                'nome' => 'Banco Cooperativa Sicredi S.A.',
                'codigo_ispb' => '1181521',
            ],
            28 =>
            [
                'id' => '97',
                'codigo' => '752',
                'nome' => 'Banco BNP Paribas Brasil S.A.',
                'codigo_ispb' => '1522368',
            ],
            29 =>
            [
                'id' => '98',
                'codigo' => '91',
                'nome' => 'Unicred Central Rs',
                'codigo_ispb' => '1634601',
            ],
            30 =>
            [
                'id' => '99',
                'codigo' => '399',
                'nome' => 'Kirton Bank S.A. – Banco Múltiplo',
                'codigo_ispb' => '1701201',
            ],
            31 =>
            [
                'id' => '100',
                'codigo' => '108',
                'nome' => 'Portocred S.A.',
                'codigo_ispb' => '1800019',
            ],
            32 =>
            [
                'id' => '101',
                'codigo' => '757',
                'nome' => 'Banco Keb Hana Do Brasil S.A.',
                'codigo_ispb' => '2318507',
            ],
            33 =>
            [
                'id' => '102',
                'codigo' => '102',
                'nome' => 'XP Investimentos S.A.',
                'codigo_ispb' => '2332886',
            ],
            34 =>
            [
                'id' => '103',
                'codigo' => '348',
                'nome' => 'Banco XP S/A',
                'codigo_ispb' => '33264668',
            ],
            35 =>
            [
                'id' => '104',
                'codigo' => '340',
                'nome' => 'Super Pagamentos S/A (Superdital)',
                'codigo_ispb' => '9554480',
            ],
            36 =>
            [
                'id' => '105',
                'codigo' => '84',
                'nome' => 'Uniprime Norte Do Paraná',
                'codigo_ispb' => '2398976',
            ],
            37 =>
            [
                'id' => '106',
                'codigo' => '180',
                'nome' => 'Cm Capital Markets Cctvm Ltda',
                'codigo_ispb' => '2685483',
            ],
            38 =>
            [
                'id' => '107',
                'codigo' => '66',
                'nome' => 'Banco Morgan Stanley S.A.',
                'codigo_ispb' => '2801938',
            ],
            39 =>
            [
                'id' => '108',
                'codigo' => '15',
                'nome' => 'UBS Brasil Cctvm S.A.',
                'codigo_ispb' => '2819125',
            ],
            40 =>
            [
                'id' => '109',
                'codigo' => '143',
                'nome' => 'Treviso Cc S.A.',
                'codigo_ispb' => '2992317',
            ],
            41 =>
            [
                'id' => '110',
                'codigo' => '62',
                'nome' => 'Hipercard Banco Múltiplo S.A.',
                'codigo_ispb' => '3012230',
            ],
            42 =>
            [
                'id' => '111',
                'codigo' => '74',
                'nome' => 'Banco J. Safra S.A.',
                'codigo_ispb' => '3017677',
            ],
            43 =>
            [
                'id' => '112',
                'codigo' => '99',
                'nome' => 'Uniprime Central Ccc Ltda',
                'codigo_ispb' => '3046391',
            ],
            44 =>
            [
                'id' => '113',
                'codigo' => '25',
                'nome' => 'Banco Alfa S.A.',
                'codigo_ispb' => '3323840',
            ],
            45 =>
            [
                'id' => '114',
                'codigo' => '75',
                'nome' => 'Bco Abn Amro S.A.',
                'codigo_ispb' => '3532415',
            ],
            46 =>
            [
                'id' => '115',
                'codigo' => '40',
                'nome' => 'Banco Cargill S.A.',
                'codigo_ispb' => '3609817',
            ],
            47 =>
            [
                'id' => '116',
                'codigo' => '190',
                'nome' => 'Servicoop',
                'codigo_ispb' => '3973814',
            ],
            48 =>
            [
                'id' => '117',
                'codigo' => '63',
                'nome' => 'Banco Bradescard',
                'codigo_ispb' => '4184779',
            ],
            49 =>
            [
                'id' => '118',
                'codigo' => '191',
                'nome' => 'Nova Futura Ctvm Ltda',
                'codigo_ispb' => '4257795',
            ],
            50 =>
            [
                'id' => '119',
                'codigo' => '64',
                'nome' => 'Goldman Sachs Do Brasil Bm S.A.',
                'codigo_ispb' => '4332281',
            ],
            51 =>
            [
                'id' => '120',
                'codigo' => '97',
                'nome' => 'Ccc Noroeste Brasileiro Ltda',
                'codigo_ispb' => '4632856',
            ],
            52 =>
            [
                'id' => '121',
                'codigo' => '16',
                'nome' => 'Ccm Desp Trâns Sc E Rs',
                'codigo_ispb' => '4715685',
            ],
            53 =>
            [
                'id' => '122',
                'codigo' => '12',
                'nome' => 'Banco Inbursa',
                'codigo_ispb' => '4866275',
            ],
            54 =>
            [
                'id' => '123',
                'codigo' => '3',
                'nome' => 'Banco Da Amazônia S.A.',
                'codigo_ispb' => '4902979',
            ],
            55 =>
            [
                'id' => '124',
                'codigo' => '60',
                'nome' => 'Confidence Cc S.A.',
                'codigo_ispb' => '4913129',
            ],
            56 =>
            [
                'id' => '125',
                'codigo' => '37',
                'nome' => 'Banco Do Estado Do Pará S.A.',
                'codigo_ispb' => '4913711',
            ],
            57 =>
            [
                'id' => '126',
                'codigo' => '159',
                'nome' => 'Casa do Crédito S.A.',
                'codigo_ispb' => '5442029',
            ],
            58 =>
            [
                'id' => '127',
                'codigo' => '172',
                'nome' => 'Albatross Ccv S.A.',
                'codigo_ispb' => '5452073',
            ],
            59 =>
            [
                'id' => '128',
                'codigo' => '85',
                'nome' => 'Cooperativa Central de Créditos – Ailos',
                'codigo_ispb' => '5463212',
            ],
            60 =>
            [
                'id' => '129',
                'codigo' => '114',
                'nome' => 'Central Cooperativa De Crédito no Estado do Espírito Santo',
                'codigo_ispb' => '5790149',
            ],
            61 =>
            [
                'id' => '130',
                'codigo' => '36',
                'nome' => 'Banco Bradesco BBI S.A.',
                'codigo_ispb' => '6271464',
            ],
            62 =>
            [
                'id' => '131',
                'codigo' => '394',
                'nome' => 'Banco Bradesco Financiamentos S.A.',
                'codigo_ispb' => '7207996',
            ],
            63 =>
            [
                'id' => '132',
                'codigo' => '4',
                'nome' => 'Banco Do Nordeste Do Brasil S.A.',
                'codigo_ispb' => '7237373',
            ],
            64 =>
            [
                'id' => '133',
                'codigo' => '320',
                'nome' => 'China Construction Bank – Ccb Brasil S.A.',
                'codigo_ispb' => '7450604',
            ],
            65 =>
            [
                'id' => '134',
                'codigo' => '189',
                'nome' => 'Hs Financeira',
                'codigo_ispb' => '7512441',
            ],
            66 =>
            [
                'id' => '135',
                'codigo' => '105',
                'nome' => 'Lecca Cfi S.A.',
                'codigo_ispb' => '7652226',
            ],
            67 =>
            [
                'id' => '136',
                'codigo' => '76',
                'nome' => 'Banco KDB Brasil S.A.',
                'codigo_ispb' => '7656500',
            ],
            68 =>
            [
                'id' => '137',
                'codigo' => '82',
                'nome' => 'Banco Topázio S.A.',
                'codigo_ispb' => '7679404',
            ],
            69 =>
            [
                'id' => '138',
                'codigo' => '286',
                'nome' => 'Cooperativa de Crédito Rural de De Ouro',
                'codigo_ispb' => '7853842',
            ],
            70 =>
            [
                'id' => '139',
                'codigo' => '93',
                'nome' => 'PóloCred Scmepp Ltda',
                'codigo_ispb' => '7945233',
            ],
            71 =>
            [
                'id' => '140',
                'codigo' => '273',
                'nome' => 'Ccr De São Miguel Do Oeste',
                'codigo_ispb' => '8253539',
            ],
            72 =>
            [
                'id' => '141',
                'codigo' => '157',
                'nome' => 'Icap Do Brasil Ctvm Ltda',
                'codigo_ispb' => '9105360',
            ],
            73 =>
            [
                'id' => '142',
                'codigo' => '183',
                'nome' => 'Socred S.A.',
                'codigo_ispb' => '9210106',
            ],
            74 =>
            [
                'id' => '143',
                'codigo' => '14',
                'nome' => 'Natixis Brasil S.A.',
                'codigo_ispb' => '9274232',
            ],
            75 =>
            [
                'id' => '144',
                'codigo' => '130',
                'nome' => 'Caruana Scfi',
                'codigo_ispb' => '9313766',
            ],
            76 =>
            [
                'id' => '145',
                'codigo' => '127',
                'nome' => 'Codepe Cvc S.A.',
                'codigo_ispb' => '9512542',
            ],
            77 =>
            [
                'id' => '146',
                'codigo' => '79',
                'nome' => 'Banco Original Do Agronegócio S.A.',
                'codigo_ispb' => '9516419',
            ],
            78 =>
            [
                'id' => '147',
                'codigo' => '81',
                'nome' => 'Bbn Banco Brasileiro De Negocios S.A.',
                'codigo_ispb' => '10264663',
            ],
            79 =>
            [
                'id' => '148',
                'codigo' => '118',
                'nome' => 'Standard Chartered Bi S.A.',
                'codigo_ispb' => '11932017',
            ],
            80 =>
            [
                'id' => '149',
                'codigo' => '133',
                'nome' => 'Cresol Confederação',
                'codigo_ispb' => '10398952',
            ],
            81 =>
            [
                'id' => '150',
                'codigo' => '121',
                'nome' => 'Banco Agibank S.A.',
                'codigo_ispb' => '10664513',
            ],
            82 =>
            [
                'id' => '151',
                'codigo' => '83',
                'nome' => 'Banco Da China Brasil S.A.',
                'codigo_ispb' => '10690848',
            ],
            83 =>
            [
                'id' => '152',
                'codigo' => '138',
                'nome' => 'Get Money Cc Ltda',
                'codigo_ispb' => '10853017',
            ],
            84 =>
            [
                'id' => '153',
                'codigo' => '24',
                'nome' => 'Banco Bandepe S.A.',
                'codigo_ispb' => '10866788',
            ],
            85 =>
            [
                'id' => '154',
                'codigo' => '95',
                'nome' => 'Banco Confidence De Câmbio S.A.',
                'codigo_ispb' => '11703662',
            ],
            86 =>
            [
                'id' => '155',
                'codigo' => '94',
                'nome' => 'Banco Finaxis',
                'codigo_ispb' => '11758741',
            ],
            87 =>
            [
                'id' => '156',
                'codigo' => '276',
                'nome' => 'Senff S.A.',
                'codigo_ispb' => '11970623',
            ],
            88 =>
            [
                'id' => '157',
                'codigo' => '137',
                'nome' => 'Multimoney Cc Ltda',
                'codigo_ispb' => '12586596',
            ],
            89 =>
            [
                'id' => '158',
                'codigo' => '92',
                'nome' => 'BRK S.A.',
                'codigo_ispb' => '12865507',
            ],
            90 =>
            [
                'id' => '159',
                'codigo' => '47',
                'nome' => 'Banco do Estado de Sergipe S.A.',
                'codigo_ispb' => '13009717',
            ],
            91 =>
            [
                'id' => '160',
                'codigo' => '144',
                'nome' => 'Bexs Banco De Cambio S.A.',
                'codigo_ispb' => '13059145',
            ],
            92 =>
            [
                'id' => '161',
                'codigo' => '126',
                'nome' => 'BR Partners Banco de Investimento S.A.',
                'codigo_ispb' => '13220493',
            ],
            93 =>
            [
                'id' => '162',
                'codigo' => '301',
                'nome' => 'BPP Instituição De Pagamentos S.A.',
                'codigo_ispb' => '13370835',
            ],
            94 =>
            [
                'id' => '163',
                'codigo' => '173',
                'nome' => 'BRL Trust Dtvm Sa',
                'codigo_ispb' => '13486793',
            ],
            95 =>
            [
                'id' => '164',
                'codigo' => '119',
                'nome' => 'Banco Western Union do Brasil S.A.',
                'codigo_ispb' => '13720915',
            ],
            96 =>
            [
                'id' => '165',
                'codigo' => '254',
                'nome' => 'Paraná Banco S.A.',
                'codigo_ispb' => '14388334',
            ],
            97 =>
            [
                'id' => '166',
                'codigo' => '268',
                'nome' => 'Barigui Companhia Hipotecária',
                'codigo_ispb' => '14511781',
            ],
            98 =>
            [
                'id' => '167',
                'codigo' => '107',
                'nome' => 'Banco Bocom BBM S.A.',
                'codigo_ispb' => '15114366',
            ],
            99 =>
            [
                'id' => '168',
                'codigo' => '412',
                'nome' => 'Banco Capital S.A.',
                'codigo_ispb' => '15173776',
            ],
            100 =>
            [
                'id' => '169',
                'codigo' => '124',
                'nome' => 'Banco Woori Bank Do Brasil S.A.',
                'codigo_ispb' => '15357060',
            ],
            101 =>
            [
                'id' => '170',
                'codigo' => '149',
                'nome' => 'Facta S.A. Cfi',
                'codigo_ispb' => '15581638',
            ],
            102 =>
            [
                'id' => '171',
                'codigo' => '197',
                'nome' => 'Stone Pagamentos S.A.',
                'codigo_ispb' => '16501555',
            ],
            103 =>
            [
                'id' => '172',
                'codigo' => '142',
                'nome' => 'Broker Brasil Cc Ltda',
                'codigo_ispb' => '16944141',
            ],
            104 =>
            [
                'id' => '173',
                'codigo' => '389',
                'nome' => 'Banco Mercantil Do Brasil S.A.',
                'codigo_ispb' => '17184037',
            ],
            105 =>
            [
                'id' => '174',
                'codigo' => '184',
                'nome' => 'Banco Itaú BBA S.A.',
                'codigo_ispb' => '17298092',
            ],
            106 =>
            [
                'id' => '175',
                'codigo' => '634',
                'nome' => 'Banco Triangulo S.A (Banco Triângulo)',
                'codigo_ispb' => '17351180',
            ],
            107 =>
            [
                'id' => '176',
                'codigo' => '545',
                'nome' => 'Senso Ccvm S.A.',
                'codigo_ispb' => '17352220',
            ],
            108 =>
            [
                'id' => '177',
                'codigo' => '132',
                'nome' => 'ICBC do Brasil Bm S.A.',
                'codigo_ispb' => '17453575',
            ],
            109 =>
            [
                'id' => '178',
                'codigo' => '298',
                'nome' => 'Vip’s Cc Ltda',
                'codigo_ispb' => '17772370',
            ],
            110 =>
            [
                'id' => '179',
                'codigo' => '129',
                'nome' => 'UBS Brasil Bi S.A.',
                'codigo_ispb' => '18520834',
            ],
            111 =>
            [
                'id' => '180',
                'codigo' => '128',
                'nome' => 'Ms Bank S.A Banco De Câmbio',
                'codigo_ispb' => '19307785',
            ],
            112 =>
            [
                'id' => '181',
                'codigo' => '194',
                'nome' => 'Parmetal Dtvm Ltda',
                'codigo_ispb' => '20155248',
            ],
            113 =>
            [
                'id' => '182',
                'codigo' => '310',
                'nome' => 'VORTX Dtvm Ltda',
                'codigo_ispb' => '22610500',
            ],
            114 =>
            [
                'id' => '183',
                'codigo' => '163',
                'nome' => 'Commerzbank Brasil S.A Banco Múltiplo',
                'codigo_ispb' => '23522214',
            ],
            115 =>
            [
                'id' => '184',
                'codigo' => '280',
                'nome' => 'Avista S.A.',
                'codigo_ispb' => '23862762',
            ],
            116 =>
            [
                'id' => '185',
                'codigo' => '146',
                'nome' => 'Guitta Cc Ltda',
                'codigo_ispb' => '24074692',
            ],
            117 =>
            [
                'id' => '186',
                'codigo' => '279',
                'nome' => 'Ccr De Primavera Do Leste',
                'codigo_ispb' => '26563270',
            ],
            118 =>
            [
                'id' => '187',
                'codigo' => '182',
                'nome' => 'Dacasa Financeira S/A',
                'codigo_ispb' => '27406222',
            ],
            119 =>
            [
                'id' => '188',
                'codigo' => '278',
                'nome' => 'Genial Investimentos Cvm S.A.',
                'codigo_ispb' => '27652684',
            ],
            120 =>
            [
                'id' => '189',
                'codigo' => '271',
                'nome' => 'Ib Cctvm Ltda',
                'codigo_ispb' => '27842177',
            ],
            121 =>
            [
                'id' => '190',
                'codigo' => '21',
                'nome' => 'Banco Banestes S.A.',
                'codigo_ispb' => '28127603',
            ],
            122 =>
            [
                'id' => '191',
                'codigo' => '246',
                'nome' => 'Banco Abc Brasil S.A.',
                'codigo_ispb' => '28195667',
            ],
            123 =>
            [
                'id' => '192',
                'codigo' => '751',
                'nome' => 'Scotiabank Brasil',
                'codigo_ispb' => '29030467',
            ],
            124 =>
            [
                'id' => '193',
                'codigo' => '208',
                'nome' => 'Banco BTG Pactual S.A.',
                'codigo_ispb' => '30306294',
            ],
            125 =>
            [
                'id' => '194',
                'codigo' => '746',
                'nome' => 'Banco Modal S.A.',
                'codigo_ispb' => '30723886',
            ],
            126 =>
            [
                'id' => '195',
                'codigo' => '241',
                'nome' => 'Banco Classico S.A.',
                'codigo_ispb' => '31597552',
            ],
            127 =>
            [
                'id' => '196',
                'codigo' => '612',
                'nome' => 'Banco Guanabara S.A.',
                'codigo_ispb' => '31880826',
            ],
            128 =>
            [
                'id' => '197',
                'codigo' => '604',
                'nome' => 'Banco Industrial Do Brasil S.A.',
                'codigo_ispb' => '31895683',
            ],
            129 =>
            [
                'id' => '198',
                'codigo' => '505',
                'nome' => 'Banco Credit Suisse (Brl) S.A.',
                'codigo_ispb' => '32062580',
            ],
            130 =>
            [
                'id' => '199',
                'codigo' => '196',
                'nome' => 'Banco Fair Cc S.A.',
                'codigo_ispb' => '32648370',
            ],
            131 =>
            [
                'id' => '200',
                'codigo' => '300',
                'nome' => 'Banco La Nacion Argentina',
                'codigo_ispb' => '33042151',
            ],
            132 =>
            [
                'id' => '201',
                'codigo' => '477',
                'nome' => 'Citibank N.A.',
                'codigo_ispb' => '33042953',
            ],
            133 =>
            [
                'id' => '202',
                'codigo' => '266',
                'nome' => 'Banco Cedula S.A.',
                'codigo_ispb' => '33132044',
            ],
            134 =>
            [
                'id' => '203',
                'codigo' => '122',
                'nome' => 'Banco Bradesco BERJ S.A.',
                'codigo_ispb' => '33147315',
            ],
            135 =>
            [
                'id' => '204',
                'codigo' => '376',
                'nome' => 'Banco J.P. Morgan S.A.',
                'codigo_ispb' => '33172537',
            ],
            136 =>
            [
                'id' => '205',
                'codigo' => '473',
                'nome' => 'Banco Caixa Geral Brasil S.A.',
                'codigo_ispb' => '33466988',
            ],
            137 =>
            [
                'id' => '206',
                'codigo' => '745',
                'nome' => 'Banco Citibank S.A.',
                'codigo_ispb' => '33479023',
            ],
            138 =>
            [
                'id' => '207',
                'codigo' => '120',
                'nome' => 'Banco Rodobens S.A.',
                'codigo_ispb' => '33603457',
            ],
            139 =>
            [
                'id' => '208',
                'codigo' => '265',
                'nome' => 'Banco Fator S.A.',
                'codigo_ispb' => '33644196',
            ],
            140 =>
            [
                'id' => '209',
                'codigo' => '7',
                'nome' => 'BNDES (Banco Nacional Do Desenvolvimento Social)',
                'codigo_ispb' => '33657248',
            ],
            141 =>
            [
                'id' => '210',
                'codigo' => '188',
                'nome' => 'Ativa S.A Investimentos',
                'codigo_ispb' => '33775974',
            ],
            142 =>
            [
                'id' => '211',
                'codigo' => '134',
                'nome' => 'BGC Liquidez Dtvm Ltda',
                'codigo_ispb' => '33862244',
            ],
            143 =>
            [
                'id' => '212',
                'codigo' => '641',
                'nome' => 'Banco Alvorada S.A.',
                'codigo_ispb' => '33870163',
            ],
            144 =>
            [
                'id' => '213',
                'codigo' => '29',
                'nome' => 'Banco Itaú Consignado S.A.',
                'codigo_ispb' => '33885724',
            ],
            145 =>
            [
                'id' => '214',
                'codigo' => '243',
                'nome' => 'Banco Máxima S.A.',
                'codigo_ispb' => '33923798',
            ],
            146 =>
            [
                'id' => '215',
                'codigo' => '78',
                'nome' => 'Haitong Bi Do Brasil S.A.',
                'codigo_ispb' => '34111187',
            ],
            147 =>
            [
                'id' => '216',
                'codigo' => '111',
                'nome' => 'Banco Oliveira Trust Dtvm S.A.',
                'codigo_ispb' => '36113876',
            ],
            148 =>
            [
                'id' => '217',
                'codigo' => '17',
                'nome' => 'Bny Mellon Banco S.A.',
                'codigo_ispb' => '42272526',
            ],
            149 =>
            [
                'id' => '218',
                'codigo' => '174',
                'nome' => 'Pernambucanas Financ S.A.',
                'codigo_ispb' => '43180355',
            ],
            150 =>
            [
                'id' => '219',
                'codigo' => '495',
                'nome' => 'La Provincia Buenos Aires Banco',
                'codigo_ispb' => '44189447',
            ],
            151 =>
            [
                'id' => '220',
                'codigo' => '125',
                'nome' => 'Brasil Plural S.A Banco',
                'codigo_ispb' => '45246410',
            ],
            152 =>
            [
                'id' => '221',
                'codigo' => '488',
                'nome' => 'Jpmorgan Chase Bank',
                'codigo_ispb' => '46518205',
            ],
            153 =>
            [
                'id' => '222',
                'codigo' => '65',
                'nome' => 'Banco Andbank S.A.',
                'codigo_ispb' => '48795256',
            ],
            154 =>
            [
                'id' => '223',
                'codigo' => '492',
                'nome' => 'Ing Bank N.V.',
                'codigo_ispb' => '49336860',
            ],
            155 =>
            [
                'id' => '224',
                'codigo' => '250',
                'nome' => 'Banco Bcv',
                'codigo_ispb' => '50585090',
            ],
            156 =>
            [
                'id' => '225',
                'codigo' => '145',
                'nome' => 'Levycam Ccv Ltda',
                'codigo_ispb' => '50579044',
            ],
            157 =>
            [
                'id' => '226',
                'codigo' => '494',
                'nome' => 'Banco Rep Oriental Uruguay',
                'codigo_ispb' => '51938876',
            ],
            158 =>
            [
                'id' => '227',
                'codigo' => '253',
                'nome' => 'Bexs Cc S.A.',
                'codigo_ispb' => '52937216',
            ],
            159 =>
            [
                'id' => '228',
                'codigo' => '269',
                'nome' => 'Hsbc Banco De Investimento',
                'codigo_ispb' => '53518684',
            ],
            160 =>
            [
                'id' => '229',
                'codigo' => '213',
                'nome' => 'Bco Arbi S.A.',
                'codigo_ispb' => '54403563',
            ],
            161 =>
            [
                'id' => '230',
                'codigo' => '139',
                'nome' => 'Intesa Sanpaolo Brasil S.A.',
                'codigo_ispb' => '55230916',
            ],
            162 =>
            [
                'id' => '231',
                'codigo' => '18',
                'nome' => 'Banco Tricury S.A.',
                'codigo_ispb' => '57839805',
            ],
            163 =>
            [
                'id' => '232',
                'codigo' => '630',
                'nome' => 'Banco Intercap S.A.',
                'codigo_ispb' => '58497702',
            ],
            164 =>
            [
                'id' => '233',
                'codigo' => '224',
                'nome' => 'Banco Fibra S.A.',
                'codigo_ispb' => '58616418',
            ],
            165 =>
            [
                'id' => '234',
                'codigo' => '600',
                'nome' => 'Banco Luso Brasileiro S.A.',
                'codigo_ispb' => '59118133',
            ],
            166 =>
            [
                'id' => '235',
                'codigo' => '623',
                'nome' => 'Banco Pan S.A.',
                'codigo_ispb' => '59285411',
            ],
            167 =>
            [
                'id' => '236',
                'codigo' => '204',
                'nome' => 'Banco Bradesco Cartoes S.A.',
                'codigo_ispb' => '59438325',
            ],
            168 =>
            [
                'id' => '237',
                'codigo' => '479',
                'nome' => 'Banco ItauBank S.A.',
                'codigo_ispb' => '60394079',
            ],
            169 =>
            [
                'id' => '238',
                'codigo' => '456',
                'nome' => 'Banco MUFG Brasil S.A.',
                'codigo_ispb' => '60498557',
            ],
            170 =>
            [
                'id' => '239',
                'codigo' => '464',
                'nome' => 'Banco Sumitomo Mitsui Brasil S.A.',
                'codigo_ispb' => '60518222',
            ],
            171 =>
            [
                'id' => '240',
                'codigo' => '613',
                'nome' => 'Omni Banco S.A.',
                'codigo_ispb' => '60850229',
            ],
            172 =>
            [
                'id' => '241',
                'codigo' => '652',
                'nome' => 'Itaú Unibanco Holding Bm S.A.',
                'codigo_ispb' => '60872504',
            ],
            173 =>
            [
                'id' => '242',
                'codigo' => '653',
                'nome' => 'Banco Indusval S.A.',
                'codigo_ispb' => '61024352',
            ],
            174 =>
            [
                'id' => '243',
                'codigo' => '69',
                'nome' => 'Banco Crefisa S.A.',
                'codigo_ispb' => '61033106',
            ],
            175 =>
            [
                'id' => '244',
                'codigo' => '370',
                'nome' => 'Banco Mizuho S.A.',
                'codigo_ispb' => '61088183',
            ],
            176 =>
            [
                'id' => '245',
                'codigo' => '249',
                'nome' => 'Banco Investcred Unibanco S.A.',
                'codigo_ispb' => '61182408',
            ],
            177 =>
            [
                'id' => '246',
                'codigo' => '318',
                'nome' => 'Banco BMG S.A.',
                'codigo_ispb' => '61186680',
            ],
            178 =>
            [
                'id' => '247',
                'codigo' => '626',
                'nome' => 'Banco Ficsa S.A.',
                'codigo_ispb' => '61348538',
            ],
            179 =>
            [
                'id' => '248',
                'codigo' => '270',
                'nome' => 'Sagitur Cc Ltda',
                'codigo_ispb' => '61444949',
            ],
            180 =>
            [
                'id' => '249',
                'codigo' => '366',
                'nome' => 'Banco Societe Generale Brasil',
                'codigo_ispb' => '61533584',
            ],
            181 =>
            [
                'id' => '250',
                'codigo' => '113',
                'nome' => 'Magliano S.A.',
                'codigo_ispb' => '61723847',
            ],
            182 =>
            [
                'id' => '251',
                'codigo' => '131',
                'nome' => 'Tullett Prebon Brasil Cvc Ltda',
                'codigo_ispb' => '61747085',
            ],
            183 =>
            [
                'id' => '252',
                'codigo' => '11',
                'nome' => 'C.Suisse Hedging-Griffo Cv S.A (Credit Suisse)',
                'codigo_ispb' => '61809182',
            ],
            184 =>
            [
                'id' => '253',
                'codigo' => '611',
                'nome' => 'Banco Paulista',
                'codigo_ispb' => '61820817',
            ],
            185 =>
            [
                'id' => '254',
                'codigo' => '755',
                'nome' => 'Bofa Merrill Lynch Bm S.A.',
                'codigo_ispb' => '62073200',
            ],
            186 =>
            [
                'id' => '255',
                'codigo' => '89',
                'nome' => 'Ccr Reg Mogiana',
                'codigo_ispb' => '62109566',
            ],
            187 =>
            [
                'id' => '256',
                'codigo' => '643',
                'nome' => 'Banco Pine S.A.',
                'codigo_ispb' => '62144175',
            ],
            188 =>
            [
                'id' => '257',
                'codigo' => '140',
                'nome' => 'Easynvest – Título Cv S.A.',
                'codigo_ispb' => '62169875',
            ],
            189 =>
            [
                'id' => '258',
                'codigo' => '707',
                'nome' => 'Banco Daycoval S.A.',
                'codigo_ispb' => '62232889',
            ],
            190 =>
            [
                'id' => '259',
                'codigo' => '288',
                'nome' => 'Carol Dtvm Ltda',
                'codigo_ispb' => '62237649',
            ],
            191 =>
            [
                'id' => '260',
                'codigo' => '101',
                'nome' => 'Renascença Dtvm Ltda',
                'codigo_ispb' => '62287735',
            ],
            192 =>
            [
                'id' => '261',
                'codigo' => '487',
                'nome' => 'Deutsche Bank S.A (Banco Alemão)',
                'codigo_ispb' => '62331228',
            ],
            193 =>
            [
                'id' => '262',
                'codigo' => '233',
                'nome' => 'Banco Cifra S.A.',
                'codigo_ispb' => '62421979',
            ],
            194 =>
            [
                'id' => '263',
                'codigo' => '177',
                'nome' => 'Guide Investimentos S.A. Corretora de Valores',
                'codigo_ispb' => '65913436',
            ],
            195 =>
            [
                'id' => '264',
                'codigo' => '633',
                'nome' => 'Banco Rendimento S.A.',
                'codigo_ispb' => '68900810',
            ],
            196 =>
            [
                'id' => '265',
                'codigo' => '218',
                'nome' => 'Banco Bs2 S.A.',
                'codigo_ispb' => '71027866',
            ],
            197 =>
            [
                'id' => '266',
                'codigo' => '292',
                'nome' => 'BS2 Distribuidora De Títulos E Investimentos',
                'codigo_ispb' => '28650236',
            ],
            198 =>
            [
                'id' => '267',
                'codigo' => '169',
                'nome' => 'Banco Olé Bonsucesso Consignado S.A.',
                'codigo_ispb' => '71371686',
            ],
            199 =>
            [
                'id' => '268',
                'codigo' => '293',
                'nome' => 'Lastro Rdv Dtvm Ltda',
                'codigo_ispb' => '71590442',
            ],
            200 =>
            [
                'id' => '269',
                'codigo' => '285',
                'nome' => 'Frente Cc Ltda',
                'codigo_ispb' => '71677850',
            ],
            201 =>
            [
                'id' => '270',
                'codigo' => '80',
                'nome' => 'B&T Cc Ltda',
                'codigo_ispb' => '73622748',
            ],
            202 =>
            [
                'id' => '271',
                'codigo' => '753',
                'nome' => 'Novo Banco Continental S.A Bm',
                'codigo_ispb' => '74828799',
            ],
            203 =>
            [
                'id' => '272',
                'codigo' => '222',
                'nome' => 'Banco Crédit Agricole Br S.A.',
                'codigo_ispb' => '75647891',
            ],
            204 =>
            [
                'id' => '273',
                'codigo' => '754',
                'nome' => 'Banco Sistema S.A.',
                'codigo_ispb' => '76543115',
            ],
            205 =>
            [
                'id' => '274',
                'codigo' => '98',
                'nome' => 'Credialiança Ccr',
                'codigo_ispb' => '78157146',
            ],
            206 =>
            [
                'id' => '275',
                'codigo' => '610',
                'nome' => 'Banco VR S.A.',
                'codigo_ispb' => '78626983',
            ],
            207 =>
            [
                'id' => '276',
                'codigo' => '712',
                'nome' => 'Banco Ourinvest S.A.',
                'codigo_ispb' => '78632767',
            ],
            208 =>
            [
                'id' => '277',
                'codigo' => '10',
                'nome' => 'CREDICOAMO CRÉDITO RURAL COOPERATIVA',
                'codigo_ispb' => '81723108',
            ],
            209 =>
            [
                'id' => '278',
                'codigo' => '283',
                'nome' => 'RB Capital Investimentos Dtvm Ltda',
                'codigo_ispb' => '89960090',
            ],
            210 =>
            [
                'id' => '279',
                'codigo' => '217',
                'nome' => 'Banco John Deere S.A.',
                'codigo_ispb' => '91884981',
            ],
            211 =>
            [
                'id' => '280',
                'codigo' => '117',
                'nome' => 'Advanced Cc Ltda',
                'codigo_ispb' => '92856905',
            ],
            212 =>
            [
                'id' => '281',
                'codigo' => '336',
                'nome' => 'Banco C6 S.A – C6 Bank',
                'codigo_ispb' => '28326000',
            ],
            213 =>
            [
                'id' => '282',
                'codigo' => '654',
                'nome' => 'Banco A.J. Renner S.A.',
                'codigo_ispb' => '92874270',
            ],
            214 =>
            [
                'id' => '283',
                'codigo' => '0',
                'nome' => 'Banco Central do Brasil – Selic',
                'codigo_ispb' => '38121',
            ],
            215 =>
            [
                'id' => '284',
                'codigo' => '0',
                'nome' => 'Banco Central do Brasil',
                'codigo_ispb' => '38166',
            ],
            216 =>
            [
                'id' => '285',
                'codigo' => '272',
                'nome' => 'AGK Corretora de Câmbio S.A.',
                'codigo_ispb' => '250699',
            ],
            217 =>
            [
                'id' => '286',
                'codigo' => '0',
                'nome' => 'Secretaria do Tesouro Nacional – STN',
                'codigo_ispb' => '394460',
            ],
            218 =>
            [
                'id' => '287',
                'codigo' => '330',
                'nome' => 'Banco Bari de Investimentos e Financiamentos S.A.',
                'codigo_ispb' => '556603',
            ],
            219 =>
            [
                'id' => '288',
                'codigo' => '362',
                'nome' => 'CIELO S.A.',
                'codigo_ispb' => '1027058',
            ],
            220 =>
            [
                'id' => '289',
                'codigo' => '322',
                'nome' => 'Cooperativa de Crédito Rural de Abelardo Luz – Sulcredi/Crediluz',
                'codigo_ispb' => '1073966',
            ],
            221 =>
            [
                'id' => '290',
                'codigo' => '350',
                'nome' => 'Cooperativa De Crédito Rural De Pequenos Agricultores E Da Reforma Agrária Do Ce',
                'codigo_ispb' => '1330387',
            ],
            222 =>
            [
                'id' => '291',
                'codigo' => '91',
                'nome' => 'Central De Cooperativas De Economia E Crédito Mútuo Do Estado Do Rio Grande Do S',
                'codigo_ispb' => '1634601',
            ],
            223 =>
            [
                'id' => '292',
                'codigo' => '379',
                'nome' => 'COOPERFORTE – Cooperativa De Economia E Crédito Mútuo Dos Funcionários De Instit',
                'codigo_ispb' => '1658426',
            ],
            224 =>
            [
                'id' => '293',
                'codigo' => '378',
                'nome' => 'BBC LEASING S.A. – Arrendamento Mercantil',
                'codigo_ispb' => '1852137',
            ],
            225 =>
            [
                'id' => '294',
                'codigo' => '360',
                'nome' => 'TRINUS Capital Distribuidora De Títulos E Valores Mobiliários S.A.',
                'codigo_ispb' => '2276653',
            ],
            226 =>
            [
                'id' => '295',
                'codigo' => '84',
                'nome' => 'UNIPRIME NORTE DO PARANÁ – COOPERATIVA DE CRÉDITO LTDA',
                'codigo_ispb' => '2398976',
            ],
            227 =>
            [
                'id' => '296',
                'codigo' => '0',
                'nome' => 'Câmara Interbancária de Pagamentos – CIP – LDL',
                'codigo_ispb' => '2992335',
            ],
            228 =>
            [
                'id' => '297',
                'codigo' => '387',
                'nome' => 'Banco Toyota do Brasil S.A.',
                'codigo_ispb' => '3215790',
            ],
            229 =>
            [
                'id' => '298',
                'codigo' => '326',
                'nome' => 'PARATI – CRÉDITO, FINANCIAMENTO E INVESTIMENTO S.A.',
                'codigo_ispb' => '3311443',
            ],
            230 =>
            [
                'id' => '299',
                'codigo' => '315',
                'nome' => 'PI Distribuidora de Títulos e Valores Mobiliários S.A.',
                'codigo_ispb' => '3502968',
            ],
            231 =>
            [
                'id' => '300',
                'codigo' => '307',
                'nome' => 'Terra Investimentos Distribuidora de Títulos e Valores Mobiliários Ltda.',
                'codigo_ispb' => '3751794',
            ],
            232 =>
            [
                'id' => '301',
                'codigo' => '296',
                'nome' => 'VISION S.A. CORRETORA DE CAMBIO',
                'codigo_ispb' => '4062902',
            ],
            233 =>
            [
                'id' => '302',
                'codigo' => '382',
                'nome' => 'FIDÚCIA SOCIEDADE DE CRÉDITO AO MICROEMPREENDEDOR E À EMPRESA DE PEQUENO PORTE L',
                'codigo_ispb' => '4307598',
            ],
            234 =>
            [
                'id' => '303',
                'codigo' => '97',
                'nome' => 'Credisis – Central de Cooperativas de Crédito Ltda.',
                'codigo_ispb' => '4632856',
            ],
            235 =>
            [
                'id' => '304',
                'codigo' => '16',
                'nome' => 'COOPERATIVA DE CRÉDITO MÚTUO DOS DESPACHANTES DE TRÂNSITO DE SANTA CATARINA E RI',
                'codigo_ispb' => '4715685',
            ],
            236 =>
            [
                'id' => '305',
                'codigo' => '299',
                'nome' => 'SOROCRED   CRÉDITO, FINANCIAMENTO E INVESTIMENTO S.A.',
                'codigo_ispb' => '4814563',
            ],
            237 =>
            [
                'id' => '306',
                'codigo' => '359',
                'nome' => 'ZEMA CRÉDITO, FINANCIAMENTO E INVESTIMENTO S/A',
                'codigo_ispb' => '5351887',
            ],
            238 =>
            [
                'id' => '307',
                'codigo' => '391',
                'nome' => 'COOPERATIVA DE CRÉDITO RURAL DE IBIAM – SULCREDI/IBIAM',
                'codigo_ispb' => '8240446',
            ],
            239 =>
            [
                'id' => '308',
                'codigo' => '368',
                'nome' => 'Banco CSF S.A.',
                'codigo_ispb' => '8357240',
            ],
            240 =>
            [
                'id' => '309',
                'codigo' => '259',
                'nome' => 'MONEYCORP BANCO DE CÂMBIO S.A.',
                'codigo_ispb' => '8609934',
            ],
            241 =>
            [
                'id' => '310',
                'codigo' => '364',
                'nome' => 'GERENCIANET S.A.',
                'codigo_ispb' => '9089356',
            ],
            242 =>
            [
                'id' => '311',
                'codigo' => '14',
                'nome' => 'STATE STREET BRASIL S.A. – BANCO COMERCIAL',
                'codigo_ispb' => '9274232',
            ],
            243 =>
            [
                'id' => '312',
                'codigo' => '81',
                'nome' => 'Banco Seguro S.A.',
                'codigo_ispb' => '10264663',
            ],
            244 =>
            [
                'id' => '313',
                'codigo' => '384',
                'nome' => 'GLOBAL FINANÇAS SOCIEDADE DE CRÉDITO AO MICROEMPREENDEDOR E À EMPRESA DE PEQUENO',
                'codigo_ispb' => '11165756',
            ],
            245 =>
            [
                'id' => '314',
                'codigo' => '88',
                'nome' => 'BANCO RANDON S.A.',
                'codigo_ispb' => '11476673',
            ],
            246 =>
            [
                'id' => '315',
                'codigo' => '319',
                'nome' => 'OM DISTRIBUIDORA DE TÍTULOS E VALORES MOBILIÁRIOS LTDA',
                'codigo_ispb' => '11495073',
            ],
            247 =>
            [
                'id' => '316',
                'codigo' => '274',
                'nome' => 'MONEY PLUS SOCIEDADE DE CRÉDITO AO MICROEMPREENDEDOR E A EMPRESA DE PEQUENO PORT',
                'codigo_ispb' => '11581339',
            ],
            248 =>
            [
                'id' => '317',
                'codigo' => '95',
                'nome' => 'Travelex Banco de Câmbio S.A.',
                'codigo_ispb' => '11703662',
            ],
            249 =>
            [
                'id' => '318',
                'codigo' => '332',
                'nome' => 'Acesso Soluções de Pagamento S.A.',
                'codigo_ispb' => '13140088',
            ],
            250 =>
            [
                'id' => '319',
                'codigo' => '325',
                'nome' => 'Órama Distribuidora de Títulos e Valores Mobiliários S.A.',
                'codigo_ispb' => '13293225',
            ],
            251 =>
            [
                'id' => '320',
                'codigo' => '331',
                'nome' => 'Fram Capital Distribuidora de Títulos e Valores Mobiliários S.A.',
                'codigo_ispb' => '13673855',
            ],
            252 =>
            [
                'id' => '321',
                'codigo' => '396',
                'nome' => 'HUB PAGAMENTOS S.A.',
                'codigo_ispb' => '13884775',
            ],
            253 =>
            [
                'id' => '322',
                'codigo' => '309',
                'nome' => 'CAMBIONET CORRETORA DE CÂMBIO LTDA.',
                'codigo_ispb' => '14190547',
            ],
            254 =>
            [
                'id' => '323',
                'codigo' => '313',
                'nome' => 'AMAZÔNIA CORRETORA DE CÂMBIO LTDA.',
                'codigo_ispb' => '16927221',
            ],
            255 =>
            [
                'id' => '324',
                'codigo' => '377',
                'nome' => 'MS SOCIEDADE DE CRÉDITO AO MICROEMPREENDEDOR E À EMPRESA DE PEQUENO PORTE LTDA',
                'codigo_ispb' => '17826860',
            ],
            256 =>
            [
                'id' => '325',
                'codigo' => '321',
                'nome' => 'CREFAZ SOCIEDADE DE CRÉDITO AO MICROEMPREENDEDOR E A EMPRESA DE PEQUENO PORTE LT',
                'codigo_ispb' => '18188384',
            ],
            257 =>
            [
                'id' => '326',
                'codigo' => '383',
                'nome' => 'BOLETOBANCÁRIO.COM TECNOLOGIA DE PAGAMENTOS LTDA.',
                'codigo_ispb' => '21018182',
            ],
            258 =>
            [
                'id' => '327',
                'codigo' => '324',
                'nome' => 'CARTOS SOCIEDADE DE CRÉDITO DIRETO S.A.',
                'codigo_ispb' => '21332862',
            ],
            259 =>
            [
                'id' => '328',
                'codigo' => '380',
                'nome' => 'PICPAY SERVICOS S.A.',
                'codigo_ispb' => '22896431',
            ],
            260 =>
            [
                'id' => '329',
                'codigo' => '343',
                'nome' => 'FFA SOCIEDADE DE CRÉDITO AO MICROEMPREENDEDOR E À EMPRESA DE PEQUENO PORTE LTDA.',
                'codigo_ispb' => '24537861',
            ],
            261 =>
            [
                'id' => '330',
                'codigo' => '349',
                'nome' => 'AL5 S.A. CRÉDITO, FINANCIAMENTO E INVESTIMENTO',
                'codigo_ispb' => '27214112',
            ],
            262 =>
            [
                'id' => '331',
                'codigo' => '374',
                'nome' => 'REALIZE CRÉDITO, FINANCIAMENTO E INVESTIMENTO S.A.',
                'codigo_ispb' => '27351731',
            ],
            263 =>
            [
                'id' => '332',
                'codigo' => '0',
                'nome' => 'B3 SA – Brasil, Bolsa, Balcão – Segmento Cetip UTVM',
                'codigo_ispb' => '28719664',
            ],
            264 =>
            [
                'id' => '333',
                'codigo' => '0',
                'nome' => 'Câmara Interbancária de Pagamentos – CIP C3',
                'codigo_ispb' => '29011780',
            ],
            265 =>
            [
                'id' => '334',
                'codigo' => '352',
                'nome' => 'TORO CORRETORA DE TÍTULOS E VALORES MOBILIÁRIOS LTDA',
                'codigo_ispb' => '29162769',
            ],
            266 =>
            [
                'id' => '335',
                'codigo' => '329',
                'nome' => 'QI Sociedade de Crédito Direto S.A.',
                'codigo_ispb' => '32402502',
            ],
            267 =>
            [
                'id' => '336',
                'codigo' => '342',
                'nome' => 'Creditas Sociedade de Crédito Direto S.A.',
                'codigo_ispb' => '32997490',
            ],
            268 =>
            [
                'id' => '337',
                'codigo' => '397',
                'nome' => 'LISTO SOCIEDADE DE CRÉDITO DIRETO S.A.',
                'codigo_ispb' => '34088029',
            ],
            269 =>
            [
                'id' => '338',
                'codigo' => '355',
                'nome' => 'ÓTIMO SOCIEDADE DE CRÉDITO DIRETO S.A.',
                'codigo_ispb' => '34335592',
            ],
            270 =>
            [
                'id' => '339',
                'codigo' => '367',
                'nome' => 'VITREO DISTRIBUIDORA DE TÍTULOS E VALORES MOBILIÁRIOS S.A.',
                'codigo_ispb' => '34711571',
            ],
            271 =>
            [
                'id' => '340',
                'codigo' => '373',
                'nome' => 'UP.P SOCIEDADE DE EMPRÉSTIMO ENTRE PESSOAS S.A.',
                'codigo_ispb' => '35977097',
            ],
            272 =>
            [
                'id' => '341',
                'codigo' => '408',
                'nome' => 'BÔNUSCRED SOCIEDADE DE CRÉDITO DIRETO S.A.',
                'codigo_ispb' => '36586946',
            ],
            273 =>
            [
                'id' => '342',
                'codigo' => '404',
                'nome' => 'SUMUP SOCIEDADE DE CRÉDITO DIRETO S.A.',
                'codigo_ispb' => '37241230',
            ],
            274 =>
            [
                'id' => '343',
                'codigo' => '403',
                'nome' => 'CORA SOCIEDADE DE CRÉDITO DIRETO S.A.',
                'codigo_ispb' => '37880206',
            ],
            275 =>
            [
                'id' => '344',
                'codigo' => '306',
                'nome' => 'PORTOPAR DISTRIBUIDORA DE TITULOS E VALORES MOBILIARIOS LTDA.',
                'codigo_ispb' => '40303299',
            ],
            276 =>
            [
                'id' => '345',
                'codigo' => '174',
                'nome' => 'PEFISA S.A. – CRÉDITO, FINANCIAMENTO E INVESTIMENTO',
                'codigo_ispb' => '43180355',
            ],
            277 =>
            [
                'id' => '346',
                'codigo' => '354',
                'nome' => 'NECTON INVESTIMENTOS S.A. CORRETORA DE VALORES MOBILIÁRIOS E COMMODITIES',
                'codigo_ispb' => '52904364',
            ],
            278 =>
            [
                'id' => '347',
                'codigo' => '0',
                'nome' => 'BMF Bovespa S.A. – Bolsa de Valores, Mercadorias e Futuros – Camara BMFBOVESPA',
                'codigo_ispb' => '54641030',
            ],
            279 =>
            [
                'id' => '348',
                'codigo' => '630',
                'nome' => 'Banco Smartbank S.A.',
                'codigo_ispb' => '58497702',
            ],
            280 =>
            [
                'id' => '349',
                'codigo' => '393',
                'nome' => 'Banco Volkswagen S.A.',
                'codigo_ispb' => '59109165',
            ],
            281 =>
            [
                'id' => '350',
                'codigo' => '390',
                'nome' => 'BANCO GM S.A.',
                'codigo_ispb' => '59274605',
            ],
            282 =>
            [
                'id' => '351',
                'codigo' => '381',
                'nome' => 'BANCO MERCEDES-BENZ DO BRASIL S.A.',
                'codigo_ispb' => '60814191',
            ],
            283 =>
            [
                'id' => '352',
                'codigo' => '626',
                'nome' => 'BANCO C6 CONSIGNADO S.A.',
                'codigo_ispb' => '61348538',
            ],
            284 =>
            [
                'id' => '353',
                'codigo' => '755',
                'nome' => 'Bank of America Merrill Lynch Banco Múltiplo S.A.',
                'codigo_ispb' => '62073200',
            ],
            285 =>
            [
                'id' => '354',
                'codigo' => '89',
                'nome' => 'CREDISAN COOPERATIVA DE CRÉDITO',
                'codigo_ispb' => '62109566',
            ],
            286 =>
            [
                'id' => '355',
                'codigo' => '363',
                'nome' => 'SOCOPA SOCIEDADE CORRETORA PAULISTA S.A.',
                'codigo_ispb' => '62285390',
            ],
            287 =>
            [
                'id' => '356',
                'codigo' => '365',
                'nome' => 'SOLIDUS S.A. CORRETORA DE CAMBIO E VALORES MOBILIARIOS',
                'codigo_ispb' => '68757681',
            ],
            288 =>
            [
                'id' => '357',
                'codigo' => '281',
                'nome' => 'Cooperativa de Crédito Rural Coopavel',
                'codigo_ispb' => '76461557',
            ],
            289 =>
            [
                'id' => '358',
                'codigo' => '654',
                'nome' => 'BANCO DIGIMAIS S.A.',
                'codigo_ispb' => '76461557',
            ],
            290 =>
            [
                'id' => '359',
                'codigo' => '371',
                'nome' => 'WARREN CORRETORA DE VALORES MOBILIÁRIOS E CÂMBIO LTDA.',
                'codigo_ispb' => '92875780',
            ],
            291 =>
            [
                'id' => '360',
                'codigo' => '289',
                'nome' => 'DECYSEO CORRETORA DE CAMBIO LTDA.',
                'codigo_ispb' => '94968518',
            ],
        ];

        foreach ($params as $param) {
            Banco::create($param);
        }
    }
}
