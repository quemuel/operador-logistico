<?php

use App\Models\Block;
use App\Models\Condo;
use App\Models\Garage;
use App\Models\Pavement;
use App\Models\Unit;
use Illuminate\Database\Seeder;

class PracaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Condo::create([
        //     'url' => 'condominio2',
        //     'fantasy_name' => 'outro condomínio do admin',
        //     'partner_id' => 1,
        //     'logo_url' => 'https://via.placeholder.com/300',
        //     'cpf_cnpj' => '32596808021',
        //     'social_reason' => 'admin 1 ltda',
        //     'latitude' => -15.5411067,
        //     'longitude' => -47.340538,
        //     'type' => Condo::TYPE['FISICA'],
        //     'state_registration_exemption' => true,
        //     'state_registration_indicator' => Condo::STATE_REGISTRATION_INDICATOR['ISENTO'],
        //     'simple_national_opting' => false,
        //     'state_registration' => null,
        //     'municipal_registration' => null,
        //     'suframa_registration' => null,
        //     'fundation_date' => null,
        //     'active' => true,
        //     'sms_opting' => false,
        //     'observation' => 'condominio cadastrado para servir ao admin',
        // ]);

        // Condo::create([
        //     'url' => 'condominio3',
        //     'fantasy_name' => 'Condomínio do parceiro 2',
        //     'partner_id' => 2,
        //     'logo_url' => 'https://via.placeholder.com/300',
        //     'cpf_cnpj' => '79719137029',
        //     'social_reason' => 'parceiro 2 ltda',
        //     'latitude' => -15.5411067,
        //     'longitude' => -47.340538,
        //     'type' => Condo::TYPE['FISICA'],
        //     'state_registration_exemption' => true,
        //     'state_registration_indicator' => Condo::STATE_REGISTRATION_INDICATOR['ISENTO'],
        //     'simple_national_opting' => false,
        //     'state_registration' => null,
        //     'municipal_registration' => null,
        //     'suframa_registration' => null,
        //     'fundation_date' => null,
        //     'active' => true,
        //     'sms_opting' => false,
        //     'observation' => 'condominio cadastrado para servir ao parceiro 2',
        // ]);
    }
}
