<?php

namespace Database\Seeders;

use App\Models\Parametro;
use App\Models\Praca;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;

class ParametroTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $parametros = new Collection();

        $pracaSalvador = Praca::where('descricao', 'Salvador')->first();
        if($pracaSalvador)
            $parametros->push([
                'key' => 'PRACA_SALVADOR_ID',
                'value' => $pracaSalvador->id
            ]);
        $pracaAracaju = Praca::where('descricao', 'Aracajú')->first();
        if($pracaAracaju)
            $parametros->push([
                'key' => 'PRACA_ARACAJU_ID',
                'value' => $pracaAracaju->id
            ]);

        foreach ($parametros as $parametro) {
            Parametro::create($parametro);
        }
    }
}
