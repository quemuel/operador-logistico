"use strict";

// Class definition
var KTUserAdd = function () {
	// Base elements
	var wizardEl;
	var formEl;
	var validator;
	var wizard;
	var avatar;

	// Private functions
	var initWizard = function () {
		// Initialize form wizard
		wizard = new KTWizard('kt_user_add_user', {
			startStep: 1, // initial active step number
			clickableSteps: true  // allow step clicking
		});

		// Validation before going to next page
		wizard.on('beforeNext', function (wizardObj) {
			/*formEl.data("validator").settings.errorPlacement = function (error, element) {
				//Custom position: first name
				if (element.hasClass("erro_in_table")) {
					error.insertAfter(element);
					element.parent().addClass();
					//error.appendTo(element.parent());
					//error.appendTo(element.parent().next("label"));
					//error.insertBefore(element);
				}
				// Default position: if no match is met (other fields)
				else {
					//error.append($('.errorTxt span'));
				}
			}*/
			var settings = formEl.validate().settings;
			if (wizardObj.currentStep == 1) {
			} else if (wizardObj.currentStep == 2) {
				$('#email_ema_nom_email').rules('remove', 'required');
				$('#telefone_tel_num_telefone').rules('remove', 'required');
				settings.ignore = ":hidden:not(#email_item_adicionado, #telefone_item_adicionado)";
			} else if (wizardObj.currentStep == 3) {
				$('#endereco_end_num_cep').rules('remove', 'required');
				$('#endereco_end_nom_logradouro').rules('remove', 'required');
				$('#endereco_end_nom_bairro').rules('remove', 'required');
				$('#endereco_end_nom_cidade').rules('remove', 'required');
				$('#endereco_end_nom_estado').rules('remove', 'required');
				settings.ignore = ":hidden:not(#endereco_item_adicionado)";
			} else if (wizardObj.currentStep == 4) {
				$('#dadosEclesiasticoViewModel_funcaoDadosEclesiastico_fde_id_fun').rules('remove', 'required');
				$('#dadosEclesiasticoViewModel_tituloDadosEclesiastico_tde_id_tit').rules('remove', 'required');
				settings.ignore = ":hidden:not(#dadosEclesiasticoViewModel_tituloDadosEclesiastico_item_adicionado, #dadosEclesiasticoViewModel_funcaoDadosEclesiastico_item_adicionado)";
			} else if (wizardObj.currentStep == 5) {
				$('#parentesco_prt_id_tpa').rules('remove', 'required');
				$('#parentesco_prt_id_pes_parentesco').rules('remove', 'required');
				$('#parentesco_prt_nom_parente').rules('remove', 'required');
				settings.ignore = ":hidden:not(#parentesco_item_adicionado)";
			} else if (wizardObj.currentStep == 6) {
			}
			// Modify validation settings
			/*$.extend(true, settings, {
				rules: {
					// Remove validation of username
					"email.ema_nom_email": {}
				}
			});*/
			var formIsValid = validator.form();
			var a = validator.errorList;
			if (formIsValid !== true) {
				wizardObj.stop();  // don't go to the next step
			}
		});		

		// Change event
		wizard.on('change', function(wizard) {
			KTUtil.scrollTop();
		});
	}
	var initValidation = function() {
		validator = formEl.validate({
			// Validate only visible fields
			//ignore: "",
			//ignore: ":hidden:not(#email_listar_email_preenchido)",
			/*rules: {
				listar_email: {
					required: true
				}
			},*/
			// Validation rules
			/*rules: {
				// Step 1
				profile_avatar: {
					//required: true
				},
				profile_first_name: {
					required: true
				},
				profile_last_name: {
					required: true
				},
				profile_phone: {
					required: true
				},
				profile_email: {
					required: true,
					email: true
				}
			},*/
			/*ignore: function (index, el) {
				//console.log(el.getAttribute("name"));
				var $el = $(el);

				if ($el.hasClass('always-validate')) {
					return false;
				}

				// Default behavior
				return $el.is(':hidden');
			}, */
			// Display error
			invalidHandler: function (event, validator) {
				var errors = validator.numberOfInvalids();
				var a = validator.errorList;
				KTUtil.scrollTop();

				swal.fire({
					"title": "",
					"text": "There are some errors in your submission. Please correct them.",
					"type": "error",
					"buttonStyling": false,
					"confirmButtonClass": "btn btn-brand btn-sm btn-bold"
				});
			},
			/*errorPlacement: function (error, element) {
				//Custom position: first name
				if (element.hasClass("erro_in_table")) {
					error.insertAfter(element);
					//error.appendTo(element.parent());
					//error.appendTo(element.parent().next("label"));
					//error.insertBefore(element);
				}
				// Default position: if no match is met (other fields)
				else {
					error.append($('.errorTxt span'));
				}
			},*/

			// Submit valid form
			submitHandler: function (form) {}
		});
	}

	var initSubmit = function() {
		var btn = formEl.find('[data-ktwizard-type="action-submit"]');

		btn.on('click', function(e) {
			e.preventDefault();

			//if (validator.form()) {
				// See: src\js\framework\base\app.js
				//KTApp.progress(btn);
				//KTApp.block(formEl);

				// See: http://malsup.com/jquery/form/#ajaxSubmit
				formEl.ajaxSubmit({
					success: function() {
						//KTApp.unprogress(btn);
						//KTApp.unblock(formEl);

						swal.fire({
							"title": "",
							"text": "The application has been successfully submitted!",
							"type": "success",
							"confirmButtonClass": "btn btn-secondary"
						});
					}
				});
			//}
		});
	}

	var initUserForm = function() {
		avatar = new KTAvatar('kt_user_add_avatar');
	}

	return {
		// public functions
		init: function() {
			formEl = $('#kt_user_add_form');

			initWizard();
			initValidation();
			initSubmit();
			initUserForm();
		}
	};
}();

jQuery(document).ready(function() {
	KTUserAdd.init();
});
