﻿ /*(function ($) {   
    jQuery.validator.addMethod("denominacaorequired_validator", function (value, element, params) {
        var otherpropertyvalue = $('#' + $(element).data("otherproperty")).val();

        if (otherpropertyvalue == ID_RELIGIAO_PROTESTANTISIMO) {
            if (value == '') {
                return false;
            }
        }
        return true;
    });
    jQuery.validator.unobtrusive.adapters.addBool('denominacaorequired', 'denominacaorequired_validator');

    jQuery.validator.addMethod("datacasamentorequired_validator", function (value, element, params) {
        if (this.optional(element)) return true;
        var otherpropertyvalue = $('#' + $(element).data("otherproperty")).val();

        if (otherpropertyvalue == ID_ESTADO_CIVIL_CASADO) {
            if (value == '') {
                return false;
            }
        }
        return true;
    });
    jQuery.validator.unobtrusive.adapters.addBool('datacasamentorequired', 'datacasamentorequired_validator');
});
*/
$(document).ready(function () {
    var formEl = $('#kt_user_add_form');
    var settings = formEl.validate().settings;
    $("#btn-add-email").click(function () {
        settings.ignore = ":hidden:not(#email_item_adicionado, #telefone_item_adicionado)";
        $('#email_ema_nom_email').rules('add', { required: true });
        if ($("#email_ema_nom_email").valid()) {
            $.post(urlAddEmail, { "ema_nom_email": $("#email_ema_nom_email").val() }, function (data) {
                $('#email_ema_nom_email').rules('remove', 'required');
                $("#email_ema_nom_email").val("");
                var datahtml = $.parseHTML(data);
                $("#email_item_adicionado").val($(datahtml).filter("#email_count").val());
                $("#email_item_adicionado").valid();
                $("#div_table_emails").html(data);
            });
        }
    });
    $("#btn-add-telefone").click(function () {
        settings.ignore = ":hidden:not(#email_item_adicionado, #telefone_item_adicionado)";
        $('#telefone_tel_num_telefone').rules('add', { required: true });
        if ($("#telefone_tel_num_telefone").valid()) {
            $.post(urlAddTelefone, { "tel_num_telefone": $("#telefone_tel_num_telefone").val() }, function (data) {
                $('#telefone_tel_num_telefone').rules('remove', 'required');
                $("#telefone_tel_num_telefone").val("");
                var datahtml = $.parseHTML(data);
                $("#telefone_item_adicionado").val($(datahtml).filter("#telefone_count").val());
                $("#telefone_item_adicionado").valid();
                $("#div_table_telefones").html(data);
            });
        }
    });
    $("#btn-add-endereco").click(function () {
        settings.ignore = ":hidden:not(#endereco_item_adicionado)";
        $('#endereco_end_num_cep').rules('add', { required: true });
        $('#endereco_end_nom_logradouro').rules('add', { required: true });
        $('#endereco_end_nom_bairro').rules('add', { required: true });
        $('#endereco_end_nom_cidade').rules('add', { required: true });
        $('#endereco_end_nom_estado').rules('add', { required: true });
        if ($("#endereco_end_num_cep, #endereco_end_nom_logradouro, #endereco_end_nom_bairro, #endereco_end_nom_cidade, #endereco_end_nom_estado").valid()) {
            $.post(urlAddEndereco, {
                "endereco.end_num_cep": $("#endereco_end_num_cep").val(),
                "endereco.end_nom_logradouro": $("#endereco_end_nom_logradouro").val(),
                "endereco.end_flg_sem_numero": $("#endereco_end_flg_sem_numero").is(":checked"),
                "endereco.end_num_numero": $("#endereco_end_num_numero").val(),
                "endereco.end_nom_bairro": $("#endereco_end_nom_bairro").val(),
                "endereco.end_nom_complemento": $("#endereco_end_nom_complemento").val(),
                "endereco.end_nom_cidade": $("#endereco_end_nom_cidade").val(),
                "endereco.end_nom_estado": $("#endereco_end_nom_estado").val()
            }, function (data) {
                $('#endereco_end_num_cep').rules('remove', 'required');
                $('#endereco_end_nom_logradouro').rules('remove', 'required');
                $('#endereco_end_nom_bairro').rules('remove', 'required');
                $('#endereco_end_nom_cidade').rules('remove', 'required');
                $('#endereco_end_nom_estado').rules('remove', 'required');

                $("#endereco_end_num_cep").val("");
                $("#endereco_end_nom_logradouro").val("");
                $("#endereco_end_flg_sem_numero").prop("checked", false);
                $("#endereco_end_num_numero").removeAttr("disabled");
                $("#endereco_end_num_numero").val("");
                $("#endereco_end_nom_bairro").val("");
                $("#endereco_end_nom_complemento").val("");
                $("#endereco_end_nom_cidade").val("");
                $("#endereco_end_nom_estado").val("");
                
                var datahtml = $.parseHTML(data);
                $("#endereco_item_adicionado").val($(datahtml).filter("#endereco_count").val());
                $("#endereco_item_adicionado").valid();

                $("#div_table_enderecos").html(data);
            });
        }
    });

    $("#btn-add-titulo").click(function () {
    settings.ignore = ":hidden:not(#dadosEclesiasticoViewModel_tituloDadosEclesiastico_item_adicionado, #dadosEclesiasticoViewModel_funcaoDadosEclesiastico_item_adicionado)";
        $('#dadosEclesiasticoViewModel_tituloDadosEclesiastico_tde_id_tit').rules('add', { required: true });
        if ($("#dadosEclesiasticoViewModel_tituloDadosEclesiastico_tde_id_tit").valid()) {
            $.post(urlAddTitulo, { "tituloDadosEclesiastico.tde_id_tit": $("#dadosEclesiasticoViewModel_tituloDadosEclesiastico_tde_id_tit").val() }, function (data) {
                $('#dadosEclesiasticoViewModel_tituloDadosEclesiastico_tde_id_tit').rules('remove', 'required');
                $("#dadosEclesiasticoViewModel_tituloDadosEclesiastico_tde_id_tit").val("");
                var datahtml = $.parseHTML(data);
                $("#dadosEclesiasticoViewModel_tituloDadosEclesiastico_item_adicionado").val($(datahtml).filter("#titulo_count").val());
                $("#dadosEclesiasticoViewModel_tituloDadosEclesiastico_item_adicionado").valid();

                $("#div_table_titulos").html(data);
            });
        }
    }); 

    $("#btn-add-funcao").click(function () {
    settings.ignore = ":hidden:not(#dadosEclesiasticoViewModel_tituloDadosEclesiastico_item_adicionado, #dadosEclesiasticoViewModel_funcaoDadosEclesiastico_item_adicionado)";
        $('#dadosEclesiasticoViewModel_funcaoDadosEclesiastico_fde_id_fun').rules('add', { required: true });
        if ($("#dadosEclesiasticoViewModel_funcaoDadosEclesiastico_fde_id_fun").valid()) {
            $.post(urlAddFuncao, { "funcaoDadosEclesiastico.fde_id_fun": $("#dadosEclesiasticoViewModel_funcaoDadosEclesiastico_fde_id_fun").val() }, function (data) {
                $('#dadosEclesiasticoViewModel_funcaoDadosEclesiastico_fde_id_fun').rules('remove', 'required');
                $("#dadosEclesiasticoViewModel_funcaoDadosEclesiastico_fde_id_fun").val("");
                var datahtml = $.parseHTML(data);
                $("#dadosEclesiasticoViewModel_funcaoDadosEclesiastico_item_adicionado").val($(datahtml).filter("#funcao_count").val());
                $("#dadosEclesiasticoViewModel_funcaoDadosEclesiastico_item_adicionado").valid();

                $("#div_table_funcoes").html(data);
            });
        }
    });

    $("#btn-add-parentesco").click(function () {
        $('#parentesco_prt_id_tpa').rules('add', { required: true });
        if ($('#parentesco_prt_id_tpa,#parentesco_prt_id_pes_parentesco,#parentesco_prt_nom_parente').valid()) {
            $.post(urlAddParentesco, {
                "parentesco.prt_id_tpa": $("#parentesco_prt_id_tpa").val(),
                "parentesco.prt_id_pes_parentesco": $("#parentesco_prt_id_pes_parentesco").val(),
                "parentesco.prt_nom_parente": $("#parentesco_prt_nom_parente").val(),
            }, function (data) {
                settings.ignore = ":hidden:not(#parentesco_item_adicionado)";
                $('#parentesco_prt_id_tpa,#parentesco_prt_id_pes_parentesco,#parentesco_prt_nom_parente').rules('remove', 'required');

                $("#parentesco_prt_id_tpa").val("");
                $("#parentesco_prt_id_pes_parentesco").val("");
                $("#parentesco_prt_nom_parente").val("");

                var datahtml = $.parseHTML(data);
                $("#parentesco_item_adicionado").val($(datahtml).filter("#parentesco_count").val());
                $("#parentesco_item_adicionado").valid();

                $("#div_table_parentescos").html(data);
            });
        }
    });
    $("#pessoa_pes_id_eci").change(function () {
        if ($(this).val() == ID_ESTADO_CIVIL_CASADO) {
            $("#div_dat_casamento").show();
        } else {
            $("#div_dat_casamento").hide();
        }
    });
    $("#dadosEclesiasticoViewModel_dadosEclesiastico_dec_id_rel_origem_religiosa").change(function () {
        if ($(this).val() == ID_RELIGIAO_PROTESTANTISIMO) {
            $("#div_denominacao_origem_religiosa").show();
        } else {
            $("#div_denominacao_origem_religiosa").hide();
        }
    });
    $("#dadosEclesiasticoViewModel_dadosEclesiastico_dec_id_rel_ultima").change(function () {
        if ($(this).val() == ID_RELIGIAO_PROTESTANTISIMO) {
            $("#div_denominacao_ultima").show();
        } else {
            $("#div_denominacao_ultima").hide();
        }
    });
    $("#pessoa_cadastrada").change(function () {
        if ($(this).is(":checked")) {
            $("#div_name_parentesco").hide();
            $("#div_pessoa_parentesco").show();
        } else {
            $("#div_name_parentesco").show();
            $("#div_pessoa_parentesco").hide();
        }
    });
    $("#endereco_end_flg_sem_numero").change(function () {
        if ($(this).is(":checked")) {
            $("#endereco_end_num_numero").prop("disabled", true);
        } else {
            $("#endereco_end_num_numero").prop("disabled", false);
        }
    });
});

function btnRemoverEmail(index) {
    $.post(urlRemoverEmail, { "index": index }, function (data) {
        var datahtml = $.parseHTML(data);
        $("#email_item_adicionado").val($(datahtml).filter("#email_count").val());
        $('#email_item_adicionado').valid();
        $("#div_table_emails").html(data);
    });
}
function btnRemoverTelefone(index) {
    $.post(urlRemoverTelefone, { "index": index }, function (data) {
        var datahtml = $.parseHTML(data);
        $("#telefone_item_adicionado").val($(datahtml).filter("#telefone_count").val());
        $('#telefone_item_adicionado').valid();
        $("#div_table_telefones").html(data);
    });
}
function btnRemoverEndereco(index) {
    $.post(urlRemoverEndereco, { "index": index }, function (data) {
        var datahtml = $.parseHTML(data);
        $("#endereco_item_adicionado").val($(datahtml).filter("#endereco_count").val());
        $('#endereco_item_adicionado').valid();        
        $("#div_table_enderecos").html(data);
    });
}
function btnRemoverTitulo(index) {
    $.post(urlRemoverTitulo, { "index": index }, function (data) {
        var datahtml = $.parseHTML(data);
        $("#dadosEclesiasticoViewModel_tituloDadosEclesiastico_item_adicionado").val($(datahtml).filter("#titulo_count").val());
        $('#dadosEclesiasticoViewModel_tituloDadosEclesiastico_item_adicionado').valid();  
        $("#div_table_titulos").html(data);
    });
}
function btnRemoverFuncao(index) {
    $.post(urlRemoverFuncao, { "index": index }, function (data) {
        var datahtml = $.parseHTML(data);
        $("#dadosEclesiasticoViewModel_funcaoDadosEclesiastico_item_adicionado").val($(datahtml).filter("#funcao_count").val());
        $('#dadosEclesiasticoViewModel_funcaoDadosEclesiastico_item_adicionado').valid();        
        $("#div_table_funcoes").html(data);
    });
}
function btnRemoverParentesco(index) {
    $.post(urlRemoverParentesco, { "index": index }, function (data) {
        var datahtml = $.parseHTML(data);
        $("#parentesco_item_adicionado").val($(datahtml).filter("#parentesco_count").val());
        $('#parentesco_item_adicionado').valid();        
        $("#div_table_parentescos").html(data);
    });
}