jQuery.extend(jQuery.validator.messages, {
	required: "Este campo � requerido.",
	remote: "Por favor, corrija este campo.",
	email: "Por favor, forne�a um endere�o de email v�lido.",
	url: "Por favor, forne�a uma URL v�lida.",
	date: "Por favor, forne�a uma data v�lida.",
	dateISO: "Por favor, forne�a uma data v�lida (ISO).",
	number: "Por favor, forne�a um n&uacute;mero v�lido.",
	digits: "Por favor, forne�a somente d�gitos.",
	creditcard: "Por favor, forne�a um cart�o de cr�dito v�lido.",
	equalTo: "Por favor, forne�a o mesmo valor novamente.",
	accept: "Por favor, forne�a um valor com uma extens�o v�lida.",
	maxlength: jQuery.validator.format("Por favor, forne�a n�o mais que {0} caracteres."),
	minlength: jQuery.validator.format("Por favor, forne�a ao menos {0} caracteres."),
	rangelength: jQuery.validator.format("Por favor, forne�a um valor entre {0} e {1} caracteres de comprimento."),
	range: jQuery.validator.format("Por favor, forne�a um valor entre {0} e {1}."),
	max: jQuery.validator.format("Por favor, forne�a um valor menor ou igual a {0}."),
	min: jQuery.validator.format("Por favor, forne�a um valor maior ou igual a {0}.")
});

$.validator.setDefaults({
	highlight: function (element) {
		if ($(element).hasClass("erro_in_parent") || $(element).parent('.input-group').length) {
			$(element).parent().addClass('is-invalid');
			$(element).parent().removeClass('is-valid');		
		}
		$(element).addClass('is-invalid');
		$(element).removeClass('is-valid');
	},
	unhighlight: function (element) {
		if ($(element).hasClass("erro_in_parent") || $(element).parent('.input-group').length) {
			$(element).parent().addClass('is-valid');
			$(element).parent().removeClass('is-invalid');
		}
		$(element).addClass('is-valid');
		$(element).removeClass('is-invalid');
	},
	errorElement: 'span',
	errorClass: 'invalid-feedback',
	errorPlacement: function (error, element) {
		/*if (element.hasClass("erro_in_table")) {
			error.insertAfter(element.parent());
		} else*/
		if (element.parent('.input-group').length) {
			error.insertAfter(element.parent());
		} else {
			error.insertAfter(element);
		}
	}
});