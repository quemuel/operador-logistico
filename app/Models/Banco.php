<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int    $codigo
 * @property int    $codigo_ispb
 * @property string $nome
 */
class Banco extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'banco';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'codigo', 'nome', 'codigo_ispb'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'codigo' => 'int', 'nome' => 'string', 'codigo_ispb' => 'int'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...

    // Relations ...
}
