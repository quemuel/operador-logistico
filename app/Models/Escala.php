<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $entregador_id
 * @property int $periodo_escala_id
 * @property int $area_entrega_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class Escala extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'escala';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'entregador_id', 'periodo_escala_id', 'area_entrega_id', 'created_at', 'updated_at'
    ];
    /*protected $fillable = [
        'entregador_id', 'periodo_escala_id', 'turno_id', 'created_at', 'updated_at'
    ];*/

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'entregador_id' => 'int', 'periodo_escala_id' => 'int', 'area_entrega_id' => 'int', 'created_at' => 'timestamp', 'updated_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    public function turnos()
    {
        return $this->morphToMany(
            Turno::class, 
            'et_morph',     //name for the morphable
            'escala_turno',    //pivot table
        );
    }

    public function escalaTurnos()
    {
        return $this->morphMany(
            EscalaTurno::class, 
            'et_morph',     //name for the morphable
        );
    }

    
    /*public function dias()
    {
        return $this->morphToMany(
            Dia::class, 
            'ds_morph',     //name for the morphable
            'dias_semanal',    //pivot table
        );
        return $this->morphToMany(
            Location::class, 
            'locationable',     //name for the morphable
            'locationables',    //pivot table
            'locationable_id',  //foreign key on the pivot table to identify this model record
            'location_id',      //foreign key on the pivot table to identify related model record
            'account_id',       //primary key column name for this model's table
            'id'                //primary key column name for related model's table
        );
    }
    */

    public function entregador()
    {
        return $this->hasOne('App\Models\Entregador', 'id','entregador_id');
    }
    
    public function periodoEscala()
    {
        return $this->hasOne('App\Models\PeriodoEscala', 'id','periodo_escala_id');
    }
    
    public function areaEntrega()
    {
        return $this->hasOne('App\Models\AreaEntrega', 'id','area_entrega_id');
    }
}
