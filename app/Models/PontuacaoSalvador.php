<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PontuacaoSalvador
 * 
 * @property string|null $praca
 * @property Carbon|null $dia_data
 * @property string|null $entregador
 * @property int $qtd_turnos
 * @property float|null $qtd_rejeitadas
 * @property float|null $qtd_cancelamento
 * @property float|null $tempo_online
 * @property int|null $pto_rejeite
 * @property int|null $pto_cancel
 * @property int|null $pto_tempo_online
 * @property int $pto_turno
 *
 * @package App\Models
 */
class PontuacaoSalvador extends Model
{
	protected $table = 'pontuacao_salvador';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'qtd_turnos' => 'int',
		'qtd_rejeitadas' => 'float',
		'qtd_cancelamento' => 'float',
		'tempo_online' => 'float',
		'pto_rejeite' => 'int',
		'pto_cancel' => 'int',
		'pto_tempo_online' => 'int',
		'pto_turno' => 'int'
	];

	protected $dates = [
		'dia_data'
	];

	protected $fillable = [
		'praca',
		'dia_data',
		'entregador',
		'qtd_turnos',
		'qtd_rejeitadas',
		'qtd_cancelamento',
		'tempo_online',
		'pto_rejeite',
		'pto_cancel',
		'pto_tempo_online',
		'pto_turno'
	];
}
