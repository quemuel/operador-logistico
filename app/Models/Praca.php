<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Praca
 * 
 * @property string $descricao
 *
 * @package App\Models
 */
class Praca extends Model
{
	protected $table = 'praca';
	public $timestamps = false;

	protected $fillable = [
		'descricao'
	];
}
