<?php

namespace App\Models;

/**
 * Class FiltroDashboard
 * 
 * @property bool|null $tipoFiltroSemanal
 * @property array|null $semanasAnos
 * @property string|null $nomePraca
 * @property string|null $dataInicial
 * @property string|null $dataFinal
 * 
 */
class FiltroDashboard
{
    public function __construct(array $attributes)
    {
		//isset($attributes['tipoFiltroSemanal']) caso nao exista siginifica que o tipo padrao eh sempre semanal, entao eh igual true
        $this->tipoFiltroSemanal = isset($attributes['tipoFiltroSemanal']) ? filter_var($attributes['tipoFiltroSemanal'], FILTER_VALIDATE_BOOLEAN) : true;
        $this->semanasAnos = isset($attributes['semanasAnos']) ? $attributes['semanasAnos'] : [];
        $this->dataInicial = isset($attributes['dataInicial']) ? $attributes['dataInicial'] : null;
        $this->dataFinal = isset($attributes['dataFinal']) ? $attributes['dataFinal'] : null;
        $this->nomePraca = isset($attributes['nomePraca']) ? $attributes['nomePraca'] : null;
    }

	public $tipoFiltroSemanal;
	public $semanasAnos = [];
	public $nomePraca;
	public $dataInicial;
	public $dataFinal;
}
