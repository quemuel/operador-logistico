<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Classificacao
 * 
 * @property int|null $semana_ano
 * @property string|null $semana_anterior
 * @property string|null $semana_atual
 * @property Carbon|null $data_ini_semana
 * @property Carbon|null $data_fim_semana
 * @property string|null $entregador
 * @property string|null $praca
 * @property float|null $qtd_ofertadas
 * @property float|null $qtd_aceitas
 * @property float|null $qtd_completadas
 * @property float|null $qtd_rejeitadas
 * @property float|null $tempo_online
 * @property float|null $rejeite
 * @property string|null $classificacao
 *
 * @package App\Models
 */
class Classificacao extends Model
{
	protected $table = 'classificacao';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'semana_ano' => 'int',
		'qtd_ofertadas' => 'float',
		'qtd_aceitas' => 'float',
		'qtd_completadas' => 'float',
		'qtd_rejeitadas' => 'float',
		'tempo_online' => 'float',
		'rejeite' => 'float'
	];

	protected $dates = [
		'data_ini_semana',
		'data_fim_semana'
	];

	protected $fillable = [
		'semana_ano',
		'semana_anterior',
		'semana_atual',
		'data_ini_semana',
		'data_fim_semana',
		'entregador',
		'praca',
		'qtd_ofertadas',
		'qtd_aceitas',
		'qtd_completadas',
		'qtd_rejeitadas',
		'tempo_online',
		'rejeite',
		'classificacao'
	];
}
