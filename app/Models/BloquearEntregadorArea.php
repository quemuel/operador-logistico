<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $entregador_id
 * @property int $area_entrega_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class BloquearEntregadorArea extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bloquear_entregador_area';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'entregador_id', 'area_entrega_id', 'created_at', 'updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'entregador_id' => 'int', 'area_entrega_id' => 'int', 'created_at' => 'timestamp', 'updated_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    public function entregador()
    {
        return $this->hasOne('App\Models\Entregador', 'id','entregador_id');
    }
    public function areaEntrega()
    {
        return $this->hasOne('App\Models\AreaEntrega', 'id','area_entrega');
    }
}
