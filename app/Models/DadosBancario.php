<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * @property string $municipio
 * @property string $nome
 * @property string $cpf
 * @property string $pix
 * @property string $agencia
 * @property string $conta
 * @property int    $created_at
 * @property int    $updated_at
 * @property int    $banco_id
 */
class DadosBancario extends Model
{
    use HasFactory, Notifiable;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dados_bancario';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'municipio', 'nome', 'cpf', 'pix', 'agencia', 'conta', 'created_at', 'updated_at', 'banco_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'municipio' => 'string', 'nome' => 'string', 'cpf' => 'string', 'pix' => 'string', 'agencia' => 'string', 'conta' => 'string', 'created_at' => 'timestamp', 'updated_at' => 'timestamp', 'banco_id' => 'int'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = true;

    // Scopes...

    // Functions ...

    // Relations ...

    public function banco()
    {
        return $this->hasOne('App\Models\Banco', 'id','banco_id');
    }
}
