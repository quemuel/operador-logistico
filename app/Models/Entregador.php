<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Entregador
 * 
 * @property string $nome_completo
 * @property string  $cpf
 * @property string $email
 * @property string $telefone
 * @property int $praca_id
 * @property boolean $ativo
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class Entregador extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'entregador';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome_completo', 'cpf', 'email', 'telefone', 'praca_id', 'ativo', 'created_at', 'updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nome_completo' => 'string', 
        'cpf' => 'string', 
        'email' => 'string', 
        'telefone' => 'string', 
        'praca_id' => 'int', 
        'ativo' => 'boolean', 
        'created_at' => 'timestamp', 
        'updated_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    public function praca()
    {
        return $this->hasOne('App\Models\Praca', 'id','praca_id');
    }

    public function escalas() 
    {
        return $this->hasMany('App\Models\Escala');
    }
}
