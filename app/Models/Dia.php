<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $nome
 * @property int $codigo
 * @property int $codigo2
 */
class Dia extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dia';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome', 'codigo', 'codigo2'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nome' => 'string',
        'codigo' => 'int',
        'codigo2' => 'int'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;
}
