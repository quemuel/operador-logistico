<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TbDado
 * 
 * @property Carbon|null $data
 * @property string|null $turno
 * @property string|null $tag
 * @property int|null $id do entregador
 * @property string|null $entregador
 * @property string|null $praca
 * @property string|null $sub praca
 * @property string|null $tempo online
 * @property string|null $tempo online absoluto
 * @property int|null $numero de pedidos aceitos e completados
 * @property int|null $numero de corridas aceitas
 * @property int|null $numero de corridas completadas
 * @property int|null $numero de corridas ofertadas
 * @property int|null $numero de corridas rejeitadas
 * @property string|null $soma das taxas das corridas aceitas
 *
 * @package App\Models
 */
class TbDado extends Model
{
	protected $table = 'tb_dados';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'numero de pedidos aceitos e completados' => 'int',
		'numero de corridas aceitas' => 'int',
		'numero de corridas completadas' => 'int',
		'numero de corridas ofertadas' => 'int',
		'numero de corridas rejeitadas' => 'int',
	];

	protected $dates = [
		'data' => 'datetime:Y-m-d',
	];

	protected $fillable = [
		'data',
		'turno',
		'tag',
		'id do entregador',
		'entregador',
		'praca',
		'sub praca',
		'tempo online',
		'tempo online absoluto',
		'numero de pedidos aceitos e completados',
		'numero de corridas aceitas',
		'numero de corridas completadas',
		'numero de corridas ofertadas',
		'numero de corridas rejeitadas',
		'soma das taxas das corridas aceitas'
	];
}
