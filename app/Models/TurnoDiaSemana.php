<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TurnoDiaSemana
 * 
 * @property int $id
 * @property int $entregador_id
 * @property int $turno_id
 * @property string $dias_semana
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class TurnoDiaSemana extends Model
{
	protected $table = 'turno_dia_semana';

	protected $casts = [
		'entregador_id' => 'int',
		'turno_id' => 'int'
	];

	protected $fillable = [
		'entregador_id',
		'turno_id',
		'dias_semana'
	];
}
