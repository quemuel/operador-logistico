<?php
namespace App\Models\Dashboard;

class DadosSalvador
{
    public $nomePraca;
    public $dadosPraca;
    public $tempoOnlineModalidade;
    public $tempoOnlineModalidadeJson;
    public $dadosVeiculo; 
    public $tempoOnlineTurno;
    public $tempoOnlineTurnoJson;
    public $entregadoresTempoOnline;
    public $entregadoresRejeite; 
    public $entregadoresCancelamento;
}
