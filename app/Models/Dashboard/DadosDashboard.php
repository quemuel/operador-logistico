<?php
namespace App\Models\Dashboard;

use App\Models\FiltroDashboard;

/**
 * Class FiltroDashboard
 * 
 * @property string|null $semanaAnoAtual
 * @property string|null $salvador
 * @property string|null $aracaju
 * @property FiltroDashboard $filtroDashboard
 * 
 */
class DadosDashboard
{
    public $semanaAnoAtual;
    public $semanasAnos;
    public $salvador;
    public $aracaju; 
    public $filtroDashboard; 
}
