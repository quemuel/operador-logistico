<?php
namespace App\Models\Dashboard;

class DadosAracaju
{
    public $nomePraca;
    public $dadosPraca;
    public $tempoOnlineModalidade;
    public $tempoOnlineModalidadeJson;
    public $dadosVeiculo; 
    public $tempoOnlineTurno;
    public $tempoOnlineTurnoJson;
    public $entregadoresTempoOnline;
    public $entregadoresRejeite; 
    public $entregadoresCancelamento;
}
