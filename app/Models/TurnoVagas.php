<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $periodo_escala_id
 * @property int $area_entrega_id
 * @property int $turno_id
 * @property int $dia_id
 * @property int $qtd_vagas
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class TurnoVagas extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'turno_vagas';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'periodo_escala_id', 'area_entrega_id', 'turno_id', 'dia_id', 'qtd_vagas', 'created_at', 'updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'periodo_escala_id' => 'int', 'area_entrega_id' => 'int', 'turno_id' => 'int', 'dia_id' => 'int', 'qtd_vagas' => 'int', 'created_at' => 'timestamp', 'updated_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    public function periodoEscala()
    {
        return $this->hasOne('App\Models\PeriodoEscala', 'id','periodo_escala_id');
    }

    public function areaEntrega()
    {
        return $this->hasOne('App\Models\AreaEntrega', 'id','area_entrega_id');
    }

    public function turno()
    {
        return $this->hasOne('App\Models\Turno', 'id','turno_id');
    }

    public function dia()
    {
        return $this->hasOne('App\Models\Dia', 'id','dia_id');
    }
}
