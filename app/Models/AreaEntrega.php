<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AreaEntrega
 * 
 * @property int $id
 * @property string $descricao
 * @property int $praca_id
 * @property string $modalidade
 * @property bool $is_dedicado
 * @property string $codigo
 *
 * @package App\Models
 */
class AreaEntrega extends Model
{
	protected $table = 'area_entrega';
	public $timestamps = false;

	protected $casts = [
		'praca_id' => 'int'
	];

	protected $fillable = [
		'descricao',
		'praca_id',
		'modalidade',
		'is_dedicado',
		'codigo'
	];

    public function praca()
    {
        return $this->hasOne('App\Models\Praca', 'id','praca_id');
    }
}