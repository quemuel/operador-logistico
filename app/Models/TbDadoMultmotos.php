<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TbDado
 * 
 * @property Carbon|null $data
 * @property string|null $turno
 * @property string|null $tag
 * @property int|null $id do entregador
 * @property string|null $entregador
 * @property string|null $operador logistico
 * @property string|null $praca
 * @property string|null $tempo online
 * @property string|null $tempo online absoluto
 * @property string|null $tempo bloqueado (%)
 * @property string|null $tempo bloqueado (HH:MM)
 * @property int|null $numero de pedidos aceitos e completados
 * @property int|null $numero de corridas aceitas
 * @property int|null $numero de corridas completadas
 * @property int|null $numero de corridas ofertadas
 * @property int|null $numero de corridas rejeitadas
 * @property string|null $soma das taxas das corridas aceitas
 * @property string|null $soma das distancias das corridas aceitas (km)
 *
 * @package App\Models
 */
class TbDadoMultmotos extends Model
{
    protected $connection = 'mysql2';
	
	protected $table = 'tb_dados';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id do entregador' => 'int',
		'numero de pedidos aceitos e completados' => 'int',
		'numero de corridas aceitas' => 'int',
		'numero de corridas completadas' => 'int',
		'numero de corridas ofertadas' => 'int',
		'numero de corridas rejeitadas' => 'int',
		//'data' => 'datetime:Y-m-d',
	];

	protected $dates = [
		'data' => 'datetime:Y-m-d',
	];

	protected $fillable = [
		'data',
		'turno',
		'tag',
		'id do entregador',
		'entregador',
		'operador logistico',
		'praca',
		'tempo online',
		'tempo online absoluto',
		'tempo bloqueado (%)',
		'tempo bloqueado (HH:MM)',
		'numero de pedidos aceitos e completados',
		'numero de corridas aceitas',
		'numero de corridas completadas',
		'numero de corridas ofertadas',
		'numero de corridas rejeitadas',
		'soma das taxas das corridas aceitas',
		'soma das distancias das corridas aceitas (km)'
	];
}
