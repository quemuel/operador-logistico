<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SaldoTurnoVagas
 * 
 * @property int|null $periodo_escala_id
 * @property int|null $area_entrega_id
 * @property int|null $turno_id
 * @property int|null $dia_id
 * @property int|null $qtd_vagas
 * @property int|null $qtd_utilizadas
 * @property int|null $saldo_vagas
 * 
 * @package App\Models
 */

class SaldoTurnoVagas extends Model
{
	protected $table = 'saldo_turno_vagas';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'periodo_escala_id' => 'int',
		'area_entrega_id' => 'int',
		'turno_id' => 'int',
		'dia_id' => 'int',
		'qtd_vagas' => 'int',
		'qtd_utilizadas' => 'int',
		'saldo_vagas' => 'int'
	];

    public function periodoEscala()
    {
        return $this->hasOne('App\Models\PeriodoEscala', 'id','periodo_escala_id');
    }

    public function areaEntrega()
    {
        return $this->hasOne('App\Models\AreaEntrega', 'id','area_entrega_id');
    }

    public function turno()
    {
        return $this->hasOne('App\Models\Turno', 'id','turno_id');
    }

    public function dia()
    {
        return $this->hasOne('App\Models\Dia', 'id','dia_id');
    }
    
}

/* Script SQL

CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `saldo_turno_vagas` AS
    SELECT 
        `tv`.`periodo_escala_id` AS `periodo_escala_id`,
        `ae`.`id` AS `area_entrega_id`,
        `t`.`id` AS `turno_id`,
        IFNULL(`tv`.`qtd_vagas`, 0) AS `qtd_vagas`,
        SUM(CASE
            WHEN `ds`.`dia_id` IS NOT NULL THEN 1
            ELSE 0
        END) AS `qtd_utilizadas`,
        CASE
            WHEN
                `tv`.`qtd_vagas` IS NOT NULL
                    AND `tv`.`qtd_vagas` - SUM(CASE
                    WHEN `ds`.`dia_id` IS NOT NULL THEN 1
                    ELSE 0
                END) > 0
            THEN
                `tv`.`qtd_vagas` - SUM(CASE
                    WHEN `ds`.`dia_id` IS NOT NULL THEN 1
                    ELSE 0
                END)
            ELSE 0
        END AS `saldo_vagas`
    FROM
        (((((`area_entrega` `ae`
        JOIN `turno` `t` ON (`t`.`praca_id` = `ae`.`praca_id`
            AND `t`.`ativo` = 1))
        LEFT JOIN `turno_vagas` `tv` ON (`tv`.`area_entrega_id` = `ae`.`id`
            AND `tv`.`turno_id` = `t`.`id`))
        LEFT JOIN `escala` `e` ON (`tv`.`periodo_escala_id` = `e`.`periodo_escala_id`
            AND `e`.`area_entrega_id` = `ae`.`id`))
        LEFT JOIN `escala_turno` `et` ON (`et`.`et_morph_type` = 'App\Models\Escala'
            AND `et`.`et_morph_id` = `e`.`id`
            AND `t`.`id` = `et`.`turno_id`))
        LEFT JOIN `dias_semanal` `ds` ON (`ds`.`escala_turno_id` = `et`.`id`))
    WHERE
        `tv`.`id` IS NOT NULL
            AND `ae`.`ativo` = 1
    GROUP BY `tv`.`periodo_escala_id` , `ae`.`id` , `t`.`id` , `tv`.`qtd_vagas`


    CREATE 
        ALGORITHM = UNDEFINED 
        DEFINER = `root`@`localhost` 
        SQL SECURITY DEFINER
    VIEW `saldo_turno_vagas` AS
        SELECT 
            `tv`.`periodo_escala_id` AS `periodo_escala_id`,
            `ae`.`id` AS `area_entrega_id`,
            `t`.`id` AS `turno_id`,
            `tv`.`dia_id` AS `dia_id`,
            IFNULL(`tv`.`qtd_vagas`, 0) AS `qtd_vagas`,
            SUM(CASE
                WHEN `ds`.`dia_id` IS NOT NULL THEN 1
                ELSE 0
            END) AS `qtd_utilizadas`,
            CASE
                WHEN
                    `tv`.`qtd_vagas` IS NOT NULL
                        AND `tv`.`qtd_vagas` - SUM(CASE
                        WHEN `ds`.`dia_id` IS NOT NULL THEN 1
                        ELSE 0
                    END) > 0
                THEN
                    `tv`.`qtd_vagas` - SUM(CASE
                        WHEN `ds`.`dia_id` IS NOT NULL THEN 1
                        ELSE 0
                    END)
                ELSE 0
            END AS `saldo_vagas`
        FROM
            (((((`area_entrega` `ae`
            JOIN `turno` `t` ON (`t`.`praca_id` = `ae`.`praca_id`
                AND `t`.`ativo` = 1))
            LEFT JOIN `turno_vagas` `tv` ON (`tv`.`area_entrega_id` = `ae`.`id`
                AND `tv`.`turno_id` = `t`.`id`))
            LEFT JOIN `escala` `e` ON (`tv`.`periodo_escala_id` = `e`.`periodo_escala_id`
                AND `e`.`area_entrega_id` = `ae`.`id`))
            LEFT JOIN `escala_turno` `et` ON (`et`.`et_morph_type` = 'App\\Models\\Escala'
                AND `et`.`et_morph_id` = `e`.`id`
                AND `t`.`id` = `et`.`turno_id`))
            LEFT JOIN `dias_semanal` `ds` ON (`ds`.`escala_turno_id` = `et`.`id`
                AND `tv`.`dia_id` = `ds`.`dia_id`))
        WHERE
            `tv`.`id` IS NOT NULL
                AND `ae`.`ativo` = 1
        GROUP BY `tv`.`periodo_escala_id` , `ae`.`id` , `tv`.`dia_id` , `t`.`id` , `tv`.`qtd_vagas`

*/