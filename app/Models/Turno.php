<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $descricao
 * @property int    $praca_id
 *
 * @package App\Models
 */
class Turno extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'turno';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'descricao', 'praca_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'descricao' => 'string', 
        'praca_id' => 'int'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    public function praca()
    {
        return $this->hasOne('App\Models\Praca', 'id','praca_id');
    }
    
    public function escalas()
    {
        return $this->morphedByMany(Escala::class, 'ds_morph', 'dias_semanal');
    }
    
    public function escalasConfig()
    {
        return $this->morphedByMany(EscalaConfig::class, 'ds_morph', 'dias_semanal');
    }
}
