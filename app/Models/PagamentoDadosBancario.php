<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int    $data_turno
 * @property int    $dados_bancario_id
 * @property int    $created_at
 * @property int    $updated_at
 * @property string $turno
 * @property string $praca
 * @property string $entregador
 * @property string $tipo
 * @property string $descricao
 */
class PagamentoDadosBancario extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pagamento_dados_bancario';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'data_turno', 'turno', 'praca', 'entregador', 'tipo', 'valor', 'descricao', 'dados_bancario_id', 'created_at', 'updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'data_turno' => 'timestamp', 'turno' => 'string', 'praca' => 'string', 'entregador' => 'string', 'tipo' => 'string', 'descricao' => 'string', 'dados_bancario_id' => 'int', 'created_at' => 'timestamp', 'updated_at' => 'timestamp'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'data_turno', 'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...

    // Relations ...
}
