<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $et_morph_type
 * @property int    $turno_id
 */
class EscalaTurno extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'escala_turno';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'et_morph_type', 'et_morph_id', 'turno_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'et_morph_type' => 'string', 'turno_id' => 'int'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;
 
    /**
     * The roles that belong to the user.
     */
    public function dias()
    {
        return $this->belongsToMany(Dia::class, 'dias_semanal');
    }

    public function turno()
    {
        return $this->hasOne('App\Models\Turno', 'id','turno_id');
    }

    public function diasSemanais()
    {
        return $this->hasMany(DiasSemanal::class);
    }
}
