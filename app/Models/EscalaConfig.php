<?php

namespace App\Models;

use Carbon\Carbon;
//use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $area_entrega_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class EscalaConfig extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'escala_config';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'area_entrega_id', 'turno_id', 'created_at', 'updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'area_entrega_id' => 'int', 'turno_id' => 'int', 'created_at' => 'timestamp', 'updated_at' => 'timestamp'
    ];

    
    /*public function getAttributes()
    {
        $attributes = parent::getAttributes();

        foreach ($attributes as $name => $value) {
            if ($name == 'dias_semana') {
                if($value) {
                    $attributes[$name] = Dia::whereIn('id', explode(';', $value))->get();
                }
            }
        }
        return $attributes;
    }*/

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    public function turnos()
    {
        return $this->morphToMany(
            Turno::class, 
            'et_morph',     //name for the morphable
            'escala_turno',    //pivot table
        );
    }

    public function escalaConfigTurnos()
    {
        return $this->morphMany(
            EscalaTurno::class, 
            'et_morph',     //name for the morphable
        );
    }

    /*public function turno()
    {
        return $this->hasOne('App\Models\Turno', 'id','turno_id');
    }*/
    
    public function areaEntrega()
    {
        return $this->hasOne('App\Models\AreaEntrega', 'id','area_entrega_id');
    }
}
