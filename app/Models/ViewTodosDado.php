<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ViewTodosDado
 * 
 * @property Carbon|null $dia_data
 * @property int|null $semana_ano
 * @property string|null $semana_anterior
 * @property string|null $semana_atual
 * @property string|null $modalidade
 * @property string|null $turno
 * @property string|null $tag
 * @property int|null $entregador_id
 * @property string|null $entregador
 * @property string|null $praca
 * @property float|null $tempo_online
 * @property float|null $tempo_bloqueado
 * @property int|null $qtd_aceitas
 * @property int|null $qtd_completadas
 * @property int|null $qtd_ofertadas
 * @property int|null $qtd_rejeitadas
 *
 * @package App\Models
 */
class ViewTodosDado extends Model
{
	protected $table = 'view_todos_dados';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'semana_ano' => 'int',
		'entregador_id' => 'int',
		'tempo_online' => 'float',
		'tempo_bloqueado' => 'float',
		'qtd_aceitas' => 'int',
		'qtd_completadas' => 'int',
		'qtd_ofertadas' => 'int',
		'qtd_rejeitadas' => 'int'
	];

	protected $dates = [
		'dia_data'
	];

	protected $fillable = [
		'dia_data',
		'semana_ano',
		'semana_anterior',
		'semana_atual',
		'modalidade',
		'turno',
		'tag',
		'entregador_id',
		'entregador',
		'praca',
		'tempo_online',
		'tempo_bloqueado',
		'qtd_aceitas',
		'qtd_completadas',
		'qtd_ofertadas',
		'qtd_rejeitadas'
	];
}
