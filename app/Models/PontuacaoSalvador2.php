<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PontuacaoSalvador2
 * 
 * @property string|null $praca
 * @property Carbon|null $dia_data
 * @property string|null $entregador
 * @property float|null $qtd_rejeitadas
 * @property float|null $qtd_cancelamento
 * @property float|null $tempo_online
 * @property int|null $pto_tempo_online
 * @property int|null $pto_rejeite
 * @property int|null $pto_cancel
 *
 * @package App\Models
 */
class PontuacaoSalvador2 extends Model
{
	protected $table = 'pontuacao_salvador_2';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'qtd_rejeitadas' => 'float',
		'qtd_cancelamento' => 'float',
		'tempo_online' => 'float',
		'pto_tempo_online' => 'int',
		'pto_rejeite' => 'int',
		'pto_cancel' => 'int'
	];

	protected $dates = [
		'dia_data'
	];

	protected $fillable = [
		'praca',
		'dia_data',
		'entregador',
		'qtd_rejeitadas',
		'qtd_cancelamento',
		'tempo_online',
		'pto_tempo_online',
		'pto_rejeite',
		'pto_cancel'
	];
}
