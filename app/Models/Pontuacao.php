<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Pontuacao
 * 
 * @property string|null $praca
 * @property Carbon|null $dia_data
 * @property string|null $mes_ano
 * @property string|null $entregador
 * @property int $qtd_turnos
 * @property float|null $qtd_rejeitadas
 * @property float|null $qtd_cancelamento
 * @property float|null $tempo_online
 * @property float|null $qtd_completadas
 * @property int|null $pto_rejeite_cancel
 * @property int|null $pto_tempo_online
 * @property int|null $pto_corrida_completa
 * @property int $pto_turno
 *
 * @package App\Models
 */
class Pontuacao extends Model
{
	protected $table = 'pontuacao';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'qtd_turnos' => 'int',
		'qtd_rejeitadas' => 'float',
		'qtd_cancelamento' => 'float',
		'tempo_online' => 'float',
		'qtd_completadas' => 'float',
		'pto_rejeite_cancel' => 'int',
		'pto_tempo_online' => 'int',
		'pto_corrida_completa' => 'int',
		'pto_turno' => 'int'
	];

	protected $dates = [
		'dia_data'
	];

	protected $fillable = [
		'praca',
		'dia_data',
		'mes_ano',
		'entregador',
		'qtd_turnos',
		'qtd_rejeitadas',
		'qtd_cancelamento',
		'tempo_online',
		'qtd_completadas',
		'pto_rejeite_cancel',
		'pto_tempo_online',
		'pto_corrida_completa',
		'pto_turno'
	];
}
