<?php

namespace App\ViewModels;

/**
 * @property string $nome
 * @property bool $ativo
 * @property bool $exibir
 * @property bool $temVaga
 * @property bool $emConfig
 */
class DiaVM
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $nome;

    /**
     * @var bool
     */
    public $ativo;

    /**
     * @var bool
     */
    public $exibir;

    /**
     * @var bool
     */
    public $temVaga;

    /**
     * @var bool
     */
    public $emConfig;
}
