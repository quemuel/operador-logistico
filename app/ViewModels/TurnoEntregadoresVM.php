<?php

namespace App\ViewModels;

use App\Models\Turno;
use Illuminate\Database\Eloquent\Collection;



class TurnoEntregadoresVM
{    
    /*{
        'id' : '_todo',
        'title'  : 'Try Drag me!',
        'item'  : [
            {
                'title':'You can drag me too',
            },
            {
                'title':'Buy Milk',
            }
        ]
    }*/
    
    public $id;

    public $title;

    /**
     * @var Collection
     */
    public $item;
}

class Item
{    
    public $title;
}