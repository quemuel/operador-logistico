<?php

namespace App\ViewModels;

use App\Models\Turno;
use Illuminate\Database\Eloquent\Collection;

/**
 * @property string $nome
 */
class TabelaCriacaoEscalaVM
{    
    /**
     * @var Turno
     */
    public $turno;

    /**
     * @var Collection
     */
    public $dias;
}
