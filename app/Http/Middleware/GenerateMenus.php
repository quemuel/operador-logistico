<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Services\Util\LaravelMenu;
//use Menu as LaravelMenu;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $laravelMenu = new LaravelMenu();
        $laravelMenu->make('MyNavBar', function ($menu) {
            $menu->options([
                'active_class' => 'kt-menu__item--active kt-menu__item--open',
            ]);
            $menu->add('<span class="kt-menu__link-text">Página Inicial</span>', ["route" => "dashboard", "class" => "kt-menu__item"])
                ->prepend('<i class="kt-menu__link-icon flaticon2-architecture-and-city"></i>')
                ->link->attr([
                    'class'  => 'kt-menu__link'
                ]);

            
            $menu->add('<h4 class="kt-menu__section-text">Menus</h4>', ["class" => "kt-menu__section"])
                ->append('<i class="kt-menu__section-icon flaticon-more-v2"></i>');

            $menu->add('<span class="kt-menu__link-text">Dados Bancários</span>', ["route" => "financeiro.index", "class" => "kt-menu__item"])
                ->prepend('<i class="kt-menu__link-icon flaticon-price-tag"></i>')
                ->link->attr([
                    'class'  => 'kt-menu__link'
                ]);

            $menu->add('<span class="kt-menu__link-text">Importar Faturamento</span>', ["route" => "dados-importacao.importar-arquivo-financeiro", "class" => "kt-menu__item"])
                ->prepend('<i class="kt-menu__link-icon flaticon-interface-10"></i>')
                ->link->attr([
                    'class'  => 'kt-menu__link'
                ]);

            $menu->add('<span class="kt-menu__link-text">Faturamento</span>', ["route" => "faturamento.index", "class" => "kt-menu__item"])
                ->prepend('<i class="kt-menu__link-icon flaticon-notepad"></i>')
                ->link->attr([
                    'class'  => 'kt-menu__link'
                ]);

            $menu->add('<span class="kt-menu__link-text">Período Agendamento</span>', ["route" => "periodoEscala.index", "class" => "kt-menu__item"])
                ->prepend('<i class="kt-menu__link-icon flaticon-calendar-with-a-clock-time-tools"></i>')
                ->link->attr([
                    'class'  => 'kt-menu__link'
                ]);

            $menuEscala = $menu->add('<span class="kt-menu__link-text">Agendamento</span>', ["class" => "kt-menu__item kt-menu__item--submenu"])
                ->prepend('<i class="kt-menu__link-icon flaticon-event-calendar-symbol"></i>')    
                ->append('<i class="kt-menu__ver-arrow la la-angle-right"></i>');
            
            $menuEscalaLink = $menuEscala->link;
            $menuEscalaLink->href("javascript:;");
            $menuEscalaLink->attr([
                'class'  => 'kt-menu__link kt-menu__toggle'
            ]);

            //submenu Escala x Área
            /*$menuEscala->add('<span class="kt-menu__link-text">Escala x Área</span>', ["route" => "escala.create", "class" => "kt-menu__item"])
                ->prepend('<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>')
                ->link->attr([
                    'class'  => 'kt-menu__link'
                ]);*/

            //submenu Por Área
            $menuEscala->add('<span class="kt-menu__link-text">Por Área</span>', ["route" => "escala.por-area", "class" => "kt-menu__item"])
                ->prepend('<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>')
                ->link->attr([
                    'class'  => 'kt-menu__link'
                ]);

            //submenu Por Entregador
            $menuEscala->add('<span class="kt-menu__link-text">Por Entregador</span>', ["route" => "escala.por-entregador", "class" => "kt-menu__item"])
                ->prepend('<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>')
                ->link->attr([
                    'class'  => 'kt-menu__link'
                ]);

                
            //Vagas
            $menuVagas = $menu->add('<span class="kt-menu__link-text">Vagas</span>', ["class" => "kt-menu__item kt-menu__item--submenu"])
                ->prepend('<i class="kt-menu__link-icon flaticon-list-3"></i>')    
                ->append('<i class="kt-menu__ver-arrow la la-angle-right"></i>');
            
            $menuVagasLink = $menuVagas->link;
            $menuVagasLink->href("javascript:;");
            $menuVagasLink->attr([
                'class'  => 'kt-menu__link kt-menu__toggle'
            ]);

            //submenu tela vagas por periodo escala
            $menuVagas->add('<span class="kt-menu__link-text">Geral</span>', ["route" => "vagas.geral", "class" => "kt-menu__item"])
                ->prepend('<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>')
                ->link->attr([
                    'class'  => 'kt-menu__link'
                ]);

            //submenu listagem de vagas por periodo escala e area de entrega
            $menuVagas->add('<span class="kt-menu__link-text">Listagem</span>', ["route" => "vagas.listagem", "class" => "kt-menu__item"])
                ->prepend('<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>')
                ->link->attr([
                    'class'  => 'kt-menu__link'
                ]);
        });
        return $next($request);
    }
}
