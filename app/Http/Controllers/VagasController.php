<?php

namespace App\Http\Controllers;

use App\Models\AreaEntrega;
use App\Models\Dia;
use App\Models\Escala;
use App\Models\PeriodoEscala;
use App\Models\SaldoTurnoVagas;
use App\Models\Turno;
use App\Models\TurnoVagas;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DataTables;
use Exception;
use Faker\Provider\Uuid;
use Illuminate\Support\Facades\DB;
use PDOException;

class VagasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function geral($periodoEscalaId = null, $data = null)
    {
        $periodosEscala = PeriodoEscala::orderBy('data_final', 'desc')
            ->get();

        if ($periodoEscalaId) {
            $periodoEscala = $periodosEscala->where('id', $periodoEscalaId)->first();
        } else {
            $periodoEscala = $periodosEscala->first();
        }
        $saldoTurnosVagas = collect();
        if($data)
            $saldoTurnosVagas = SaldoTurnoVagas::with(['areaEntrega', 'turno', 'dia'])
                ->where('periodo_escala_id', $periodoEscala ? $periodoEscala->id : null)
                /*->where(function ($q) use($data) {
                    if ($data) 
                        $q->where('dia.codigo', Carbon::parse($data)->dayOfWeek);
                })*/
                ->whereHas('dia', function ($query) use($data) {
                    if ($data) 
                        $query->where('dia.codigo', Carbon::parse($data)->dayOfWeek);
                    //return $query->where('entregador.praca_id', $pracaId);
                })
                ->orderBy('area_entrega_id', 'asc')
                ->orderBy('turno_id', 'asc')
                ->get();

        return view('vagas.geral', compact('periodosEscala', 'periodoEscala', 'saldoTurnosVagas', 'data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listagem($periodoEscalaId = null)
    {
        $periodosEscala = PeriodoEscala::orderBy('data_final', 'desc')
            ->get();

        if ($periodoEscalaId) {
            $periodoEscala = $periodosEscala->where('id', $periodoEscalaId)->first();
        } else {
            $periodoEscala = $periodosEscala->first();
        }

        return view('vagas.listagem', compact('periodosEscala', 'periodoEscala', 'periodoEscalaId'));
    }
    
    public function editar($periodoEscalaId, $areaEntregaId)
    {
        $periodoEscala = PeriodoEscala::find($periodoEscalaId);
        $areaEntrega = AreaEntrega::find($areaEntregaId);
        $turnosVagas = TurnoVagas::with('turno')
            ->select(['turno_id', 'qtd_vagas'])
            ->where('periodo_escala_id', $periodoEscalaId)
            ->where('area_entrega_id', $areaEntregaId)
            ->groupBy(['turno_id', 'qtd_vagas'])
            ->get();
        return view('vagas.editar', compact('periodoEscala', 'areaEntrega', 'turnosVagas'));
    }
   
    public function atualizar($periodoEscalaId, $areaEntregaId, Request $request)
    {
        try {
            $turnosVagasPost = $request->all();
            foreach ($turnosVagasPost['turnoVagas'] as $turnoId => $qtdVagas) {
                $turnosVagas = TurnoVagas::where('turno_id', $turnoId)
                    ->where('periodo_escala_id', $periodoEscalaId)
                    ->where('area_entrega_id', $areaEntregaId)
                    ->get();
                $dias = Dia::all();
                $dias->each(function ($dia) use($qtdVagas, $turnosVagas, $turnoId) {
                    $turnoVagas = $turnosVagas
                        ->where('dia_id', $dia->id)
                        ->first();
                    if($turnoVagas != null) {
                        $turnoVagas->qtd_vagas = $qtdVagas;
                        $turnoVagas->updated_at = Carbon::now();
                        $turnoVagas->dia_id = $dia->id;
                        $turnoVagas->save();
                    } else {
                        $turnoVagas = new TurnoVagas();
                        $turnoVagas->qtd_vagas = $qtdVagas;
                        $turnoVagas->turno_id = $turnoId;
                        $turnoVagas->created_at = Carbon::now();
                        $turnoVagas->dia_id = $dia->id;
                        $turnoVagas->save();
                    }
                });
            }

            /*$a = vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
                return is_numeric($binding) ? $binding : "'{$binding}'";
            })->toArray());*/

            return redirect()
                ->route('vagas.listagem', $periodoEscalaId)
                ->with('success', 'Período agendamento atualizado com sucesso!');
        } catch (PDOException $ex) {
            if ($ex->getCode() == 23505)
                return redirect()->back()->withInput()->with('error', 'Estes dados já foram cadastrados!');    
            return redirect()->back()->withInput()->with('error', $ex->getMessage());
        } catch (Exception $ex) {
            return redirect()->back()->withInput()->with('error', $ex->getMessage());
        }        
    }

    public function dados($periodoEscalaId = null)
    {
        $turnosVagasQuery = TurnoVagas::select(['periodo_escala_id', 'area_entrega_id'])
            ->with(['periodoEscala', 'areaEntrega.praca'])
            ->where(function ($q) use($periodoEscalaId) {
                if ($periodoEscalaId) 
                    $q->where('periodo_escala_id', $periodoEscalaId);
            })
            ->orderBy('periodo_escala_id', 'desc')
            ->groupBy(['periodo_escala_id', 'area_entrega_id']);
            
        return DataTables::eloquent($turnosVagasQuery)
            ->addColumn('praca', function (TurnoVagas $turnoVagas) {
                return $turnoVagas->areaEntrega->praca->descricao;
            })
            ->addColumn('periodoEscala', function (TurnoVagas $turnoVagas) {
                $dataInicial = Carbon::parse($turnoVagas->periodoEscala->data_inicial)->format('d/m/Y');
                $dataFinal = Carbon::parse($turnoVagas->periodoEscala->data_final)->format('d/m/Y');
                return "{$dataInicial} à {$dataFinal}";
            })
            ->addColumn('areaEntrega', function (TurnoVagas $turnoVagas) {
                return $turnoVagas->areaEntrega->descricao;
            })
            ->addColumn("action", function(TurnoVagas $turnoVagas){
                $routeEditar = route('vagas.editar', [$turnoVagas->periodo_escala_id, $turnoVagas->area_entrega_id]);
                $btn = "<a href='{$routeEditar}' class='edit btn btn-sm btn-clean btn-icon btn-icon-md'><i class='la la-edit text-warning'></i></a>";
                return $btn;
            })
            ->rawColumns(['action'])
            ->toJson();
    }
}
