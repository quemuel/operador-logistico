<?php

namespace App\Http\Controllers;

use App\Models\Banco;
use App\Models\DadosBancario;
use App\Models\Faturamento;
use App\Models\PagamentoDadosBancario;
use App\Models\PagamentoPagamentoDadosBancario;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DataTables;
use Exception;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use PDOException;

class FaturamentoController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth')->except(['show', 'create', 'store']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Carbon::setLocale('pt_BR');
        if(request()->get('dataInicial') && request()->get('dataFinal')) {
            $dataInicial = Carbon::parse(request()->get('dataInicial'));
            $dataFinal = Carbon::parse(request()->get('dataFinal'));
        } else {
            if(Carbon::now()->day > 15) {
                $dataInicial = new Carbon('first day of this month');
                $dataFinal = new Carbon('first day of this month');
                $dataFinal->addDay(14);
            } else {
                $dataInicial = new Carbon('first day of last month');
                $dataInicial->addDay(15);
                $dataFinal = new Carbon('last day of last month');
            }
        }
        return view('faturamento.index', [
            "dataInicial" => $dataInicial->format("Y-m-d"),
            "dataFinal" => $dataFinal->format("Y-m-d")
        ]);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function associarEntregador()
    {
        $entregador = request()->post('entregador');
        $praca = request()->post('praca');
        DadosBancario::where('id', request()->post('dados_bancario_id'))
            ->update(['nome' => $entregador, "municipio" => $praca]);

        Faturamento::where('entregador', $entregador)
            ->where('praca', $praca)
            ->update([
                'dados_bancario_id' => request()->post('dados_bancario_id'),
                'data_turno' => DB::raw("data_turno")
            ]);
            
        return redirect()
            ->route("faturamento.index")
            ->with('success', "Entregador {$entregador} associado com sucesso!");
    }

    public function dados()
    {
        Carbon::setLocale('pt_BR');
        $dataInicial = Carbon::parse(request()->get('dataInicial'));
        $dataFinal = Carbon::parse(request()->get('dataFinal'));

        $faturamentoGroupQuery = Faturamento::with('dadosBancario')
            ->select(['entregador', 'praca', 'dados_bancario_id', DB::raw("SUM(valor) as valor")])
            //->where('entregador','LIKE',"%alan%")
            ->whereBetween('data_turno', [$dataInicial->format('Y-m-d'), $dataFinal->format('Y-m-d')])
            ->groupBy(['entregador', 'praca', 'dados_bancario_id'])
            ->orderBy('entregador', 'asc')
            ->orderBy('praca', 'desc');
            
        return DataTables::eloquent($faturamentoGroupQuery)
            ->addColumn('cpf', function (Faturamento $faturamento) {
                return $faturamento->dadosBancario->cpf ?? "--";
            })
            ->addColumn('pix', function (Faturamento $faturamento) {
                return $faturamento->dadosBancario->pix ?? "--";
            })
            ->addColumn('banco', function (Faturamento $faturamento) {
                return $faturamento->dadosBancario->banco->nome ?? "--";
            })
            ->addColumn('agencia', function (Faturamento $faturamento) {
                return $faturamento->dadosBancario->agencia ?? "--";
            })
            ->addColumn('conta', function (Faturamento $faturamento) {
                return $faturamento->dadosBancario->conta ?? "-";
            })
            ->addColumn("action", function($row){
                if(!$row["dados_bancario_id"]) {
                    $btn = "<a href='javascript:void(0)' class='btn btn-success btn-sm' title='Associar entregador' data-entregador='{$row['entregador']}' data-praca='{$row['praca']}' onclick='carregarModal(this)'><i class='fa fa-check'></i></a>";
                    return $btn;
                }
                return "";
            })
            ->rawColumns(['action'])
            ->toJson();
            //->make(true);
    }

    /**    *
    * @return \Illuminate\Http\Response
    */
    public function select2DadosAjax(Request $request)
    {
    	$data = [];

        if($request->has('q')){
            $search = $request->q;
            $data = DadosBancario::select("id","nome", "municipio")
            		->where('nome','LIKE',"%$search%")
            		->get();
        }
        return response()->json($data);
    }
}
