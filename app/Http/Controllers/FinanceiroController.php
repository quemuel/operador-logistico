<?php

namespace App\Http\Controllers;

use App\Models\Banco;
use App\Models\DadosBancario;
use App\Models\Faturamento;
use Illuminate\Http\Request;
use DataTables;
use Exception;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use PDOException;

class FinanceiroController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['show', 'create', 'store']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('financeiro.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DadosBancario  $dadosBancario
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dadosBancario = DadosBancario::find(Crypt::decrypt($id));
        return view('financeiro.show', ["dadosBancario" => $dadosBancario]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //U2FsdmFkb3I=
        //QXJhY2FqdQ==
        // response('Requisição inválida', 400)
        $municipio = base64_decode(request()->get('mn'));
        if($municipio != "Salvador" && $municipio != "Aracaju" ) {
            return response('Requisição inválida', 400);
        }
        $bancos = Banco::all();
        return view('financeiro.create', compact('bancos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $municipio = base64_decode(request()->get('mn'));
        if($municipio != "Salvador" && $municipio != "Aracaju" ) {
            return response('Requisição inválida', 400);
        }
        $request->validate([
            'nome' => 'required',
            'cpf' => 'required|cpf',
            'pix' => 'required',
            'agencia' => 'required',
            'conta' => 'required',
            'banco_id' => 'required'
        ],
        [
            'required' => 'Campo :attribute obrigatório',
            'cpf' => 'CPF inválido'
        ]);

        try {
            $dadosBancarioPost = $request->all();
            $dadosBancarioPost['municipio'] = $municipio;
            if(DadosBancario::where('cpf', $dadosBancarioPost['cpf'])->exists()) {
                return redirect()->back()->withInput()->with('error', "CPF já cadastrado");
            }
            $dadosBancario = DadosBancario::create($dadosBancarioPost);

            Faturamento::where('entregador', $dadosBancario->nome)
                ->where('praca', $dadosBancario->municipio)
                ->update(['dados_bancario_id' => $dadosBancario->id]);

            return redirect()
                ->route('financeiro.show', Crypt::encrypt($dadosBancario->getKey()))
                ->with('success', 'Dados cadastrados com sucesso!');
        } 
        catch (PDOException $ex) {
          if ($ex->getCode() == 23505)
                return redirect()->back()->withInput()->with('error', 'Estes dados já foram cadastrados!');    
            return redirect()->back()->withInput()->with('error', $ex->getMessage());
        } catch (Exception $ex) {
            return redirect()->back()->withInput()->with('error', $ex->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DadosBancario  $dadosBancario
     * @return \Illuminate\Http\Response
     */
    public function edit(DadosBancario $financeiro)
    {
        $dadosBancario = $financeiro;
        $bancos = Banco::all();
        return view('financeiro.edit', compact('dadosBancario', 'bancos'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DadosBancario  $dadosBancario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DadosBancario $financeiro)
    {
        $request->validate([
            'municipio' => 'required',
            'nome' => 'required',
            'cpf' => 'required|cpf',
            'pix' => 'required',
            'agencia' => 'required',
            'conta' => 'required',
            'banco_id' => 'required'
        ],
        [
            'required' => 'Campo :attribute obrigatório',
            'cpf' => 'CPF inválido'
        ]);

        try {
            $dadosBancarioPost = $request->all();
            if($financeiro->getOriginal('cpf') != $dadosBancarioPost['cpf'] && DadosBancario::where('cpf', $dadosBancarioPost['cpf'])->exists()) {
                return redirect()->back()->withInput()->with('error', "Já existe um usuário cadastrado com CPF {$dadosBancarioPost['cpf']}!");
            }
            $financeiro->update($dadosBancarioPost);

            Faturamento::where('entregador', $financeiro->nome)
                ->where('praca', $financeiro->municipio)
                ->update(['dados_bancario_id' => $financeiro->id]);

            return redirect()->route('financeiro.index', ['s' => Crypt::encrypt("s3nh4")])
                ->with('success', 'Dados bancário atualizado com sucesso!');
        } 
        catch (PDOException $ex) {
          if ($ex->getCode() == 23505)
                return redirect()->back()->withInput()->with('error', 'Estes dados já foram cadastrados!');    
            return redirect()->back()->withInput()->with('error', $ex->getMessage());
        } catch (Exception $ex) {
            return redirect()->back()->withInput()->with('error', $ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DadosBancario  $dadosBancario
     * @return \Illuminate\Http\Response
     */
    public function destroy(DadosBancario $financeiro)
    {
        $financeiro->delete();

        return redirect()->back()
            ->with('success', 'Conta deletada com sucesso!');
    }

    public function dados()
    {
        //$dadosBancarios = DB::table('dados_bancario');
        
        $model = DadosBancario::with('banco');
        return DataTables::eloquent($model)
            ->addColumn('banco', function (DadosBancario $dadosBancario) {
                return $dadosBancario->banco->nome;
            })
            ->addColumn("action", function($row){
                $routeView = route('financeiro.show', Crypt::encrypt($row['id']));
                $routeEdit = route('financeiro.edit', $row['id']);
                $routeDelete = route('financeiro.destroy', $row['id']);
                $btn = "<a href='{$routeView}' class='btn btn-sm btn-clean btn-icon btn-icon-md'><i class='la la-search text-primary'></i></a>";
                $btn = $btn."<a href='{$routeEdit}' class='edit btn btn-sm btn-clean btn-icon btn-icon-md'><i class='la la-edit text-warning'></i></a>";
                $btn = $btn."<a href='javascript:void(0)' class='delete btn btn-sm btn-clean btn-icon btn-icon-md' data-key='{$row['id']}' data-name='{$row['nome']}' data-url='{$routeDelete}' onclick='proccessDeletion(this)'><i class='la la-trash text-danger'></i></a>";

                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
