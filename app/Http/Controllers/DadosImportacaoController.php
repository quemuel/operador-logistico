<?php

namespace App\Http\Controllers;

use App\Imports\DadosImportacao;
use App\Imports\DadosImportacaoFinanceiro;
use App\Models\Faturamento;
use App\Models\PagamentoDadosBancario;
use App\Models\ViewTodosDado;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\TbDado;
use Maatwebsite\Excel\Concerns\Importable;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DadosImportacaoController extends Controller
{
    use Importable;

    public function index()
    { 
        $tbDados = TbDado::groupBy('data')
            ->select('data', DB::raw('count(*) as qtd'))
            ->limit(5)
            ->orderBy('data', 'desc')
            ->get();
        return view('dados-importacao.importacao', compact('tbDados'));
    }

    public function importarArquivoFinanceiro()
    {
        return view('dados-importacao.importar-arquivo-financeiro');
    }
   
    public function storeImportarArquivoFinanceiro(Request $request)
    {
        Carbon::setLocale('pt_BR');
        $dataInicial = Carbon::parse($request->post('data_inicial'));
        $dataFinal = Carbon::parse($request->post('data_final'));
        
        $dadosImportacaoFinanceiro = new DadosImportacaoFinanceiro();

        $linhas = Excel::toArray($dadosImportacaoFinanceiro, $request->file('file'));
        foreach ($linhas[0] as $key => $value) {
            $linha = $key+1;
            $dataAtual = Carbon::parse($value[0]);

            if(!($dataInicial->lte($dataAtual) && $dataFinal->gte($dataAtual))) {
                return back()->with('error', "Arquivo não pode ser importado pois {$dataAtual->format('d/m/Y')} está fora do intervalo de {$dataInicial->format('d/m/Y')} à {$dataFinal->format('d/m/Y')}. Linha: $linha.");
            }
            
            $faturamentoExists = Faturamento::where([
                ['data_turno', '=', $value[0]],
                ['entregador', '=', $value[4]],
                ['praca', '=', $value[2]],
                ['turno', '=', $value[1]],
            ])->exists();

            if($faturamentoExists) {
                return back()->with('error', "Arquivo não pode ser importado pois {$dataAtual->format('d/m/Y')} já foi importado em outro momento. Linha: $linha.");
            }
        }

        Excel::import($dadosImportacaoFinanceiro, $request->file('file'));

        $qtdRegistros = count($linhas[0]);
        return back()->with('success', "Arquivo importado com sucesso! $qtdRegistros registros importados.");
    }
    
    public function importarArquivo(Request $request) 
    {
        /*
        eq() equals
        ne() not equals
        gt() greater than
        gte() greater than or equals
        lt() less than
        lte() less than or equals
        */
        Carbon::setLocale('pt_BR');
        $dataInicial = Carbon::parse($request->post('data_inicial'));
        $dataFinal = Carbon::parse($request->post('data_final'));
        
        $dadosImportacao = new DadosImportacao;
        $qtdRegistros = 0;
        foreach($request->file('files') as $key => $file) {
            //$dataNome = $file->getClientOriginalName();
            $linhas = Excel::toArray($dadosImportacao, $file);
            foreach ($linhas[0] as $key => $value) {
                $linha = $key+1;
                $dataAtual = Carbon::parse($value[0]);//TODO
                if(!($dataInicial->lte($dataAtual) && $dataFinal->gte($dataAtual))) {
                    return back()->with('error', "Arquivo não pode ser importado pois {$dataAtual->format('d/m/Y')} está fora do intervalo de {$dataInicial->format('d/m/Y')} à {$dataFinal->format('d/m/Y')}. Linha: $linha.");
                }
            }
            $qtdRegistros += count($linhas[0]);
        }

        $qtdRegistrosBD = TbDado::
            whereBetween('data', ["{$dataInicial->format('Y-m-d')} 00:00:00", "{$dataFinal->format('Y-m-d')} 23:59:59"])
            ->get()
            ->count();
        if($qtdRegistros == $qtdRegistrosBD) {
            return back()->with('error', "Arquivo não pode ser importado pois já foi importado. Bloqueio efetuado para não duplicar dados.");
        } else if($qtdRegistrosBD > 0) {
            TbDado::whereBetween('data', ["{$dataInicial->format('Y-m-d')} 00:00:00", "{$dataFinal->format('Y-m-d')} 23:59:59"])
                ->delete();
        }

        foreach($request->file('files') as $key => $file) {
            Excel::import($dadosImportacao, $file);
        }

        return back()->with('success', "Arquivo importado com sucesso! $qtdRegistros registros.");
    }
}
