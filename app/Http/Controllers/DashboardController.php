<?php

namespace App\Http\Controllers;

use App\Models\FiltroDashboard;
use App\Models\ViewTodosDado;
use App\Services\Application\Dashboard\DashboardService;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Log;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //Log::Info("sdf");
        $filtroDashboard = new FiltroDashboard($request->all());
        $dashboardService = new DashboardService();
        $dadosDashboard = $dashboardService->obterDadosDashboard($filtroDashboard);
        return view('dashboard.index', ["dadosDashboard" => $dadosDashboard]);
    }

    public function dados(Request $request)
    {
        $filtroDashboard = new FiltroDashboard($request->all());
        $dashboardService = new DashboardService();
        if(!$filtroDashboard->semanasAnos) {
            $filtroDashboard->semanasAnos = [$dashboardService->obterSemanaAnoAtual()];
        }
        $tipoTabela = request()->get('tipoTabela');
        if($tipoTabela == "EntregadoresRejeite") {
            $dados = $dashboardService->obterEntregadoresRejeite($filtroDashboard);
        } else if($tipoTabela == "EntregadoresTempoOnline") {
            $dados = $dashboardService->obterEntregadoresTempoOnline($filtroDashboard);
        } else if($tipoTabela == "EntregadoresCancelamento") {
            $dados = $dashboardService->obterEntregadoresCancelamento($filtroDashboard);
        } else {
            return [];
        }
        
        return Datatables::of($dados)
                ->addIndexColumn()
                ->make(true);
    }
}
