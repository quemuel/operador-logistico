<?php

namespace App\Http\Controllers;

use App\Models\AreaEntrega;
use App\Models\Dia;
use App\Models\Escala;
use App\Models\PeriodoEscala;
use App\Models\Turno;
use App\Models\TurnoVagas;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DataTables;
use Exception;
use Faker\Provider\Uuid;
use PDOException;

class PeriodoEscalaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('periodo-escala.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function visualizar(PeriodoEscala $periodoEscala)
    {
        $areasEntrega = AreaEntrega::with('praca')->get();
        return view('periodo-escala.visualizar', compact('periodoEscala', 'areasEntrega'));
    }
    
    public function cadastrar()
    {
        return view('periodo-escala.cadastrar');
    }
    
    public function guardar(Request $request)
    {
        try {
            $periodoEscalaPost = $request->all();
            $datas = explode(" - ", $periodoEscalaPost['datas']);
            $periodoEscala = [
                "data_inicial" => Carbon::createFromFormat('d/m/Y', $datas[0])->format('Y-m-d'),
                "data_final" => Carbon::createFromFormat('d/m/Y', $datas[1])->format('Y-m-d'),
            ];

            $existeConflito = PeriodoEscala::whereDate('data_inicial','<=', $periodoEscala['data_inicial'])
                ->whereDate('data_final','>=', $periodoEscala['data_inicial'])
                ->whereDate('data_inicial','<=', $periodoEscala['data_final'])
                ->whereDate('data_final','>=', $periodoEscala['data_final'])
                ->orWhereBetween('data_inicial', [$periodoEscala['data_inicial'], $periodoEscala['data_final']])
                ->whereBetween('data_inicial', [$periodoEscala['data_inicial'], $periodoEscala['data_final']])
                ->exists();

            if($existeConflito)
                return redirect()->back()->withInput()->with('error', "Cadastro não realizado, pois há algum conflito de datas com outros períodos antes cadastrados.");
            
            $periodoEscala['codigo'] = Uuid::uuid();
            $periodoEscala['created_at'] = Carbon::now();
            $periodoEscalaBD = PeriodoEscala::create($periodoEscala);

            $areasEntrega = AreaEntrega::where('ativo', 1)->get();
                
            foreach ($areasEntrega as $areaEntrega) {
                $turnos = Turno::where('praca_id', $areaEntrega->praca_id)->get();
                foreach ($turnos as $turno) {
                    $dias = Dia::all();
                    $dias->each(function ($dia) use($turno, $areaEntrega, $periodoEscalaBD) {
                        $turnoVagas = new TurnoVagas();
                        $turnoVagas->periodo_escala_id = $periodoEscalaBD->id;
                        $turnoVagas->area_entrega_id = $areaEntrega->id;
                        $turnoVagas->turno_id = $turno->id;
                        $turnoVagas->dia_id = $dia->id;
                        $turnoVagas->qtd_vagas = 0;
                        $turnoVagas->created_at = Carbon::now();
                        $turnoVagas->save();
                    });
                }
            }

            return redirect()
                ->route('periodoEscala.index')
                ->with('success', 'Período agendamento cadastrado com sucesso!');
        } 
        catch (PDOException $ex) {
            if ($ex->getCode() == 23505)
                return redirect()->back()->withInput()->with('error', 'Estes dados já foram cadastrados!');    
            return redirect()->back()->withInput()->with('error', $ex->getMessage());
        } catch (Exception $ex) {
            return redirect()->back()->withInput()->with('error', $ex->getMessage());
        }
    }
    
    public function editar(PeriodoEscala $periodoEscala)
    {
        return view('periodo-escala.editar', compact('periodoEscala'));
    }
   
    public function atualizar(PeriodoEscala $periodoEscala, Request $request)
    {
        try {
            $periodoEscalaPost = $request->all();
            $datas = explode(" - ", $periodoEscalaPost['datas']);
            
            $periodoEscala->data_inicial = Carbon::createFromFormat('d/m/Y', $datas[0])->format('Y-m-d');
            $periodoEscala->data_final = Carbon::createFromFormat('d/m/Y', $datas[1])->format('Y-m-d');
            
            $existeConflito = PeriodoEscala::where('id','<>', $periodoEscala->id)
                ->where(function($query) use($periodoEscala){
                    $query->whereDate('data_final','>=', $periodoEscala->data_inicial)
                    ->whereDate('data_inicial','<=', $periodoEscala->data_final)
                    ->whereDate('data_final','>=', $periodoEscala->data_final)
                    ->orWhereBetween('data_inicial', [$periodoEscala->data_inicial, $periodoEscala->data_final])
                    ->whereBetween('data_inicial', [$periodoEscala->data_inicial, $periodoEscala->data_final]);
                })->exists();

            /*$a = vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
                return is_numeric($binding) ? $binding : "'{$binding}'";
            })->toArray());*/

            if($existeConflito)
                return redirect()->back()->withInput()->with('error', "Alteração não realizada, pois há algum conflito de datas com outros períodos antes cadastrados.");
            
            $periodoEscala->updated_at = Carbon::now();
            $periodoEscala->save();

            return redirect()
                ->route('periodoEscala.index')
                ->with('success', 'Período agendamento atualizado com sucesso!');
        } catch (PDOException $ex) {
            if ($ex->getCode() == 23505)
                return redirect()->back()->withInput()->with('error', 'Estes dados já foram cadastrados!');    
            return redirect()->back()->withInput()->with('error', $ex->getMessage());
        } catch (Exception $ex) {
            return redirect()->back()->withInput()->with('error', $ex->getMessage());
        }        
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DadosBancario  $dadosBancario
     * @return \Illuminate\Http\Response
     */
    public function excluir(PeriodoEscala $periodoEscala)
    {
        try {
            $periodoEmUso = Escala::where('periodo_escala_id', $periodoEscala->id)
                ->exists();

            if($periodoEmUso)
                return redirect()->back()->with('error', "Exclusão não realizada, pois há escala(s) associadas a esse período agendamento.");

            TurnoVagas::where('periodo_escala_id', $periodoEscala->id)->delete();

            $periodoEscala->delete();

            return redirect()
                ->route('periodoEscala.index')
                ->with('success', 'Período agendamento excluido com sucesso!');
        } catch (PDOException $ex) {
            if ($ex->getCode() == 23505)
                return redirect()->back()->withInput()->with('error', 'Estes dados já foram cadastrados!');    
            return redirect()->back()->withInput()->with('error', $ex->getMessage());
        } catch (Exception $ex) {
            return redirect()->back()->withInput()->with('error', $ex->getMessage());
        }  
    }

    public function dados()
    {
        $periodoEscalaQuery = PeriodoEscala::orderBy('created_at', 'desc');
            
        return DataTables::eloquent($periodoEscalaQuery)
            ->addColumn('data_inicial', function (PeriodoEscala $periodoEscala) {
                return Carbon::parse($periodoEscala->data_inicial)->format('d/m/Y');
            })
            ->addColumn('data_final', function (PeriodoEscala $periodoEscala) {
                return Carbon::parse($periodoEscala->data_final)->format('d/m/Y');
            })
            ->addColumn('created_at', function (PeriodoEscala $periodoEscala) {
                return Carbon::parse($periodoEscala->created_at)->format('d/m/Y H:i');
            })
            ->addColumn("action", function($periodoEscala){
                $dataInicial = Carbon::parse($periodoEscala->data_inicial)->format('d/m/Y');
                $dataFinal = Carbon::parse($periodoEscala->data_final)->format('d/m/Y');
                $datas = "{$dataInicial} à {$dataFinal}";

                $routeView = route('periodoEscala.visualizar', $periodoEscala['id']);
                $routeEdit = route('periodoEscala.editar', $periodoEscala['id']);
                $routeDelete = route('periodoEscala.excluir', $periodoEscala['id']);
                $btn = "<a href='{$routeView}' class='btn btn-sm btn-clean btn-icon btn-icon-md'><i class='la la-search text-primary'></i></a>";
                $btn = $btn."<a href='{$routeEdit}' class='edit btn btn-sm btn-clean btn-icon btn-icon-md'><i class='la la-edit text-warning'></i></a>";
                $btn = $btn."<a href='javascript:void(0)' class='delete btn btn-sm btn-clean btn-icon btn-icon-md' data-key='{$periodoEscala['id']}' data-name='{$datas}' data-url='{$routeDelete}' onclick='proccessDeletion(this)'><i class='la la-trash text-danger'></i></a>";

                return $btn;
            })
            ->rawColumns(['action'])
            ->toJson();
    }
}
