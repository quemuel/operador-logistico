<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;

class MigrationController extends Controller
{
    /**
     * 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function v1()
    {
        Artisan::call('migrate', array(
          '--path' => 'database/migrations/2021_07_27_214147_alter_column_dados_bancario_table.php',
          //'--database' => 'dynamicdb',
          //'--force' => true
        ));
        /*Artisan::call('migrate', array(
          '--path' => 'database/migrations/2021_06_24_222241_add_columns_table_users.php'
        ));*//*
        Artisan::call('db:seed', array(
          '--class' => 'CreateParamsSeeder'
        ));
        Artisan::call('migrate:rollback', array(
          '--step' => '1'
        ));
        */
        //return "Migração v1 executada! dados_bancario_table";
        return "Migração v1 executada! alter_column_dados_bancario_table";
    }
}
