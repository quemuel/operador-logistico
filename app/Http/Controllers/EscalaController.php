<?php

namespace App\Http\Controllers;

use App\Models\AreaEntrega;
use App\Models\EscalaConfig;
use App\Models\BloquearEntregadorArea;
use DataTables;
use App\Models\Dia;
use App\Models\DiasSemanal;
use App\Models\Entregador;
use App\Models\Escala;
use App\Models\EscalaTurno;
use App\Models\Parametro;
use App\Models\PeriodoEscala;
use App\Models\Praca;
use App\Models\SaldoTurnoVagas;
use App\Models\Turno;
use App\Models\TurnoDiaSemana;
use App\Models\TurnoVagas;
use App\ViewModels\DiaVM;
use App\ViewModels\Item;
use App\ViewModels\TabelaCriacaoEscalaVM;
use App\ViewModels\TabelaVisualizaEscalaVM;
use App\ViewModels\TurnoEntregadoresVM;
use App\ViewModels\TurnoVM;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PDOException;
use Illuminate\Support\Str;

class EscalaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function visualizar($escalaId)
    {
        $escala = Escala::with('escalaTurnos.dias', 'escalaTurnos.turno', 'entregador', 'areaEntrega')
            ->where('id', $escalaId)
            ->first();

        $entregador = $escala->entregador;

        $praca = Praca::find($entregador->praca_id);

        $areaEntrega = $escala->areaEntrega;

        $periodoEscala = $escala->periodoEscala;

        $tabelaEscala = $this->gerarTabelaEscalaVisualizacao($escala, $praca->id);

        return view('escala.visualizar', compact('praca', 'areaEntrega', 'entregador', 'escala', 'periodoEscala', 'tabelaEscala'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function porEntregador()
    {
        //$escala = Escala::with('escalaTurnos.dias')->get();
        //dump($escala);

        //$escalaConfig = EscalaConfig::with('escalaTurnos.dias')->where('escala_config.id', 1)->get();
        //dd($escalaConfig);
        return view('escala.por-entregador');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function porArea()
    {
        return view('escala.por-area');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function visualizarPorArea($pracaId, $periodoEscalaId, $areaEntregaId, $data = null)
    {
        $praca = Praca::find($pracaId);
        $areaEntrega = AreaEntrega::find($areaEntregaId);
        $periodoEscala = PeriodoEscala::find($periodoEscalaId);

        $turnosEntregadoresJson = "[]";
        $turnos = new Collection();
        if($data) {
            $turnos = Turno::where('praca_id', $pracaId)->get();
            $dataCarbon = Carbon::parse($data);
            $turnosEntregadoresColect = new Collection();
            foreach ($turnos as $turno) {
                $entregadores = DB::table('escala')
                    ->selectRaw("entregador.nome_completo")
                    ->join('escala_turno', function($q) {
                        $q->on('escala_turno.et_morph_id', '=', 'escala.id');
                        $q->where('escala_turno.et_morph_type', '=', 'App\Models\Escala');
                    })
                    ->join('dias_semanal', 'escala_turno.id', '=', 'dias_semanal.escala_turno_id')
                    ->join('dia', 'dias_semanal.dia_id', '=', 'dia.id')
                    ->join('entregador', 'escala.entregador_id', '=', 'entregador.id')
                    ->join('area_entrega', 'escala.area_entrega_id', '=', 'area_entrega.id')
                    ->where('entregador.praca_id', $pracaId)
                    ->where('escala.periodo_escala_id', $periodoEscalaId)
                    ->where('escala.area_entrega_id', $areaEntregaId)
                    ->where('dia.codigo', $dataCarbon->dayOfWeek)
                    ->where('escala_turno.turno_id', $turno->id)
                    ->get();

                /*$a = vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
                    return is_numeric($binding) ? $binding : "'{$binding}'";
                })->toArray());*/

                $turnoEntregadoresVM = new TurnoEntregadoresVM();
                $turnoEntregadoresVM->id = $turno->id;
                $turnoEntregadoresVM->title = $turno->descricao;
                $entregadoresColect = new Collection();
                foreach ($entregadores as $entregador) {
                    $item = new Item();
                    $item->title = $entregador->nome_completo;
                    $entregadoresColect->push($item);
                }
                $turnoEntregadoresVM->item = $entregadoresColect;
                $turnosEntregadoresColect->push($turnoEntregadoresVM);
            }
            $turnosEntregadoresJson = $turnosEntregadoresColect->toJson();
            /*
            $toSql = $turnosEscalaQuery->toSql();
            $get = $turnosEscalaQuery->get();
            $c = $turnosEscala->pluck('turno_id')->all();
            */
        }

        return view('escala.visualizar-por-area', compact('praca', 'areaEntrega', 'periodoEscala', 'data', 'turnosEntregadoresJson'));
    }
    
    //$escalasPorArea = Escala::with(['escalaTurnos.turno'])
    /*
    $inventories = $inventories->leftJoin('media', function($q) {
        $q->on('media.model_id', '=', 'inventories.id');
        $q->where('media.model_type', '=', 'App\Models\Inventory');
    });*/
    /*$multiplied = $collection->map(function ($item, $key) {
        return $item * 2;
    });
     
    $multiplied->all();

    ->whereHas('entregador', function ($query) use($pracaId) {
        return $query->where('entregador.praca_id', $pracaId);
    })
    //->where('entregador.praca_id', $pracaId)
    ->where('escala.periodo_escala_id', $periodoEscalaId)
    ->where('escala.area_entrega_id', $areaEntregaId)
    //->orderBy('entregador.nome_completo', 'asc')
    ->get();

    $escalaTurnos = DB::table('escala')
        ->selectRaw('praca.descricao praca, entregador.praca_id, escala.periodo_escala_id, periodo_escala.created_at, periodo_escala.data_inicial, periodo_escala.data_final, escala.area_entrega_id, area_entrega.descricao area_entrega')
        ->join('entregador', 'escala.entregador_id', '=', 'entregador.id')
        ->join('praca', 'entregador.praca_id', '=', 'praca.id')
        ->join('periodo_escala', 'escala.periodo_escala_id', '=', 'periodo_escala.id')
        ->join('area_entrega', 'escala.area_entrega_id', '=', 'area_entrega.id')
        ->groupByRaw('praca.descricao, entregador.praca_id, escala.periodo_escala_id, periodo_escala.created_at, periodo_escala.data_inicial, periodo_escala.data_final, escala.area_entrega_id, area_entrega.descricao')
        ->orderByRaw('praca.descricao, periodo_escala.created_at desc, area_entrega.descricao');


    $escalasPorArea = Escala::with(['entregador' => function ($query)  {
            $query->orderBy('nome_completo', 'asc');
        }])->with(['periodoEscala', 'areaEntrega', 'escalaTurnos.dias', 'escalaTurnos.turno'])
        ->whereHas('entregador', function ($query) use($pracaId) {
            return $query->where('entregador.praca_id', $pracaId);
        })
        //->where('entregador.praca_id', $pracaId)
        ->where('escala.periodo_escala_id', $periodoEscalaId)
        ->where('escala.area_entrega_id', $areaEntregaId)
        //->orderBy('entregador.nome_completo', 'asc')
        ->get();

    $grouped = $escalasPorArea->groupBy('escalaTurnos');
    $a = $grouped->all();*/
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function create()
    {
        return view('escala.create');
    }*/
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function agendar($codigo, $areaEntregaGuid, $cpf=null)
    {
        $requisicaoValida = [
            "valida" => true,
            "mensagem" => ""
        ];
        
        $periodoEscala = PeriodoEscala::where('codigo', $codigo)
            ->where(function($query) {
                $query->whereDate('data_inicial','<=', date('Y-m-d'))
                ->whereDate('data_final','>=', date('Y-m-d'))
                ->orwhereDate('data_inicial','>=', date('Y-m-d'));
            })->first();
            
        if(!$periodoEscala){
            $requisicaoValida["valida"] = false;
            $requisicaoValida["mensagem"] = "Link expirado!";
            return view('escala.agendar', compact('requisicaoValida'));
        }
        
        $areaEntrega = AreaEntrega::where('codigo', $areaEntregaGuid)
            ->first();

        if(!$areaEntrega){
            $requisicaoValida["valida"] = false;
            $requisicaoValida["mensagem"] = "Url inválida!";
            return view('escala.agendar', compact('requisicaoValida'));
        }

        $pracaId = $areaEntrega->praca_id; 
        $praca = Praca::find($pracaId);

        $entregador = null;
        $tabelaEscala = null;
        $escala = null;

        if($cpf) {
            Log::info("Iniciando agendamento com {$cpf}");
            $entregador = Entregador::where('cpf', preg_replace('/\D/', '', $cpf))
                ->where('praca_id', $pracaId)
                ->first();
                
            if($entregador == null){
                return back()->with('error', "Não há entregador para o CPF {$cpf} em {$praca->descricao}!");
            } else if(!$entregador->ativo) {
                return back()->with('error', "Entregador de CPF {$cpf} inativo!");
            } else if(BloquearEntregadorArea::where('entregador_id', $entregador->id)->where('area_entrega_id', $areaEntrega->id)->exists()) {
                return back()->with('error', "Entregador de CPF {$cpf} sem acesso para área {$areaEntrega->descricao} em {$praca->descricao}");
            }
                        
            $escala = Escala::with('escalaTurnos.dias', 'escalaTurnos.turno', 'areaEntrega')
                ->where('entregador_id', $entregador->id)
                ->where('periodo_escala_id', $periodoEscala->id)
                ->first();

            if($escala != null && $escala->area_entrega_id != $areaEntrega->id) {
                $requisicaoValida["valida"] = false;
                $requisicaoValida["mensagem"] = "Entregador de CPF {$cpf} já está agendado para entrega na área {$escala->areaEntrega->descricao}". ($escala->areaEntrega->is_dedicado ? " (Dedicado)" : "");
                return view('escala.agendar', compact('requisicaoValida'));
            }

            //configuracoes para criar escala
            $escalaConfig = EscalaConfig::with('escalaConfigTurnos.dias', 'escalaConfigTurnos.turno')
                ->where('area_entrega_id', $areaEntrega->id)
                ->first();

            if(!$escalaConfig || $escalaConfig->escalaConfigTurnos == null || $escalaConfig->escalaConfigTurnos->isEmpty()){
                return back()->with('error', "Problemas na configuração de agendamento, procure o administrador do sistema!");
            }

            $turnosIds = $escalaConfig->escalaConfigTurnos->map(function($escalaConfigTurno) {
                return $escalaConfigTurno->turno_id;
            });

            $saldoTurnosVagas = SaldoTurnoVagas::with('turno')
                ->where('periodo_escala_id', $periodoEscala->id)
                ->where('area_entrega_id', $areaEntrega->id)
                ->whereIn('turno_id', $turnosIds->all())
                ->get();

            if(!$saldoTurnosVagas->some('saldo_vagas', '>', 0)){
                return back()->with('error', "Não há vagas disponíveis para nenhum turno!");
            }
            
            $tabelaEscala = $this->gerarTabelaEscalaAgendamento($escala, $pracaId, $escalaConfig, $periodoEscala, $areaEntrega);
        }

        return view('escala.agendar', compact('requisicaoValida', 'praca', 'areaEntrega', 'entregador', 'escala', 'periodoEscala', 'tabelaEscala'));
    }
    
    private function gerarTabelaEscalaAgendamento($escala, $pracaId, $escalaConfig, $periodoEscala, $areaEntrega) {
        $cabecario = collect(['Turnos']);
        $dias = Dia::orderBy('codigo2', 'asc')->get();
        $dias->each(function ($dia) use($cabecario) {
            $cabecario->push($dia->nome);
        });

        $dayOfWeek = Carbon::now()->dayOfWeek;
        $diaInicial = Dia::where('codigo', $dayOfWeek)->first();
        //Dias validos se forem maiores que o dia atual
        
        $diasValidos = Dia::where('codigo2', '>', $diaInicial->codigo2)->get();
        
        $turnos = Turno::where('praca_id', $pracaId)
            ->get();

        $corpo = new Collection();
        $turnos->each(function ($turno) use($diasValidos, $escalaConfig, $escala, $corpo, $dias, $periodoEscala, $areaEntrega) {
            $tabelaCriacaoEscalaVM = new TabelaCriacaoEscalaVM();
            $tabelaCriacaoEscalaVM->turno = $turno;
            $diasVM = new Collection();
            $dias->each(function ($dia) use($diasValidos, $escalaConfig, $turno, $escala, $diasVM, $periodoEscala, $areaEntrega) {
                $diaVM = new DiaVM();
                $diaVM->ativo = false;
                $diaVM->emConfig = false;
                $diaVM->id = $dia->id;
                $saldoTurnoVagas = SaldoTurnoVagas::with('turno')
                    ->where('periodo_escala_id', $periodoEscala->id)
                    ->where('area_entrega_id', $areaEntrega->id)
                    ->where('turno_id', $turno->id)
                    ->where('dia_id', $dia->id)
                    ->first();
                $diaVM->temVaga = $saldoTurnoVagas != null && $saldoTurnoVagas->saldo_vagas > 0;
                //dia definido em configuracao de escala
                $escalaConfigTurno = $escalaConfig->escalaConfigTurnos->where('turno_id', $turno->id)->first();
                if($escalaConfigTurno != null)
                    $diaVM->emConfig = $escalaConfigTurno->dias->some('id', $dia->id);

                //se usuario tem escala entao eh atualizar escala, se nao eh nova escal
                if($escala != null) {
                    $escalaTurno = $escala->escalaTurnos->where('turno_id', $turno->id)->first();
                    if($escalaTurno != null)
                        $diaVM->ativo = $escalaTurno->dias->some('id', $dia->id);
                }
                $diaValido = Carbon::now()->lt($periodoEscala->data_inicial) ? true : $diasValidos->some('id', $diaVM->id);
                //$diaValido = true;
                $diaVM->exibir = $diaValido && ($diaVM->ativo || ($diaVM->emConfig && $diaVM->temVaga));
                $diaVM->nome = $diaVM->temVaga ? $dia->nome : "{$dia->nome} (Sem Vagas)";
                $diasVM->push($diaVM);
            });
            $tabelaCriacaoEscalaVM->dias = $diasVM;
            $corpo->push($tabelaCriacaoEscalaVM);
        });
        
        return  [
            "cabecario" => $cabecario,
            "corpo" => $corpo
        ];
    }
    
    private function gerarTabelaEscalaVisualizacao($escala, $pracaId, $saldoTurnosVagas = null, $escalaConfig = null) {
        $cabecario = collect(['Turnos']);
        $dias = Dia::orderBy('codigo2', 'asc')->get();
        $dias->each(function ($dia) use($cabecario) {
            $cabecario->push($dia->nome);
        });

        $turnos = Turno::where('praca_id', $pracaId)
            ->get();

        $corpo = new Collection();
        $turnos->each(function ($turno) use($escala, $corpo, $dias) {
            $tabelaCriacaoEscalaVM = new TabelaCriacaoEscalaVM();
            $tabelaCriacaoEscalaVM->turno = $turno;
            $diasVM = new Collection();
            $dias->each(function ($dia) use($turno, $escala, $diasVM) {
                $diaVM = new DiaVM();
                $diaVM->ativo = false;
                if($escala != null) {
                    $escalaTurno = $escala->escalaTurnos->where('turno_id', $turno->id)->first();
                    if($escalaTurno != null)
                        $diaVM->ativo = $escalaTurno->dias->some('id', $dia->id);
                }
                $diasVM->push($diaVM);
            });
            $tabelaCriacaoEscalaVM->dias = $diasVM;
            $corpo->push($tabelaCriacaoEscalaVM);
        });
        
        return  [
            "cabecario" => $cabecario,
            "corpo" => $corpo
        ];
    }

    public function storeAgendar(Request $request)
    {
        try {
            $escalaPost = $request->all();

            $requisicaoValida = [
                "valida" => true,
                "mensagem" => ""
            ];
            
            $periodoEscala = PeriodoEscala::where('codigo', $escalaPost['codigo'])
                ->where(function($query) {
                    $query->whereDate('data_inicial','<=', date('Y-m-d'))
                    ->whereDate('data_final','>=', date('Y-m-d'))
                    ->orwhereDate('data_inicial','>=', date('Y-m-d'));
                })->first();
                
            if(!$periodoEscala) {
                $requisicaoValida["valida"] = false;
                $requisicaoValida["mensagem"] = "Link expirado!";
                return view('escala.agendar', compact('requisicaoValida'));
            }

            $areaEntrega = AreaEntrega::where('codigo', $escalaPost['areaEntregaGuid'])
                ->first();

            if(!$areaEntrega){
                $requisicaoValida["valida"] = false;
                $requisicaoValida["mensagem"] = "Url inválida!";
                return view('escala.agendar', compact('requisicaoValida'));
            }

            $pracaId = $areaEntrega->praca_id;  
            $dias = Dia::all();
            $praca = Praca::find($pracaId);

            if($escalaPost['cpf']) {
                $request->validate([
                'cpf' => 'required|cpf',
                ],
                [
                    'required' => 'Campo :attribute obrigatório',
                    'cpf' => "CPF {$escalaPost['cpf']} inválido"
                ]);

                $entregador = Entregador::where('cpf', preg_replace('/\D/', '', $escalaPost['cpf']))
                    ->where('praca_id', $pracaId)
                    ->first();
                    
                if($entregador == null){
                    $requisicaoValida["valida"] = false;
                    $requisicaoValida["mensagem"] = "Não há entregador para o CPF {$escalaPost['cpf']} em {$praca->descricao}";
                    return view('escala.agendar', compact('requisicaoValida'));
                } else if(!$entregador->ativo) {
                    $requisicaoValida["valida"] = false;
                    $requisicaoValida["mensagem"] = "Entregador de CPF {$escalaPost['cpf']} inativo";
                    return view('escala.agendar', compact('requisicaoValida'));
                } else if(BloquearEntregadorArea::where('entregador_id', $entregador->id)->where('area_entrega_id', $areaEntrega->id)->exists()) {
                    $requisicaoValida["valida"] = false;
                    $requisicaoValida["mensagem"] = "Entregador de CPF {$escalaPost['cpf']} sem acesso para esta área " + $areaEntrega->descricao;
                    return view('escala.agendar', compact('requisicaoValida'));
                } else {
                    $existeEscala = Escala::with('escalaTurnos.dias', 'escalaTurnos.turno')
                        ->where('entregador_id', $entregador->id)
                        ->where('periodo_escala_id', $periodoEscala->id)
                        ->exists();
                    if($existeEscala) {
                        $requisicaoValida["valida"] = false;
                        $requisicaoValida["mensagem"] = "Entregador de CPF {$escalaPost['cpf']} já possui agendamento";
                        return view('escala.agendar', compact('requisicaoValida'));
                    }
                }

                $escalaConfig = EscalaConfig::with('escalaConfigTurnos.dias', 'escalaConfigTurnos.turno')
                    ->where('area_entrega_id', $areaEntrega->id)
                    ->first();
    
                $turnosIds = $escalaConfig->escalaConfigTurnos->map(function($escalaConfigTurno) {
                    return $escalaConfigTurno->turno_id;
                });
    
                $saldoTurnosVagas = SaldoTurnoVagas::with('turno')
                    ->where('periodo_escala_id', $periodoEscala->id)
                    ->where('area_entrega_id', $areaEntrega->id)
                    ->whereIn('turno_id', $turnosIds->all())
                    ->get();
    
                if(!$saldoTurnosVagas->some('saldo_vagas', '>', 0)){
                    $requisicaoValida["valida"] = false;
                    $requisicaoValida["mensagem"] = "Não há vagas disponíveis para nenhum turno";
                    return view('escala.agendar', compact('requisicaoValida'));
                }
                
                foreach ($escalaPost['turnoDia'] as $turnoId => $diasIds) {
                    if(!$turnosIds->some($turnoId)) {
                        $requisicaoValida["valida"] = false;
                        $requisicaoValida["mensagem"] = "Turno inválido para entregador de CPF {$escalaPost['cpf']}";
                        return view('escala.agendar', compact('requisicaoValida'));
                    }
                    $dias = $escalaConfig->escalaConfigTurnos->where('turno_id', $turnoId)->first()->dias;
                    $diasIdsConfig = $dias->map(function($dia) {
                        return $dia->id;
                    });
                    
                    $diasIdsCollect = collect($diasIds);
                    $differentItems = $diasIdsCollect->diff($diasIdsConfig);
                    //verificar logica
                    if(!$differentItems->isEmpty()) {
                        $requisicaoValida["valida"] = false;
                        $requisicaoValida["mensagem"] = "Dias e turnos inválidos para entregador de CPF {$escalaPost['cpf']}";
                        return view('escala.agendar', compact('requisicaoValida'));
                    }
                    
                    /*foreach ($diasIdsCollect as $diaId) {
                        if($saldoTurnosVagas->where('turno_id', $turnoId)->where('dia_id', $diaId)->some('saldo_vagas', 0)) {
                            $requisicaoValida["valida"] = false;
                            $requisicaoValida["mensagem"] = "Não há vagas para o turno selecionado (CPF {$escalaPost['cpf']})";
                            return view('escala.agendar', compact('requisicaoValida'));
                        }
                    }*/
                }
                
                DB::beginTransaction();

                $escala = new Escala();
                $escala->entregador_id = $entregador->id;
                $escala->periodo_escala_id = $periodoEscala->id;
                $escala->area_entrega_id = $areaEntrega->id;
                $escala->created_at = Carbon::now();
                $escala->save();

                foreach ($escalaPost['turnoDia'] as $turnoId => $diasIds) {
                    $escalaTurno = new EscalaTurno();
                    $escalaTurno->et_morph_id = $escala->id;
                    $escalaTurno->et_morph_type = Escala::class;
                    $escalaTurno->turno_id = $turnoId;
                    $escalaTurno->save();
                    foreach ($diasIds as $diaId) {
                        if(SaldoTurnoVagas::with('turno')
                            ->where('periodo_escala_id', $periodoEscala->id)
                            ->where('area_entrega_id', $areaEntrega->id)
                            ->where('turno_id', $turnoId)
                            ->where('dia_id', $diaId)
                            ->where('saldo_vagas', '>', 0)
                                ->exists()) {
                            $diasSemanal = new DiasSemanal();
                            $diasSemanal->escala_turno_id = $escalaTurno->id;
                            $diasSemanal->dia_id = $diaId;
                            $diasSemanal->save();
                        }
                    }
                }
                DB::commit();
            }            
            //verificar duplicidades de dados colocar unique key multiple
            return back()->with('success', "Agendamento cadastrada com sucesso!");
        } catch (PDOException $ex) {
            DB::rollBack();
            if ($ex->getCode() == 23505)
                return redirect()->back()->withInput()->with('error', 'Estes dados já foram cadastrados!');    
            return redirect()->back()->withInput()->with('error', $ex->getMessage());
        } catch (Exception $ex) {
            DB::rollBack();
            return redirect()->back()->withInput()->with('error', $ex->getMessage());
        }        
    }
   
    public function storeAtualizarAgendamento(Request $request)
    {
        try {
            $escalaPost = $request->all();

            if(!isset($escalaPost['turnoDia']))
                return back()->with('error', "Preencha ao menos um dia do agendamento!");

            $requisicaoValida = [
                "valida" => true,
                "mensagem" => ""
            ];
            
            $periodoEscala = PeriodoEscala::where('codigo', $escalaPost['codigo'])
                ->where(function($query) {
                    $query->whereDate('data_inicial','<=', date('Y-m-d'))
                    ->whereDate('data_final','>=', date('Y-m-d'))
                    ->orwhereDate('data_inicial','>=', date('Y-m-d'));
                })->first();
                
            if(!$periodoEscala){
                $requisicaoValida["valida"] = false;
                $requisicaoValida["mensagem"] = "Link expirado!";
                return view('escala.agendar', compact('requisicaoValida'));
            }

            $areaEntrega = AreaEntrega::where('codigo', $escalaPost['areaEntregaGuid'])
                ->first();

            if(!$areaEntrega){
                $requisicaoValida["valida"] = false;
                $requisicaoValida["mensagem"] = "Url inválida!";
                return view('escala.agendar', compact('requisicaoValida'));
            }

            $pracaId = $areaEntrega->praca_id; 

            $dias = Dia::all();
            $praca = Praca::find($pracaId);

            if($escalaPost['cpf']) {
                $request->validate([
                'cpf' => 'required|cpf',
                ],
                [
                    'required' => 'Campo :attribute obrigatório',
                    'cpf' => "CPF {$escalaPost['cpf']} inválido"
                ]);

                $entregador = Entregador::where('cpf', preg_replace('/\D/', '', $escalaPost['cpf']))
                    ->where('praca_id', $pracaId)
                    ->first();
                    
                if($entregador == null){
                    $requisicaoValida["valida"] = false;
                    $requisicaoValida["mensagem"] = "Não há entregador para o CPF {$escalaPost['cpf']} em {$praca->descricao}";
                    return view('escala.agendar', compact('requisicaoValida'));
                } else if(!$entregador->ativo) {
                    $requisicaoValida["valida"] = false;
                    $requisicaoValida["mensagem"] = "Entregador de CPF {$escalaPost['cpf']} inativo";
                    return view('escala.agendar', compact('requisicaoValida'));
                } else if(BloquearEntregadorArea::where('entregador_id', $entregador->id)->where('area_entrega_id', $areaEntrega->id)->exists()) {
                    $requisicaoValida["valida"] = false;
                    $requisicaoValida["mensagem"] = "Entregador de CPF {$escalaPost['cpf']} sem acesso para esta área " + $areaEntrega->descricao;
                    return view('escala.agendar', compact('requisicaoValida'));
                }

                $escala = Escala::with('escalaTurnos.dias', 'escalaTurnos.turno')
                    ->where('entregador_id', $entregador->id)
                    ->where('periodo_escala_id', $periodoEscala->id)
                    ->first();

                $escalaConfig = EscalaConfig::with('escalaConfigTurnos.dias', 'escalaConfigTurnos.turno')
                    ->where('area_entrega_id', $areaEntrega->id)
                    ->first();
    
                $turnosConfigIds = $escalaConfig->escalaConfigTurnos->map(function($escalaConfigTurno) {
                    return $escalaConfigTurno->turno_id;
                });
    
                $saldoTurnosVagas = SaldoTurnoVagas::with('turno')
                    ->where('periodo_escala_id', $periodoEscala->id)
                    ->where('area_entrega_id', $areaEntrega->id)
                    ->whereIn('turno_id', $turnosConfigIds->all())
                    ->get();
    
                if(!$saldoTurnosVagas->some('saldo_vagas', '>', 0)){
                    $requisicaoValida["valida"] = false;
                    $requisicaoValida["mensagem"] = "Não há vagas disponíveis para nenhum turno";
                    return view('escala.agendar', compact('requisicaoValida'));
                }
                
                foreach ($escalaPost['turnoDia'] as $turnoId => $diasIds) {
                    if(!$turnosConfigIds->some($turnoId)) {
                        $requisicaoValida["valida"] = false;
                        $requisicaoValida["mensagem"] = "Turno inválido para entregador de CPF {$escalaPost['cpf']}";
                        return view('escala.agendar', compact('requisicaoValida'));
                    }
                    $escalaConfigTurno = $escalaConfig->escalaConfigTurnos->where('turno_id', $turnoId)->first();
                    $dias = $escalaConfigTurno->dias;
                    $diasIdsConfig = $dias->map(function($dia) {
                        return $dia->id;
                    });
                    
                    $diasIdsCollect = collect($diasIds);
                    $differentItems = $diasIdsCollect->diff($diasIdsConfig);
                    if(!$differentItems->isEmpty()) {
                        $requisicaoValida["valida"] = false;
                        $requisicaoValida["mensagem"] = "Dias e turnos inválidos para entregador de CPF {$escalaPost['cpf']}";
                        return view('escala.agendar', compact('requisicaoValida'));
                    }
                    
                    /*foreach ($diasIdsCollect as $diaId) {
                        if(!$saldoTurnosVagas->where('turno_id', $turnoId)->where('dia_id', $diaId)->some('saldo_vagas', '>', 0)) {
                            $requisicaoValida["valida"] = false;
                            $requisicaoValida["mensagem"] = "Não há vagas para o turno selecionado (CPF {$escalaPost['cpf']})";
                            return view('escala.agendar', compact('requisicaoValida'));
                        }
                    }*/
                }
                                
                $turnosIdsInput = array_keys($escalaPost['turnoDia']);                
                $turnosEscalaIds = $escala->escalaTurnos->map(function($escalaTurno) {
                    return $escalaTurno->turno_id;
                });
                $turnosIds = $turnosEscalaIds->merge(collect($turnosIdsInput))->unique();

                DB::beginTransaction();
                foreach ($turnosIds as $turnoId) {
                    $diasIds = isset($escalaPost['turnoDia'][$turnoId]) ? $escalaPost['turnoDia'][$turnoId] : [];
                    $escalaTurno = $escala->escalaTurnos->where('turno_id', $turnoId)->first();
                    if($escalaTurno != null) {
                        $diasIdsCollect = collect($diasIds);
                        $diasIds = $diasIdsCollect->map(function($diaId) use($turnoId, $periodoEscala, $areaEntrega) {
                            if(SaldoTurnoVagas::where('periodo_escala_id', $periodoEscala->id)
                                ->where('area_entrega_id', $areaEntrega->id)
                                ->where('turno_id', $turnoId)
                                ->where('dia_id', $diaId)
                                ->where('saldo_vagas', '>', 0)
                                ->exists()) {
                                return $diaId;
                            }
                            return false;
                        })->reject(function ($value) {
                            return $value === false;
                        });
                        $escalaTurno->dias()->sync($diasIds->all());
                        if(!$escalaTurno->dias()->exists()) {
                            $escalaTurno->delete();
                        }
                    } else {
                        $escalaTurno = new EscalaTurno();
                        $escalaTurno->et_morph_id = $escala->id;
                        $escalaTurno->et_morph_type = Escala::class;
                        $escalaTurno->turno_id = $turnoId;
                        $escalaTurno->save();
                        foreach ($diasIds as $diaId) {
                            if(SaldoTurnoVagas::with('turno')
                                ->where('periodo_escala_id', $periodoEscala->id)
                                ->where('area_entrega_id', $areaEntrega->id)
                                ->where('turno_id', $turnoId)
                                ->where('dia_id', $diaId)
                                ->where('saldo_vagas', '>', 0)
                                ->exists()) {
                                $diasSemanal = new DiasSemanal();
                                $diasSemanal->escala_turno_id = $escalaTurno->id;
                                $diasSemanal->dia_id = $diaId;
                                $diasSemanal->save();
                            }
                        }
                    }
                }
                DB::commit();
            }

            //verificar duplicidades de dados colocar unique key multiple
            
            return back()->with('success', "Agendamento atualizado com sucesso!");
        } catch (PDOException $ex) {
            DB::rollBack();
            if ($ex->getCode() == 23505)
                return redirect()->back()->withInput()->with('error', 'Estes dados já foram cadastrados!');    
            return redirect()->back()->withInput()->with('error', $ex->getMessage());
        } catch (Exception $ex) {
            DB::rollBack();
            return redirect()->back()->withInput()->with('error', $ex->getMessage());
        }        
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DadosBancario  $dadosBancario
     * @return \Illuminate\Http\Response
     */
    public function excluir($escalaId)
    {
        try {
            DB::beginTransaction();

            $escala = Escala::with('escalaTurnos.diasSemanais')
                ->where('id', $escalaId)
                ->first();
                
            foreach ($escala->escalaTurnos as $escalaTurno) {
                foreach ($escalaTurno->diasSemanais as $diasSemanal) {
                    $diasSemanal->delete();
                }
                $escalaTurno->delete();
            }        
            $escala->delete();

            DB::commit();
            return redirect()->back()
                ->with('success', 'Agendamento excluido com sucesso!');
        } catch (PDOException $ex) {
            DB::rollBack();
            if ($ex->getCode() == 23505)
                return redirect()->back()->withInput()->with('error', 'Estes dados já foram cadastrados!');    
            return redirect()->back()->withInput()->with('error', $ex->getMessage());
        } catch (Exception $ex) {
            DB::rollBack();
            return redirect()->back()->withInput()->with('error', $ex->getMessage());
        }  
    }

    public function dados(Request $request)
    {
        //$periodoEscalaId = request()->get('periodoEscalaId');
        $escalaQuery = Escala::with(['entregador.praca', 'periodoEscala', 'areaEntrega'])
            ->orderBy('created_at', 'desc');
        
        if($request->get('search')) {
            $search = $request->get('search');
            if($search['value']){
                $escalaQuery = $escalaQuery->whereHas('entregador', function ($query) use($search) {
                    return $query->where('entregador.nome_completo', 'like', "%{$search['value']}%");
                })->orWhereHas('entregador.praca', function ($query) use($search) {
                    return $query->where('praca.descricao', 'like', "%{$search['value']}%");
                });
            }
        }
        /*
        if(isset($input['praca'])) {
            $escalaQuery = $escalaQuery->whereHas('praca', function ($query) use($input) {
                return $query->where('praca.descricao', 'like', "%{$input['praca']}%");
            });
        }*/

        return DataTables::eloquent($escalaQuery)
            ->addColumn('entregador', function (Escala $escala) {
                return $escala->entregador->nome_completo;
            })
            ->addColumn('praca', function (Escala $escala) {
                return $escala->entregador->praca->descricao;
            })
            ->addColumn('periodoEscala', function (Escala $escala) {
                $dataInicial = Carbon::parse($escala->periodoEscala->data_inicial)->format('d/m/Y');
                $dataFinal = Carbon::parse($escala->periodoEscala->data_final)->format('d/m/Y');
                return "{$dataInicial} à {$dataFinal}";
            })
            ->addColumn('areaEntrega', function (Escala $escala) {
                return $escala->areaEntrega->descricao;
            })
            ->addColumn('cadastro', function (Escala $escala) {
                return Carbon::parse($escala->created_at)->format('d/m/Y H:i');
            })
            ->addColumn("action", function($row){
                $routeView = route('escala.visualizar', $row['id']);
                $routeEdit = route('escala.agendar') . "/{$row->periodoEscala->codigo}/{$row->areaEntrega->codigo}/{$row->entregador->cpf}";
                $routeDelete = route('escala.excluir', $row['id']);
                $btn = "<a href='{$routeView}' class='btn btn-sm btn-clean btn-icon btn-icon-md'><i class='la la-search text-primary'></i></a>";
                $btn = $btn."<a href='{$routeEdit}' class='edit btn btn-sm btn-clean btn-icon btn-icon-md'><i class='la la-edit text-warning'></i></a>";
                $btn = $btn."<a href='javascript:void(0)' class='delete btn btn-sm btn-clean btn-icon btn-icon-md' data-key='{$row['id']}' data-name='{$row['entregador']['nome_completo']}' data-url='{$routeDelete}' onclick='proccessDeletion(this)'><i class='la la-trash text-danger'></i></a>";

                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function dadosPorArea(Request $request)
    {
        $escalaGroupQuery = DB::table('escala')
            ->selectRaw('praca.descricao praca, entregador.praca_id, escala.periodo_escala_id, periodo_escala.created_at, periodo_escala.data_inicial, periodo_escala.data_final, escala.area_entrega_id, area_entrega.descricao area_entrega')
            ->join('entregador', 'escala.entregador_id', '=', 'entregador.id')
            ->join('praca', 'entregador.praca_id', '=', 'praca.id')
            ->join('periodo_escala', 'escala.periodo_escala_id', '=', 'periodo_escala.id')
            ->join('area_entrega', 'escala.area_entrega_id', '=', 'area_entrega.id')
            ->groupByRaw('praca.descricao, entregador.praca_id, escala.periodo_escala_id, periodo_escala.created_at, periodo_escala.data_inicial, periodo_escala.data_final, escala.area_entrega_id, area_entrega.descricao')
            ->orderByRaw('praca.descricao, periodo_escala.created_at desc, area_entrega.descricao');
        
            if($request->get('search')) {
                $search = $request->get('search');
                if($search['value']) {
                    $escalaGroupQuery->where(function ($escalaGroupQuery) use($search) {
                        return $escalaGroupQuery
                            ->orWhere('area_entrega.descricao', 'like', "%{$search['value']}%")
                            ->orWhere('praca.descricao', 'like', "%{$search['value']}%");
                    });
                    /*$escalaGroupQuery = $escalaGroupQuery->whereHas('entregador', function ($query) use($search) {
                        return $query->where('entregador.nome_completo', 'like', "%{$search['value']}%");
                    })->orWhereHas('entregador.praca', function ($query) use($search) {
                        return $query->where('praca.descricao', 'like', "%{$search['value']}%");
                    });*/
                }
            }

        return DataTables::query($escalaGroupQuery)
            ->addColumn('periodoEscala', function ($escala) {
                $dataInicial = Carbon::parse($escala->data_inicial)->format('d/m/Y');
                $dataFinal = Carbon::parse($escala->data_final)->format('d/m/Y');
                return "{$dataInicial} à {$dataFinal}";
            })
            ->addColumn("action", function($row){
                $routeView = route('escala.visualizar-por-area', [$row->praca_id, $row->periodo_escala_id, $row->area_entrega_id]);
                $btn = "<a href='{$routeView}' class='btn btn-sm btn-clean btn-icon btn-icon-md'><i class='la la-search text-primary'></i></a>";
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
