<?php

namespace App\Services\Application\Dashboard;

use App\Models\Dashboard\DadosAracaju;
use App\Models\Dashboard\DadosDashboard;
use App\Models\Dashboard\DadosSalvador;
use App\Models\ViewTodosDado;
use App\Models\FiltroDashboard;
use Carbon\Carbon;
use Exception;

class DashboardService
{
    public function obterDadosDashboard(FiltroDashboard $filtroDashboard, $semanasAnos = [])
    {
        $dadosDashboard = new DadosDashboard();
        $dadosDashboard->semanasAnos = $this->obterSemanasAnos();
        $dadosDashboard->semanaAnoAtual = $this->obterSemanaAnoAtual();
        $dadosDashboard->filtroDashboard = $filtroDashboard;
        if($filtroDashboard->semanasAnos) {
            $dadosDashboard->filtroDashboard->semanasAnos = $filtroDashboard->semanasAnos;
        } else {
            $dadosDashboard->filtroDashboard->semanasAnos = [$dadosDashboard->semanaAnoAtual];
        }

        $dadosDashboard->dadosGerais = $this->obterDadosGerais($filtroDashboard);  

        $dadosSalvador = new DadosSalvador();
        $dadosSalvador->nomePraca = "Salvador";
        $filtroDashboard->nomePraca = $dadosSalvador->nomePraca;
        $dadosSalvador->dadosPraca = $this->obterDadosPorPraca($filtroDashboard);
        $dadosSalvador->tempoOnlineModalidade = $this->obterTempoOnlinePorModalidade($filtroDashboard);
        $dadosSalvador->tempoOnlineModalidadeJson = $this->obterTempoOnlinePorModalidadeJson($dadosSalvador->tempoOnlineModalidade);
        $dadosSalvador->dadosVeiculo = $this->obterDadosPorVeiculo($filtroDashboard);
        $dadosSalvador->tempoOnlineTurno = $this->obterTempoOnlinePorTurno($filtroDashboard);
        $dadosSalvador->tempoOnlineTurnoJson = $this->obterTempoOnlinePorTurnoJson($dadosSalvador->tempoOnlineTurno);
        $dadosSalvador->entregadoresTempoOnline = $this->obterEntregadoresTempoOnline($filtroDashboard);
        $dadosSalvador->entregadoresRejeite = $this->obterEntregadoresRejeite($filtroDashboard);
        $dadosSalvador->entregadoresCancelamento = $this->obterEntregadoresCancelamento($filtroDashboard);
        $dadosDashboard->salvador = $dadosSalvador;
              
        $dadosAracaju = new DadosAracaju();
        $dadosAracaju->nomePraca = "Aracaju";
        $filtroDashboard->nomePraca = $dadosAracaju->nomePraca;
        $dadosAracaju->dadosPraca = $this->obterDadosPorPraca($filtroDashboard);
        $dadosAracaju->tempoOnlineModalidade = $this->obterTempoOnlinePorModalidade($filtroDashboard);
        $dadosAracaju->tempoOnlineModalidadeJson = $this->obterTempoOnlinePorModalidadeJson($dadosAracaju->tempoOnlineModalidade);
        $dadosAracaju->dadosVeiculo = $this->obterDadosPorVeiculo($filtroDashboard);
        $dadosAracaju->tempoOnlineTurno = $this->obterTempoOnlinePorTurno($filtroDashboard);
        $dadosAracaju->tempoOnlineTurnoJson = $this->obterTempoOnlinePorTurnoJson($dadosAracaju->tempoOnlineTurno);
        $dadosAracaju->entregadoresTempoOnline = $this->obterEntregadoresTempoOnline($filtroDashboard);
        $dadosAracaju->entregadoresRejeite = $this->obterEntregadoresRejeite($filtroDashboard);
        $dadosAracaju->entregadoresCancelamento = $this->obterEntregadoresCancelamento($filtroDashboard);
        $dadosDashboard->aracaju = $dadosAracaju;

        return $dadosDashboard;
    }

    public function obterSemanasAnos()
    {
        //DB::statement("SET GLOBAL table_definition_cache = 1024");
        return ViewTodosDado::selectRaw('
            semana_ano
        ')
        ->groupBy('semana_ano')
        ->orderBy('semana_ano', 'DESC')
        ->get();
    }

    public function obterSemanaAnoAtual()
    {
        $viewTodosDado = ViewTodosDado::selectRaw('
            semana_ano
        ')
        ->orderBy('semana_ano', 'DESC')
        ->first();
        return $viewTodosDado->semana_ano;
    }

    public function obterDadosGerais(FiltroDashboard $filtroDashboard)
    {
        $viewTodosDadoQuery = ViewTodosDado::selectRaw('
            min(dia_data) data_incial,
            max(dia_data) data_final,
            ROUND(avg(tempo_online), 4) tempo_online,
            ROUND(sum(qtd_completadas)/sum(qtd_ofertadas), 4) completadas,
            ROUND(sum(qtd_rejeitadas)/sum(qtd_ofertadas), 4) rejeite,
            ROUND((sum(qtd_aceitas) - sum(qtd_completadas))/sum(qtd_ofertadas), 4) cancelamento
        ');

        if ($filtroDashboard->tipoFiltroSemanal) {
            $viewTodosDadoQuery->whereIn('semana_ano', $filtroDashboard->semanasAnos);
        } else {
            $carbonDataInicial = Carbon::createFromFormat('d/m/Y', $filtroDashboard->dataInicial);
            $carbonDataFinal = Carbon::createFromFormat('d/m/Y', $filtroDashboard->dataFinal);            
            $viewTodosDadoQuery->whereBetween('dia_data', ["{$carbonDataInicial->format('Y-m-d')} 00:00:00", "{$carbonDataFinal->format('Y-m-d')} 23:59:59"]);
        }
        
        return $viewTodosDadoQuery->first();
    }
    
    public function obterDadosPorPraca(FiltroDashboard $filtroDashboard) //$semanasAnos, $praca)
    {
        $viewTodosDadoQuery = ViewTodosDado::selectRaw('
            ROUND(avg(tempo_online), 4) tempo_online, 
            sum(qtd_ofertadas) qtd_ofertadas, 
            sum(qtd_aceitas) qtd_aceitas, 
            (sum(qtd_aceitas) - sum(qtd_completadas)) qtd_canceladas, 
            sum(qtd_completadas) qtd_completadas, 
            sum(qtd_rejeitadas) qtd_rejeitadas,
            ROUND(sum(qtd_aceitas)/sum(qtd_ofertadas), 4) aceite,
            ROUND(sum(qtd_completadas)/sum(qtd_ofertadas), 4) completada,
            ROUND(sum(qtd_rejeitadas)/sum(qtd_ofertadas), 4) rejeite,
            ROUND((sum(qtd_aceitas) - sum(qtd_completadas))/sum(qtd_ofertadas), 4) cancelamento
        ')->where('praca', $filtroDashboard->nomePraca);

        if ($filtroDashboard->tipoFiltroSemanal) {
            $viewTodosDadoQuery->whereIn('semana_ano', $filtroDashboard->semanasAnos);
        } else {
            $carbonDataInicial = Carbon::createFromFormat('d/m/Y', $filtroDashboard->dataInicial);
            $carbonDataFinal = Carbon::createFromFormat('d/m/Y', $filtroDashboard->dataFinal);            
            $viewTodosDadoQuery->whereBetween('dia_data', ["{$carbonDataInicial->format('Y-m-d')} 00:00:00", "{$carbonDataFinal->format('Y-m-d')} 23:59:59"]);
        }
        return $viewTodosDadoQuery->first();
    }

    public function obterTempoOnlinePorModalidade(FiltroDashboard $filtroDashboard)
    {
        $viewTodosDadoQuery = ViewTodosDado::selectRaw('
            modalidade,
            ROUND(avg(tempo_online), 4) tempo_online
        ')->where('praca', $filtroDashboard->nomePraca);

        if ($filtroDashboard->tipoFiltroSemanal) {
            $viewTodosDadoQuery->whereIn('semana_ano', $filtroDashboard->semanasAnos);
        } else {
            $carbonDataInicial = Carbon::createFromFormat('d/m/Y', $filtroDashboard->dataInicial);
            $carbonDataFinal = Carbon::createFromFormat('d/m/Y', $filtroDashboard->dataFinal);            
            $viewTodosDadoQuery->whereBetween('dia_data', ["{$carbonDataInicial->format('Y-m-d')} 00:00:00", "{$carbonDataFinal->format('Y-m-d')} 23:59:59"]);
        }        
        $viewTodosDadoQuery->groupBy('modalidade');
        return $viewTodosDadoQuery->get();
    }

    public function obterTempoOnlinePorModalidadeJson($tempoOnlinePorModalidade)
    {
        $dados = [];
        foreach($tempoOnlinePorModalidade as $item) {
            $dados[] = [
                "label" => $item->modalidade,
                "value" => number_format(floatval($item->tempo_online)*100, 2, ".", "")
            ];
        }
        return json_encode($dados);
    }

    public function obterDadosPorVeiculo(FiltroDashboard $filtroDashboard)
    {
        $viewTodosDadoQuery = ViewTodosDado::selectRaw('
            veiculo,
            ROUND(avg(tempo_online), 4) tempo_online, 
            sum(qtd_ofertadas) qtd_ofertadas, 
            sum(qtd_aceitas) qtd_aceitas, 
            sum(qtd_completadas) qtd_completadas, 
            (sum(qtd_aceitas) - sum(qtd_completadas)) qtd_canceladas, 
            sum(qtd_rejeitadas) qtd_rejeitadas,
            ROUND(sum(qtd_rejeitadas)/sum(qtd_ofertadas), 4) rejeite,
            ROUND((sum(qtd_aceitas) - sum(qtd_completadas))/sum(qtd_ofertadas), 4) cancelamento
        ')->where('praca', $filtroDashboard->nomePraca);

        if ($filtroDashboard->tipoFiltroSemanal) {
            $viewTodosDadoQuery->whereIn('semana_ano', $filtroDashboard->semanasAnos);
        } else {
            $carbonDataInicial = Carbon::createFromFormat('d/m/Y', $filtroDashboard->dataInicial);
            $carbonDataFinal = Carbon::createFromFormat('d/m/Y', $filtroDashboard->dataFinal);            
            $viewTodosDadoQuery->whereBetween('dia_data', ["{$carbonDataInicial->format('Y-m-d')} 00:00:00", "{$carbonDataFinal->format('Y-m-d')} 23:59:59"]);
        }        
        $viewTodosDadoQuery->groupBy('veiculo');
        return $viewTodosDadoQuery->get();
    }

    public function obterTempoOnlinePorTurno(FiltroDashboard $filtroDashboard)
    {
        $viewTodosDadoQuery = ViewTodosDado::selectRaw('
            turno,
            ROUND(avg(tempo_online), 4) tempo_online
        ')->where('praca', $filtroDashboard->nomePraca);

        if ($filtroDashboard->tipoFiltroSemanal) {
            $viewTodosDadoQuery->whereIn('semana_ano', $filtroDashboard->semanasAnos);
        } else {
            $carbonDataInicial = Carbon::createFromFormat('d/m/Y', $filtroDashboard->dataInicial);
            $carbonDataFinal = Carbon::createFromFormat('d/m/Y', $filtroDashboard->dataFinal);            
            $viewTodosDadoQuery->whereBetween('dia_data', ["{$carbonDataInicial->format('Y-m-d')} 00:00:00", "{$carbonDataFinal->format('Y-m-d')} 23:59:59"]);
        }
        $viewTodosDadoQuery->groupBy('turno');
        return $viewTodosDadoQuery->get();
    }

    public function obterTempoOnlinePorTurnoJson($tempoOnlinePorTurno)
    {
        $cores = ["Tomato", "Orange", "DodgerBlue", "MediumSeaGreen", "Gray", "SlateBlue", "Violet", "LightGray", "#ff9393", "#gf9393", "#hf9393"];
        $dados = [];
        foreach($tempoOnlinePorTurno as $key => $item) {
            $dados[] = [
                "turno" => $item->turno,
                "tempo_online" => number_format(floatval($item->tempo_online)*100, 2, ".", ""),                
                "color" => isset($cores[$key]) ? $cores[$key] : ""
            ];
        }
        return json_encode($dados);
    }

    public function obterEntregadoresTempoOnline(FiltroDashboard $filtroDashboard)
    {
        $viewTodosDadoQuery = ViewTodosDado::selectRaw('
            entregador,
            ROUND(avg(tempo_online), 4) tempo_online
        ')->where('praca', $filtroDashboard->nomePraca);

        if ($filtroDashboard->tipoFiltroSemanal) {
            $viewTodosDadoQuery->whereIn('semana_ano', $filtroDashboard->semanasAnos);
        } else {
            $carbonDataInicial = Carbon::createFromFormat('d/m/Y', $filtroDashboard->dataInicial);
            $carbonDataFinal = Carbon::createFromFormat('d/m/Y', $filtroDashboard->dataFinal);            
            $viewTodosDadoQuery->whereBetween('dia_data', ["{$carbonDataInicial->format('Y-m-d')} 00:00:00", "{$carbonDataFinal->format('Y-m-d')} 23:59:59"]);
        }
        return $viewTodosDadoQuery->groupBy('entregador');
    }

    public function obterEntregadoresRejeite(FiltroDashboard $filtroDashboard)
    {
        $viewTodosDadoQuery = ViewTodosDado::selectRaw('
            entregador,
            sum(qtd_ofertadas) qtd_ofertadas,
            sum(qtd_rejeitadas) qtd_rejeitadas,
            ROUND(sum(qtd_rejeitadas)/sum(qtd_ofertadas), 4) rejeite
        ')->where('praca', $filtroDashboard->nomePraca);

        if ($filtroDashboard->tipoFiltroSemanal) {
            $viewTodosDadoQuery->whereIn('semana_ano', $filtroDashboard->semanasAnos);
        } else {
            $carbonDataInicial = Carbon::createFromFormat('d/m/Y', $filtroDashboard->dataInicial);
            $carbonDataFinal = Carbon::createFromFormat('d/m/Y', $filtroDashboard->dataFinal);            
            $viewTodosDadoQuery->whereBetween('dia_data', ["{$carbonDataInicial->format('Y-m-d')} 00:00:00", "{$carbonDataFinal->format('Y-m-d')} 23:59:59"]);
        }
        return $viewTodosDadoQuery->groupBy('entregador');
    }

    public function obterEntregadoresCancelamento(FiltroDashboard $filtroDashboard)
    {
        $viewTodosDadoQuery = ViewTodosDado::selectRaw('
            entregador,
            sum(qtd_ofertadas) qtd_ofertadas, 
            sum(qtd_aceitas) qtd_aceitas, 
            sum(qtd_completadas) qtd_completadas,
            (sum(qtd_aceitas) - sum(qtd_completadas)) qtd_canceladas, 
            ROUND(sum(qtd_completadas)/sum(qtd_ofertadas), 4) completadas,
            ROUND((sum(qtd_aceitas) - sum(qtd_completadas))/sum(qtd_ofertadas), 4) cancelamento
        ')->where('praca', $filtroDashboard->nomePraca);

        if ($filtroDashboard->tipoFiltroSemanal) {
            $viewTodosDadoQuery->whereIn('semana_ano', $filtroDashboard->semanasAnos);
        } else {
            $carbonDataInicial = Carbon::createFromFormat('d/m/Y', $filtroDashboard->dataInicial);
            $carbonDataFinal = Carbon::createFromFormat('d/m/Y', $filtroDashboard->dataFinal);            
            $viewTodosDadoQuery->whereBetween('dia_data', ["{$carbonDataInicial->format('Y-m-d')} 00:00:00", "{$carbonDataFinal->format('Y-m-d')} 23:59:59"]);
        }
        return $viewTodosDadoQuery->groupBy('entregador');
    }
}