<?php

namespace App\Services\Util;


class LaravelBuilder extends \Lavary\Menu\Builder
{

    /**
     * Generate the menu items as list items using a recursive function.
     *
     * @param string   $type
     * @param int      $parent
     * @param array    $children_attributes
     * @param array    $item_attributes
     * @param callable $item_after_calback
     * @param array    $item_after_calback_params
     *
     * @return string
     */
    public function render($type = 'ul', $parent = null, $children_attributes = [], $item_attributes = [], $item_after_calback = null, $item_after_calback_params = [])
    {
        $items = '';

        $item_tag = in_array($type, array('ul', 'ol')) ? 'li' : $type;

        foreach ($this->whereParent($parent) as $item) {
            if ($item->link) {
                $link_attr = $item->link->attr();
                if (is_callable($item_after_calback)) {
                    call_user_func_array($item_after_calback, [
                        $item,
                        &$children_attributes,
                        &$item_attributes,
                        &$link_attr,
                        &$item_after_calback_params,
                    ]);
                }
            }
            $all_attributes = array_merge($item_attributes, $item->attr()) ;
            if (isset($item_attributes['class'])) {
                $all_attributes['class'] = $all_attributes['class'].' '.$item_attributes['class'] ;
            }
            $items .= '<'.$item_tag.self::attributes($all_attributes).'>';

            if ($item->link) {
                $items .= $item->beforeHTML.'<a'.self::attributes($link_attr).(!empty($item->url()) ? ' href="'.$item->url().'"' : '').'>'.$item->title.'</a>'.$item->afterHTML;
            } else {
                $items .= $item->title;
            }

            if ($item->hasChildren()) {
                $items .= '<div class="kt-menu__submenu"><span class="kt-menu__arrow"></span><'.$type.self::attributes($children_attributes).'>';
                // Recursive call to children.
                $items .= $this->render($type, $item->id, $children_attributes, $item_attributes, $item_after_calback, $item_after_calback_params);
                $items .= "</{$type}></div>";
            }

            $items .= "</{$item_tag}>";

            if ($item->divider) {
                $items .= '<'.$item_tag.self::attributes($item->divider).'></'.$item_tag.'>';
            }
        }

        return $items;
    }
    
}