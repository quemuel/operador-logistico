<?php

namespace App\Providers;

use App\Services\Util\Cpf;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultstringLength(191);
        Validator::extend('cpf', function ($attribute, $value, $parameters, $validator) {
            return (new Cpf())->isValid($value);
        });
    }
}
