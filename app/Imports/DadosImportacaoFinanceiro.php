<?php

namespace App\Imports;

use App\Models\DadosBancario;
use App\Models\Faturamento;
use App\Models\PagamentoDadosBancario;
use App\Models\TbDado;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;

class DadosImportacaoFinanceiro implements ToModel, WithCustomCsvSettings, WithStartRow, WithBatchInserts
{

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $praca = strtolower($row[2]);
        $dadosBancarioQuery = DadosBancario::whereRaw('lower(municipio) = (?)', ["{$praca}"]);
        $entregadorArray = explode(" ", trim($row[4]));
        foreach($entregadorArray as $entregador) {
            $entregador = strtolower($entregador);
            $dadosBancarioQuery->whereRaw('lower(nome) like (?)', ["%{$entregador}%"]);
        }

        $dadosBancario = $dadosBancarioQuery->first();
        
        return new Faturamento([
            'data_turno' => $row[0],
            'turno' => $row[1],
            'praca' => $row[2],
            'entregador' => $row[4],
            'tipo' => $row[5],
            'valor' => str_replace(",", ".", $row[6]),
            'dados_bancario_id' => $dadosBancario != null ? $dadosBancario->id : null,
            'descricao' => $row[7]
        ]);
    }
    
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';'
        ];
    }
    
    public function startRow(): int
    {
        return 2;
    }
    
    public function batchSize(): int
    {
        return 20;
    }
}
