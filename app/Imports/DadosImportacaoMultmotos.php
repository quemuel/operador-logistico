<?php

namespace App\Imports;

use App\Models\TbDadoMultmotos;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;

class DadosImportacaoMultmotos implements ToModel, WithCustomCsvSettings, WithStartRow, WithBatchInserts
{

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new TbDadoMultmotos([
            'data' => $row[0],
            'turno' => $row[1],
            'tag' => $row[2],
            'id do entregador' => $row[3],
            'entregador' => $row[4],
            'operador logistico' => $row[5],
            'praca' => $row[6],
            'tempo online' => $row[7],
            'tempo online absoluto' => $row[8],
            'tempo bloqueado (%)' => $row[9],
            'tempo bloqueado (HH:MM)' => $row[10],
            'numero de pedidos aceitos e completados' => $row[11],
            'numero de corridas aceitas' => $row[12],
            'numero de corridas completadas' => $row[13],
            'numero de corridas ofertadas' => $row[14],
            'numero de corridas rejeitadas' => $row[15],
            'soma das taxas das corridas aceitas' => $row[16],
            'soma das distancias das corridas aceitas (km)' => $row[17]
        ]);
    }
    
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';'
        ];
    }
    
    public function startRow(): int
    {
        return 2;
    }
    
    public function batchSize(): int
    {
        return 20;
    }
}
