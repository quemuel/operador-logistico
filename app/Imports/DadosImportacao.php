<?php

namespace App\Imports;

use App\Models\TbDado;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;

class DadosImportacao implements ToModel, WithCustomCsvSettings, WithStartRow, WithBatchInserts
{

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if(count($row) > 14) {
            return new TbDado([
                'data' => $row[0],
                'turno' => $row[1],
                'tag' => $row[2],
                'id do entregador' => $row[3],
                'entregador' => $row[4],
                'praca' => $row[5],
                'sub praca' => $row[6],
                'tempo online' => $row[7],
                'tempo online absoluto' => $row[8],
                'numero de pedidos aceitos e completados' => $row[9],
                'numero de corridas aceitas' => $row[10],
                'numero de corridas completadas' => $row[11],
                'numero de corridas ofertadas' => $row[12],
                'numero de corridas rejeitadas' => $row[13],
                'soma das taxas das corridas aceitas' => $row[14]
            ]);
        } else {
            return new TbDado([
                'data' => $row[0],
                'turno' => $row[1],
                'tag' => $row[2],
                'id do entregador' => $row[3],
                'entregador' => $row[4],
                'praca' => $row[5],
                'tempo online' => $row[6],
                'tempo online absoluto' => $row[7],
                'numero de pedidos aceitos e completados' => $row[8],
                'numero de corridas aceitas' => $row[9],
                'numero de corridas completadas' => $row[10],
                'numero de corridas ofertadas' => $row[11],
                'numero de corridas rejeitadas' => $row[12],
                'soma das taxas das corridas aceitas' => $row[13]
            ]);
        }
    }
    
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';'
        ];
    }
    
    public function startRow(): int
    {
        return 2;
    }
    
    public function batchSize(): int
    {
        return 20;
    }
}
