SELECT
	/*min(dia_data) dataInicial,
	max(dia_data) dataFinal,*/
	count(distinct dia_data) qtd_dia,
    entregador,
	/*ROUND(avg(qtd_turno), 2) qtd_turno, */
    sum(qtd_rejeitadas) qtd_rejeitadas,
    sum(qtd_canceladas) qtd_canceladas, 
	ROUND(avg(tempo_online), 6) tempo_online
FROM (
	SELECT dia_data, 
		entregador, praca,
		/*sum(qtd_ofertadas) qtd_ofertadas, 
		sum(qtd_aceitas) qtd_aceitas,  
		sum(qtd_completadas) qtd_completadas, */
		count(distinct turno) qtd_turno,
		sum(qtd_rejeitadas) qtd_rejeitadas,
		(sum(qtd_aceitas) - sum(qtd_completadas)) qtd_canceladas,
		ROUND(avg(tempo_online), 6) tempo_online
	FROM u428892925_sid_motos.view_todos_dados
	where dia_data between "2022-01-22 00:00" and "2022-02-15 23:59"
	group by dia_data, entregador_id, entregador
) x 
where praca = "Salvador"
/* 
AND dia_data = "2022-01-22" 
dia_data = "2020-10-11" 
AND qtd_rejeitadas = 0 
AND (qtd_aceitas - qtd_completadas) = 0*/
AND qtd_turno >= 2
group by entregador
having count(distinct dia_data) >= 19
order by qtd_dia desc, qtd_rejeitadas, qtd_canceladas, tempo_online desc, qtd_turno desc;

/*
SELECT *
FROM u428892925_sid_motos.view_todos_dados
where dia_data between "2022-01-22 00:00" and "2022-02-15 23:59"
AND entregador_id = 197073
group by dia_data, entregador_id, entregador;

*/
SELECT dia_data, 
	entregador, praca,
	/*sum(qtd_ofertadas) qtd_ofertadas, 
	sum(qtd_aceitas) qtd_aceitas,  
	sum(qtd_completadas) qtd_completadas, */
	count(distinct turno) qtd_turno,
	ROUND(avg(tempo_online), 6) tempo_online, 
	(sum(qtd_aceitas) - sum(qtd_completadas)) qtd_canceladas,
	sum(qtd_rejeitadas) qtd_rejeitadas
FROM u428892925_sid_motos.view_todos_dados
where dia_data between "2022-01-22 00:00" and "2022-02-15 23:59"
/*AND entregador_id = 197073*/
AND entregador = "Jairo De Jesus Ferreira"
group by dia_data, entregador_id, entregador