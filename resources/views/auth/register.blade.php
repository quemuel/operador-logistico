@extends('layouts.app-login')

@section('content')

{{-- begin::Aside --}}
<div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside" style="background-image: url({{asset('assets/media/bg/bg-4.jpg')}});">
    <div class="kt-grid__item">
        <a href="#" class="kt-login__logo">
            <img src="{{asset('assets/media/logos/logo-12.png')}}">
        </a>
    </div>
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
        <div class="kt-grid__item kt-grid__item--middle">
            <h3 class="kt-login__title">Seja bem vindo!</h3>
            <h4 class="kt-login__subtitle">Sistema de gerenciamento de entregadores - Ifood</h4>
        </div>
    </div>
    <div class="kt-grid__item">
        <div class="kt-login__info">
            <div class="kt-login__copyright">
                Operador Lógico
            </div>
            <div class="kt-login__menu">
                <a href="#" class="kt-link">Contato</a>
            </div>
        </div>
    </div>
</div>

{{-- begin::Aside --}}

{{-- begin::Content --}}
<div class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">

    {{-- begin::Head --}}
    <div class="kt-login__head">
        <a href="{{route('index')}}" class="kt-link kt-login__signup-link">Página Principal</a>
    </div>

    {{-- end::Head --}}

    {{-- begin::Body --}}
    <div class="kt-login__body">
        <div class="kt-login__signup">
            <div class="kt-login__head">
                <h3 class="kt-login__title">{{ __('Register') }}</h3>
                <div class="kt-login__desc">Entre com os dados para criar conta.</div>
            </div>
            <div class="kt-login__form">
                <form class="kt-form" method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="form-group">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="{{ __('Name') }}">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror  
                    </div>
                    <div class="form-group">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="{{ __('E-Mail Address') }}">

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                  
                    </div>
                    <div class="form-group">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="{{ __('Password') }}">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="{{ __('Confirm Password') }}">
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Perfil Administrador?</label>
                        <div class="row">
                            <span class="col kt-switch kt-switch--brand">
                                <label>
                                    <input type="checkbox" name="is_admin">
                                    <span></span>
                                </label>
                            </span>
                        </div>
                    </div>
                    <div class="kt-login__actions">
                        <button id="kt_login_signup_submit" class="btn btn-brand btn-pill btn-elevate">{{ __('Register') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- end::Body --}}
</div>
{{-- end::Content --}}
@endsection