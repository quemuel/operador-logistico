@extends('layouts.app-login')

@section('content')

{{-- begin::Aside --}}
<div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside" style="background-image: url({{asset('assets/media/bg/bg-4.jpg')}});">
    <div class="kt-grid__item">
        <a href="#" class="kt-login__logo">
            <img src="{{asset('assets/media/logos/logo-12.png')}}">
        </a>
    </div>
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
        <div class="kt-grid__item kt-grid__item--middle">
            <h3 class="kt-login__title">Seja bem vindo!</h3>
            <h4 class="kt-login__subtitle">Sistema de gerenciamento de entregadores - Ifood</h4>
        </div>
    </div>
    <div class="kt-grid__item">
        <div class="kt-login__info">
            <div class="kt-login__copyright">
                Operador Lógico
            </div>
            <div class="kt-login__menu">
                <a href="#" class="kt-link">Contato</a>
            </div>
        </div>
    </div>
</div>

{{-- begin::Aside --}}

{{-- begin::Content --}}
<div class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">

    {{-- begin::Head --}}
    <div class="kt-login__head">
        <a href="{{route('index')}}" class="kt-link kt-login__signup-link">Página Principal</a>
    </div>

    {{-- end::Head --}}

    {{-- begin::Body --}}
    <div class="kt-login__body">

        {{-- begin::Signin --}}
        <div class="kt-login__forgot">
            <div class="kt-login__head">
                <h3 class="kt-login__title">{{ __('Reset Password') }}</h3>
                <div class="kt-login__desc">Entre com seu e-mail para reiniciar sua senha.</div>
            </div>
            <div class="kt-login__form">
                <form class="kt-form" method="POST" action="{{ route('password.email') }}">
                    @csrf

                    @if ($message = Session::get('status'))
                        <div class="alert alert-success d-flex align-items-center py-4" role="alert">
                            <span class="fa fa-check pr-3"></span><strong>{{ $message }}</strong>
                            <span class="c-pointer ml-auto" data-dismiss="alert">
                            <span class="fas fa-times lead-1" aria-hidden="true"></span>
                            </span>
                        </div>
                    @endif
                    <div class="form-group">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="{{ __('E-Mail Address') }}">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="kt-login__actions">
                        <button id="kt_login_forgot_submit" class="btn btn-brand btn-pill btn-elevate">{{ __('Send Password Reset Link') }}</button>
                    </div>
                </form>
            </div>
        </div>

        {{-- end::Signin --}}
    </div>

    {{-- end::Body --}}
</div>
{{-- end::Content --}}
@endsection