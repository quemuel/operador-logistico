@extends('layouts.app-login')

@section('content')

{{-- begin::Aside --}}
<div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside" style="background-image: url({{asset('assets/media/bg/bg-4.jpg')}});">
    <div class="kt-grid__item">
        <a href="#" class="kt-login__logo">
            <img src="{{asset('assets/media/logos/logo-12.png')}}">
        </a>
    </div>
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
        <div class="kt-grid__item kt-grid__item--middle">
            <h3 class="kt-login__title">Seja bem vindo!</h3>
            <h4 class="kt-login__subtitle">Sistema de gerenciamento de entregadores - Ifood</h4>
        </div>
    </div>
    <div class="kt-grid__item">
        <div class="kt-login__info">
            <div class="kt-login__copyright">
                Operador Lógico
            </div>
            <div class="kt-login__menu">
                <a href="#" class="kt-link">Contato</a>
            </div>
        </div>
    </div>
</div>

{{-- begin::Aside --}}

{{-- begin::Content --}}
<div class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">

    {{-- begin::Head --}}
    <div class="kt-login__head">
        <a href="{{route('index')}}" class="kt-link kt-login__signup-link">Página Principal</a>
    </div>

    {{-- end::Head --}}

    {{-- begin::Body --}}
    <div class="kt-login__body">

        {{-- begin::Signin --}}
        <div class="kt-login__form">
            <div class="kt-login__title">
                <h3>{{ __('Login') }}</h3>
            </div>

            {{-- begin::Form --}}
            <form method="POST" class="kt-form" action="{{ route('login') }}" novalidate="novalidate" id="kt_login_form">
                @csrf

                @if ($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>    
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                <div class="form-group">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="{{ __('E-Mail Address') }}" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="{{ __('Password') }}">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>                
                <div class="form-group pt-4">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                </div>

                {{-- begin::Action --}}
                <div class="kt-login__actions">

                    @if (Route::has('password.request'))
                        <a class="kt-link kt-login__link-forgot" href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                    @endif
                    <button type="submit" class="btn btn-primary btn-elevate kt-login__btn-primary">{{ __('Login') }}</button>
                </div>

                {{-- end::Action --}}
            </form>
            {{-- end::Form --}}

            {{-- end::Options --}}
        </div>

        {{-- end::Signin --}}
    </div>

    {{-- end::Body --}}
</div>
{{-- end::Content --}}
@endsection