@extends('layouts.app')

@section('template_title')
    Painel principal
@endsection

@section('template_linked_css')
<link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Painel principal 
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <a class="kt-subheader__breadcrumbs-link">
                    @if(date("Y-m-d", strtotime($dadosDashboard->dadosGerais->data_final)) == date("Y-m-d", strtotime("-1 day")))         
                        <span class="badge badge-success"><i class="la la-check-square"></i> Atualizado</span>
                    @else
                        <span class="badge badge-danger"><i class="la la-close"></i> Desatualizado</span>       
                    @endif                    
                </a>
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet" data-ktportlet="true">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <path d="M5,4 L19,4 C19.2761424,4 19.5,4.22385763 19.5,4.5 C19.5,4.60818511 19.4649111,4.71345191 19.4,4.8 L14,12 L14,20.190983 C14,20.4671254 13.7761424,20.690983 13.5,20.690983 C13.4223775,20.690983 13.3458209,20.6729105 13.2763932,20.6381966 L10,19 L10,12 L4.6,4.8 C4.43431458,4.5790861 4.4790861,4.26568542 4.7,4.1 C4.78654809,4.03508894 4.89181489,4 5,4 Z" fill="#000000"/>
                        </g>
                    </svg>
                </span>
                <h3 class="kt-portlet__head-title pr-4">
                    Filtro
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label for="switch-tipo-filtro">Tipo de filtro</label><br>
                        <input id="switch-tipo-filtro" data-switch="true" type="checkbox" checked="checked" data-on-text="Por Semana" data-handle-width="90" data-off-text="Por Data" data-on-color="brand">
                    </div>
                </div>
            </div>
            <div id="div-select" style="{{ request()->get('tipoFiltroSemanal') == "1" || request()->get('tipoFiltroSemanal') == null ? "" : "display: none" }}" class="row">
                <div class="col-xl-4 col-lg-4 col-sm-6">
                    <div class="form-group">
                        <div class="input-group">
                            <!-- height: 38px; -->
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">Semana</label>
                            </div>
                            <select id="inputGroupSelect01" class="form-control kt-selectpicker kt-input" title="Selecione..." multiple  data-actions-box="true" data-size="5" data-selected-text-format="count>4">
                                @foreach($dadosDashboard->semanasAnos as $key => $semanaAno)
                                    <option value="{{$semanaAno->semana_ano}}" {{ in_array($semanaAno->semana_ano, $dadosDashboard->filtroDashboard->semanasAnos) ? "selected" : ""}}>{{$semanaAno->semana_ano}}</option>
                                @endforeach
                            </select>
                            <div class="input-group-append">
                                <a class="btn btn-outline-secondary btn-buscar" href="" type="button">Buscar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="div-datapicker" style="{{ request()->get('tipoFiltroSemanal') == "0" ? "" : "display: none" }}" class="row">
                <div class="col-xl-4 col-lg-4 col-sm-6">
                    <div class="form-group">
                        <div class='input-group' >
                            <?php 
                            $data = "";
                            if (request()->get('dataInicial') && request()->get('dataFinal')) {
                                $data = request()->get('dataInicial') . " - " . request()->get('dataFinal');
                            } ?>
                            <input id='kt_daterangepicker_dashboard' type='text' class="form-control" value="{{ $data }}" readonly placeholder="Selecione as datas" />
                            <div class="input-group-append">
                                <a class="btn btn-outline-secondary btn-buscar" href="" type="button">Buscar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="kt-container ">
    <div class="row">
        <div class="col-lg-3">
            <a class="kt-portlet kt-iconbox kt-iconbox--animate-fast">
                <div class="kt-portlet__body">
                    <div class="kt-iconbox__body">
                        <div class="kt-iconbox__icon">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"/>
                                    <path d="M10.9630156,7.5 L11.0475062,7.5 C11.3043819,7.5 11.5194647,7.69464724 11.5450248,7.95024814 L12,12.5 L15.2480695,14.3560397 C15.403857,14.4450611 15.5,14.6107328 15.5,14.7901613 L15.5,15 C15.5,15.2109164 15.3290185,15.3818979 15.1181021,15.3818979 C15.0841582,15.3818979 15.0503659,15.3773725 15.0176181,15.3684413 L10.3986612,14.1087258 C10.1672824,14.0456225 10.0132986,13.8271186 10.0316926,13.5879956 L10.4644883,7.96165175 C10.4845267,7.70115317 10.7017474,7.5 10.9630156,7.5 Z" fill="#000000"/>
                                    <path d="M7.38979581,2.8349582 C8.65216735,2.29743306 10.0413491,2 11.5,2 C17.2989899,2 22,6.70101013 22,12.5 C22,18.2989899 17.2989899,23 11.5,23 C5.70101013,23 1,18.2989899 1,12.5 C1,11.5151324 1.13559454,10.5619345 1.38913364,9.65805651 L3.31481075,10.1982117 C3.10672013,10.940064 3,11.7119264 3,12.5 C3,17.1944204 6.80557963,21 11.5,21 C16.1944204,21 20,17.1944204 20,12.5 C20,7.80557963 16.1944204,4 11.5,4 C10.54876,4 9.62236069,4.15592757 8.74872191,4.45446326 L9.93948308,5.87355717 C10.0088058,5.95617272 10.0495583,6.05898805 10.05566,6.16666224 C10.0712834,6.4423623 9.86044965,6.67852665 9.5847496,6.69415008 L4.71777931,6.96995273 C4.66931162,6.97269931 4.62070229,6.96837279 4.57348157,6.95710938 C4.30487471,6.89303938 4.13906482,6.62335149 4.20313482,6.35474463 L5.33163823,1.62361064 C5.35654118,1.51920756 5.41437908,1.4255891 5.49660017,1.35659741 C5.7081375,1.17909652 6.0235153,1.2066885 6.2010162,1.41822583 L7.38979581,2.8349582 Z" fill="#000000" opacity="0.3"/>
                                </g>
                            </svg> 
                        </div>
                        <div class="kt-iconbox__desc">
                            <h3 class="kt-iconbox__title">
                                <?php echo number_format(floatval($dadosDashboard->dadosGerais->tempo_online)*100, 2, ",", ".") ?>%
                            </h3>
                            <div class="kt-iconbox__content">
                                Tempo Online
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3">
            <a class="kt-portlet kt-iconbox kt-iconbox--animate-slow">
                <div class="kt-portlet__body">
                    <div class="kt-iconbox__body">
                        <div class="kt-iconbox__icon">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"/>
                                    <path d="M21.9969433,12.1933592 C21.8948657,15.4175796 19.2490111,18 16,18 L12.0583175,18 L12.0583175,18.9825492 C12.0583175,19.2586916 11.8344599,19.4825492 11.5583175,19.4825492 C11.4509855,19.4825492 11.3465023,19.4480108 11.2603165,19.3840407 L8.40328311,17.263451 C8.18154548,17.0988696 8.13521119,16.7856962 8.29979258,16.5639585 C8.32872576,16.5249774 8.36318164,16.4904176 8.40207551,16.4613672 L11.2591089,14.3274051 C11.48035,14.1621567 11.7936615,14.2075478 11.9589099,14.4287888 C12.0234473,14.5151942 12.0583175,14.6201505 12.0583175,14.7279974 L12.0583175,16 L16,16 C17.6264832,16 19.0262317,15.0292331 19.6514501,13.6354945 C20.5364094,13.3251939 21.3338787,12.8288439 21.9969433,12.1933592 Z" fill="#000000" opacity="0.3"/>
                                    <path d="M12.1000181,6 C12.5632884,3.71775968 14.5810421,2 17,2 C19.7614237,2 22,4.23857625 22,7 C22,9.76142375 19.7614237,12 17,12 C14.5810421,12 12.5632884,10.2822403 12.1000181,8 L8,8 C5.790861,8 4,9.790861 4,12 L4,13 C4,14.6568542 5.34314575,16 7,16 L7,18 C4.23857625,18 2,15.7614237 2,13 L2,12 C2,8.6862915 4.6862915,6 8,6 L12.1000181,6 Z M16.7300002,5.668 L16.7300002,9.5 L17.6900002,9.5 L17.6900002,4.5 L16.8180002,4.5 L15.0500002,5.924 L15.6100002,6.588 L16.7300002,5.668 Z" fill="#000000" fill-rule="nonzero"/>
                                </g>
                            </svg>
                        </div>
                        <div class="kt-iconbox__desc">
                            <h3 class="kt-iconbox__title">
                                <?php echo number_format(floatval($dadosDashboard->dadosGerais->completadas)*100, 2, ",", ".") ?>%
                            </h3>
                            <div class="kt-iconbox__content">
                                Corr. Completas
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3">
            <a class="kt-portlet kt-iconbox kt-iconbox--animate">
                <div class="kt-portlet__body">
                    <div class="kt-iconbox__body">
                        <div class="kt-iconbox__icon">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"/>
                                    <path d="M12,22 C6.4771525,22 2,17.5228475 2,12 C2,6.4771525 6.4771525,2 12,2 C17.5228475,2 22,6.4771525 22,12 C22,17.5228475 17.5228475,22 12,22 Z M12,20 C16.418278,20 20,16.418278 20,12 C20,7.581722 16.418278,4 12,4 C7.581722,4 4,7.581722 4,12 C4,16.418278 7.581722,20 12,20 Z M19.0710678,4.92893219 L19.0710678,4.92893219 C19.4615921,5.31945648 19.4615921,5.95262146 19.0710678,6.34314575 L6.34314575,19.0710678 C5.95262146,19.4615921 5.31945648,19.4615921 4.92893219,19.0710678 L4.92893219,19.0710678 C4.5384079,18.6805435 4.5384079,18.0473785 4.92893219,17.6568542 L17.6568542,4.92893219 C18.0473785,4.5384079 18.6805435,4.5384079 19.0710678,4.92893219 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                </g>
                            </svg>
                        </div>
                        <div class="kt-iconbox__desc">
                            <h3 class="kt-iconbox__title">
                                <?php echo number_format(floatval($dadosDashboard->dadosGerais->cancelamento)*100, 2, ",", ".") ?>%
                            </h3>
                            <div class="kt-iconbox__content">
                                Corr. Canceladas
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3">
            <a class="kt-portlet kt-iconbox kt-iconbox--animate-fast">
                <div class="kt-portlet__body">
                    <div class="kt-iconbox__body">
                        <div class="kt-iconbox__icon">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g>
                                    <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10"/>
                                    <path d="M12.0355339,10.6213203 L14.863961,7.79289322 C15.2544853,7.40236893 15.8876503,7.40236893 16.2781746,7.79289322 C16.6686989,8.18341751 16.6686989,8.81658249 16.2781746,9.20710678 L13.4497475,12.0355339 L16.2781746,14.863961 C16.6686989,15.2544853 16.6686989,15.8876503 16.2781746,16.2781746 C15.8876503,16.6686989 15.2544853,16.6686989 14.863961,16.2781746 L12.0355339,13.4497475 L9.20710678,16.2781746 C8.81658249,16.6686989 8.18341751,16.6686989 7.79289322,16.2781746 C7.40236893,15.8876503 7.40236893,15.2544853 7.79289322,14.863961 L10.6213203,12.0355339 L7.79289322,9.20710678 C7.40236893,8.81658249 7.40236893,8.18341751 7.79289322,7.79289322 C8.18341751,7.40236893 8.81658249,7.40236893 9.20710678,7.79289322 L12.0355339,10.6213203 Z" fill="#000000"/>
                                </g>
                            </svg>
                        </div>
                        <div class="kt-iconbox__desc">
                            <h3 class="kt-iconbox__title">
                                <?php echo number_format(floatval($dadosDashboard->dadosGerais->rejeite)*100, 2, ",", ".") ?>%
                            </h3>
                            <div class="kt-iconbox__content">
                                Rejeites
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--tabs">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-12 text-center">
                    <span class="badge badge-light kt-font-bold" style="font-size: 12px;">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M10.9630156,7.5 L11.0475062,7.5 C11.3043819,7.5 11.5194647,7.69464724 11.5450248,7.95024814 L12,12.5 L15.2480695,14.3560397 C15.403857,14.4450611 15.5,14.6107328 15.5,14.7901613 L15.5,15 C15.5,15.2109164 15.3290185,15.3818979 15.1181021,15.3818979 C15.0841582,15.3818979 15.0503659,15.3773725 15.0176181,15.3684413 L10.3986612,14.1087258 C10.1672824,14.0456225 10.0132986,13.8271186 10.0316926,13.5879956 L10.4644883,7.96165175 C10.4845267,7.70115317 10.7017474,7.5 10.9630156,7.5 Z" fill="#000000"/>
                                <path d="M7.38979581,2.8349582 C8.65216735,2.29743306 10.0413491,2 11.5,2 C17.2989899,2 22,6.70101013 22,12.5 C22,18.2989899 17.2989899,23 11.5,23 C5.70101013,23 1,18.2989899 1,12.5 C1,11.5151324 1.13559454,10.5619345 1.38913364,9.65805651 L3.31481075,10.1982117 C3.10672013,10.940064 3,11.7119264 3,12.5 C3,17.1944204 6.80557963,21 11.5,21 C16.1944204,21 20,17.1944204 20,12.5 C20,7.80557963 16.1944204,4 11.5,4 C10.54876,4 9.62236069,4.15592757 8.74872191,4.45446326 L9.93948308,5.87355717 C10.0088058,5.95617272 10.0495583,6.05898805 10.05566,6.16666224 C10.0712834,6.4423623 9.86044965,6.67852665 9.5847496,6.69415008 L4.71777931,6.96995273 C4.66931162,6.97269931 4.62070229,6.96837279 4.57348157,6.95710938 C4.30487471,6.89303938 4.13906482,6.62335149 4.20313482,6.35474463 L5.33163823,1.62361064 C5.35654118,1.51920756 5.41437908,1.4255891 5.49660017,1.35659741 C5.7081375,1.17909652 6.0235153,1.2066885 6.2010162,1.41822583 L7.38979581,2.8349582 Z" fill="#000000" opacity="0.3"/>
                            </g>
                        </svg>
                        Inicial <?php echo date("d/m/Y", strtotime($dadosDashboard->dadosGerais->data_incial)) ?>
                    </span>
                    <span class="badge badge-light kt-font-bold" style="font-size: 12px;">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M10.9630156,7.5 L11.0475062,7.5 C11.3043819,7.5 11.5194647,7.69464724 11.5450248,7.95024814 L12,12.5 L15.2480695,14.3560397 C15.403857,14.4450611 15.5,14.6107328 15.5,14.7901613 L15.5,15 C15.5,15.2109164 15.3290185,15.3818979 15.1181021,15.3818979 C15.0841582,15.3818979 15.0503659,15.3773725 15.0176181,15.3684413 L10.3986612,14.1087258 C10.1672824,14.0456225 10.0132986,13.8271186 10.0316926,13.5879956 L10.4644883,7.96165175 C10.4845267,7.70115317 10.7017474,7.5 10.9630156,7.5 Z" fill="#000000"/>
                                <path d="M7.38979581,2.8349582 C8.65216735,2.29743306 10.0413491,2 11.5,2 C17.2989899,2 22,6.70101013 22,12.5 C22,18.2989899 17.2989899,23 11.5,23 C5.70101013,23 1,18.2989899 1,12.5 C1,11.5151324 1.13559454,10.5619345 1.38913364,9.65805651 L3.31481075,10.1982117 C3.10672013,10.940064 3,11.7119264 3,12.5 C3,17.1944204 6.80557963,21 11.5,21 C16.1944204,21 20,17.1944204 20,12.5 C20,7.80557963 16.1944204,4 11.5,4 C10.54876,4 9.62236069,4.15592757 8.74872191,4.45446326 L9.93948308,5.87355717 C10.0088058,5.95617272 10.0495583,6.05898805 10.05566,6.16666224 C10.0712834,6.4423623 9.86044965,6.67852665 9.5847496,6.69415008 L4.71777931,6.96995273 C4.66931162,6.97269931 4.62070229,6.96837279 4.57348157,6.95710938 C4.30487471,6.89303938 4.13906482,6.62335149 4.20313482,6.35474463 L5.33163823,1.62361064 C5.35654118,1.51920756 5.41437908,1.4255891 5.49660017,1.35659741 C5.7081375,1.17909652 6.0235153,1.2066885 6.2010162,1.41822583 L7.38979581,2.8349582 Z" fill="#000000" opacity="0.3"/>
                            </g>
                        </svg>
                        Final <?php echo date("d/m/Y", strtotime($dadosDashboard->dadosGerais->data_final)) ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-xl-6">
            <div class="kt-portlet kt-portlet--tabs">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-toolbar">
                        <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#kt_portlet_base_demo_3_3_tab_content" role="tab">
                                    <i class="flaticon2-heart-rate-monitor" aria-hidden="true"></i> Salvador
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_3_2_tab_content" role="tab">
                                    <i class="flaticon2-pie-chart-2" aria-hidden="true"></i> Aracaju
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_portlet_base_demo_3_3_tab_content" role="tabpanel">
                            <div class="kt-portlet">
                                <div class="kt-portlet__body  kt-portlet__body--fit">
                                    <div class="row row-no-padding row-col-separator-xl">
                                        <div class="col-12">
                                            @include('dashboard.dados-gerais', ['praca' => $dadosDashboard->salvador])
                                        </div>
                                        <div class="col-12">
                                            @include('dashboard.dados-veiculo', ['praca' => $dadosDashboard->salvador])                                            
                                        </div>
                                        <div class="col-12">
                                            @include('dashboard.dados-turno', ['praca' => $dadosDashboard->salvador])
                                        </div>
                                        <div class="col-12">
                                            @php ($dadosDashboard->filtroDashboard->nomePraca = $dadosDashboard->salvador->nomePraca)
                                            @include('dashboard.entregadores', ['filtroDashboard' => $dadosDashboard->filtroDashboard])
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="kt_portlet_base_demo_3_2_tab_content" role="tabpanel">
                            <div class="kt-portlet">
                                <div class="kt-portlet__body  kt-portlet__body--fit">
                                    <div class="row row-no-padding row-col-separator-xl">
                                        <div class="col-12">
                                            @include('dashboard.dados-gerais', ['praca' => $dadosDashboard->aracaju])
                                        </div>
                                        <div class="col-12">
                                            @include('dashboard.dados-veiculo', ['praca' => $dadosDashboard->aracaju])                                            
                                        </div>
                                        <div class="col-12">
                                            @include('dashboard.dados-turno', ['praca' => $dadosDashboard->aracaju])
                                        </div>
                                        <div class="col-12">
                                            @php ($dadosDashboard->filtroDashboard->nomePraca = $dadosDashboard->aracaju->nomePraca)
                                            @include('dashboard.entregadores', ['filtroDashboard' => $dadosDashboard->filtroDashboard])
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- end:: Content -->
@endsection

@section('footer_scripts')
<script src="//www.amcharts.com/lib/3/amcharts.js" type="text/javascript"></script>
<script src="//www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
<script src="//www.amcharts.com/lib/3/radar.js" type="text/javascript"></script>
<script src="//www.amcharts.com/lib/3/pie.js" type="text/javascript"></script>
<script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js" type="text/javascript"></script>
<script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js" type="text/javascript"></script>
<script src="//www.amcharts.com/lib/3/plugins/export/export.min.js" type="text/javascript"></script>
<script src="//www.amcharts.com/lib/3/themes/light.js" type="text/javascript"></script>
<script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
@endsection
@section('template_fastload_js') 
    $(document).ready(function () {
        $("#inputGroupSelect01").on('change', function(){
            montaUrlSemanas();
        });        
        var a = $('.kt-selectpicker').selectpicker({
            deselectAllText: 'Nenhum',
            selectAllText: 'Todos',
            countSelectedText: '{0} Itens selecionados'
        });
        $('#kt_daterangepicker_dashboard').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            "locale": {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "Aplicar",
                "cancelLabel": "Cancelar",
                "daysOfWeek": [
                  "Dom",
                  "Seg",
                  "Ter",
                  "Qua",
                  "Qui",
                  "Sex",
                  "Sab"
                ],
                "monthNames": [
                  "Janeiro",	
                  "Fevereiro",
                  "Março",
                  "Abril",
                  "Maio",
                  "Junho",
                  "Julho",
                  "Agosto",
                  "Setembro",
                  "Outubro",
                  "Novembro",
                  "Dezembro"
                ],
                "firstDay": 1
              }
        }, function(start, end, label) {
            $('#kt_daterangepicker_dashboard .form-control').val( start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
            montaUrlDatas(start.format('DD/MM/YYYY'), end.format('DD/MM/YYYY'));
        });
        
        <?php 
        $dataInicial = "";
        if (request()->get('dataInicial') && request()->get('dataFinal')) {
            $dataInicial = request()->get('dataInicial');
            $dataFinal = request()->get('dataFinal');
        } ?>
        var startDate = {!! $dataInicial !== "" ? "moment('$dataInicial', 'DD/MM/YYYY')" : "moment().subtract('days', 1)" !!};
        var endDate = {!! $dataInicial !== "" ? "moment('$dataFinal', 'DD/MM/YYYY')" : "moment().subtract('days', 1)" !!};
        $("#kt_daterangepicker_dashboard").data('daterangepicker').setStartDate(startDate);
        $("#kt_daterangepicker_dashboard").data('daterangepicker').setEndDate(endDate);
        var dateRangePicker = $('#kt_daterangepicker_dashboard').data('daterangepicker');
        $('#kt_daterangepicker_dashboard .form-control').val( dateRangePicker.startDate.format('DD/MM/YYYY') + ' - ' + dateRangePicker.endDate.format('DD/MM/YYYY'));
        $('#switch-tipo-filtro').bootstrapSwitch('state', {{ request()->get('tipoFiltroSemanal') == "1" || request()->get('tipoFiltroSemanal') == null ? "true" : "false" }});
        $('#switch-tipo-filtro').on('switchChange.bootstrapSwitch', function (event, state) {
            if(state) {
                montaUrlSemanas();
                $('#div-select').show();
                $('#div-datapicker').hide();
            } else {
                var dateRangePicker = $('#kt_daterangepicker_dashboard').data('daterangepicker');
                montaUrlDatas(dateRangePicker.startDate.format('DD/MM/YYYY'), dateRangePicker.endDate.format('DD/MM/YYYY'));
                $('#div-select').hide();
                $('#div-datapicker').show();
            }
        });
    });
    function montaUrlSemanas() {
        var selected = []; //array to store value
        $("#inputGroupSelect01").find("option:selected").each(function(key,value){
            selected.push(value.innerHTML); //push the text to array
        });
        var params = decodeURIComponent($.param( { "tipoFiltroSemanal": "1", "semanasAnos": selected} ));
        $('.btn-buscar').attr({ href: '{{route('dashboard')}}?' + params });
    }
    function montaUrlDatas(dataInicial, dataFinal) {
        var params = decodeURIComponent($.param( { "tipoFiltroSemanal": "0", "dataInicial": dataInicial, "dataFinal": dataFinal} ));
        $('.btn-buscar').attr({ href: '{{route('dashboard')}}?' + params });
    }
@endsection