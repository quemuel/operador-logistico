
<div class="kt-portlet">
    <div class="kt-portlet__head" style="border-bottom:0">
        <div class="kt-portlet__head-label pt-4">
            <h3 class="kt-portlet__head-title">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"/>
                        <path d="M8,13.1668961 L20.4470385,11.9999863 L8,10.8330764 L8,5.77181995 C8,5.70108058 8.01501031,5.63114635 8.04403925,5.56663761 C8.15735832,5.31481744 8.45336217,5.20254012 8.70518234,5.31585919 L22.545552,11.5440255 C22.6569791,11.5941677 22.7461882,11.6833768 22.7963304,11.794804 C22.9096495,12.0466241 22.7973722,12.342628 22.545552,12.455947 L8.70518234,18.6841134 C8.64067359,18.7131423 8.57073936,18.7281526 8.5,18.7281526 C8.22385763,18.7281526 8,18.504295 8,18.2281526 L8,13.1668961 Z" fill="#000000"/>
                        <path d="M4,16 L5,16 C5.55228475,16 6,16.4477153 6,17 C6,17.5522847 5.55228475,18 5,18 L4,18 C3.44771525,18 3,17.5522847 3,17 C3,16.4477153 3.44771525,16 4,16 Z M1,11 L5,11 C5.55228475,11 6,11.4477153 6,12 C6,12.5522847 5.55228475,13 5,13 L1,13 C0.44771525,13 6.76353751e-17,12.5522847 0,12 C-6.76353751e-17,11.4477153 0.44771525,11 1,11 Z M4,6 L5,6 C5.55228475,6 6,6.44771525 6,7 C6,7.55228475 5.55228475,8 5,8 L4,8 C3.44771525,8 3,7.55228475 3,7 C3,6.44771525 3.44771525,6 4,6 Z" fill="#000000" opacity="0.3"/>
                    </g>
                </svg>
                Entregadores <small>Tempo Online | Rejeites | Cancelamentos</small>
            </h3>
        </div>
        <!--<div class="kt-portlet__head-toolbar" >
            <div class="input-group pt-4">
                 height: 38px;
                <div class="input-group-prepend" style="height: 38px;">
                    <label class="input-group-text" for="inputGroupSelect01"><i class="fa fa-filter"></i> Filtro</label>
                </div>
                <select id="inputGroupSelect01" class="form-control kt-selectpicker" title="Selecione..." multiple data-size="5"  data-actions-box="true" data-size="5" data-selected-text-format="count>3">
                    <option value="Carine Souza" >Carine Souza</option>
                    <option value="Vitor Carlos" >Vitor Carlos</option>
                    <option value="João 1" selected>João 1</option>
                    <option value="João 2" selected>João 2</option>
                    <option value="João 3" selected>João 3</option>
                    <option value="João 4" selected>João 4</option>
                    <option value="João 5" selected>João 5</option>
                    <option value="João 6" selected>João 6</option>
                </select>
            </div>
        </div> -->
    </div>
    <div class="kt-portlet__body pt-3">
        <div class="kt-section">
            <div class="kt-section__content">
                <div class="row">
                    <div class="col-md-7">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-checkable data-table-rejeite-{{$filtroDashboard->nomePraca}}">
                                <thead>
                                    <tr>
                                        <th data-priority="1">Entregador</th>
                                        <th>Qtd Ofertadas</th>
                                        <th data-priority="3">Qtd Rejeitadas</th>
                                        <th data-priority="2">Rejeites</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-checkable data-table-tempo-online-{{$filtroDashboard->nomePraca}}">
                                <thead>
                                    <tr>
                                        <th>Entregador</th>
                                        <th>Tempo Online</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row pt-4">
                    <div class="col">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-checkable data-table-cancelamento-{{$filtroDashboard->nomePraca}}">
                                <thead>
                                    <tr>
                                        <th data-priority="1">Entregador</th>
                                        <th>Qtd Ofert</th>
                                        <th>Qtd Aceite</th>
                                        <th>Qtd Complet</th>
                                        <th>Qtd Cancel</th>
                                        <th data-priority="3">Complet</th>
                                        <th data-priority="2">Cancel</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('template_fastload_js') 
    $(document).ready(function () {   
        $('.data-table-rejeite-{{$filtroDashboard->nomePraca}}').DataTable({
			responsive: true,
			processing: true,
            serverSide: true,
            "lengthMenu": [[5, 15, 35, 65, 100, -1], [5, 15, 35, 65, 100, "Todos"]],
            "order": [[ 3, "desc" ]],
            "pagingType": "full",
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.25/i18n/Portuguese-Brasil.json"
            },
            ajax: "<?php echo route('dashboard.dados', [
                'tipoTabela' => 'EntregadoresRejeite', 
                'tipoFiltroSemanal' => $filtroDashboard->tipoFiltroSemanal,
                'nomePraca' => $filtroDashboard->nomePraca, 
                'semanasAnos' => $filtroDashboard->semanasAnos, 
                'dataInicial' => $filtroDashboard->dataInicial, 
                'dataFinal' => $filtroDashboard->dataFinal
            ])?>",
            "columnDefs": [
                { responsivePriority: 1, targets: 0 },
                { responsivePriority: 2, targets: -1 },
                { responsivePriority: 3, targets: -2 },
                { "targets": [1,2,3], "searchable": false },
                { "targets": [1,2], "render": $.fn.dataTable.render.number( '.', ',', 0, '','')},
            ],
            columns: [
                {data: 'entregador', name: 'entregador'},
                {data: 'qtd_ofertadas', name: 'qtd_ofertadas'},
                {data: 'qtd_rejeitadas', name: 'qtd_rejeitadas'},
                {data: 'rejeite', name: 'rejeite', render: function(data, type, row) {
                    return (row.rejeite * 100).toFixed(2).toString().replace('.', ',') + "%"; 
                }},
            ]
        });    
        $('.data-table-tempo-online-{{$filtroDashboard->nomePraca}}').DataTable({
			responsive: true,
			processing: true,
            serverSide: true,
            "lengthMenu": [[5, 15, 35, 65, 100, -1], [5, 15, 35, 65, 100, "Todos"]],
            "order": [[ 1, "asc" ]],
            "pagingType": "full",
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.25/i18n/Portuguese-Brasil.json"
            },
            ajax: "<?php echo route('dashboard.dados', [
                'tipoTabela' => 'EntregadoresTempoOnline', 
                'tipoFiltroSemanal' => $filtroDashboard->tipoFiltroSemanal,
                'nomePraca' => $filtroDashboard->nomePraca, 
                'semanasAnos' => $filtroDashboard->semanasAnos, 
                'dataInicial' => $filtroDashboard->dataInicial, 
                'dataFinal' => $filtroDashboard->dataFinal
            ])?>",
            "columnDefs": [
                { "targets": [1], "searchable": false }
            ],
            columns: [
                {data: 'entregador', name: 'entregador'},
                {data: 'tempo_online', name: 'tempo_online', render: function(data, type, row) {
                    return (data * 100).toFixed(2).toString().replace('.', ',') + "%"; 
                }},
            ]
        });   
        $('.data-table-cancelamento-{{$filtroDashboard->nomePraca}}').DataTable({
			responsive: true,
			processing: true,
            serverSide: true,
            "lengthMenu": [[5, 15, 35, 65, 100, -1], [5, 15, 35, 65, 100, "Todos"]],
            "order": [[ 6, "desc" ]],
            "pagingType": "full",
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.25/i18n/Portuguese-Brasil.json"
            },
            ajax: "<?php echo route('dashboard.dados', [
                'tipoTabela' => 'EntregadoresCancelamento',
                'tipoFiltroSemanal' => $filtroDashboard->tipoFiltroSemanal,
                'nomePraca' => $filtroDashboard->nomePraca, 
                'semanasAnos' => $filtroDashboard->semanasAnos, 
                'dataInicial' => $filtroDashboard->dataInicial, 
                'dataFinal' => $filtroDashboard->dataFinal
            ])?>",
            "columnDefs": [
                { responsivePriority: 1, targets: 0 },
                { responsivePriority: 2, targets: -1 },
                { responsivePriority: 3, targets: -2 },
                { "targets": [1,2,3,4,5,6], "searchable": false },
                { "targets": [1,2,3,4], "render": $.fn.dataTable.render.number( '.', ',', 0, '','')},
            ],
            columns: [
                {data: 'entregador', name: 'entregador'},
                {data: 'qtd_ofertadas', name: 'qtd_ofertadas'},
                {data: 'qtd_aceitas', name: 'qtd_aceitas'},
                {data: 'qtd_completadas', name: 'qtd_completadas'},
                {data: 'qtd_canceladas', name: 'qtd_canceladas'},
                {data: 'completadas', name: 'completadas', render: function(data, type, row) {
                    return (data * 100).toFixed(2).toString().replace('.', ',') + "%"; 
                }},
                {data: 'cancelamento', name: 'cancelamento', render: function(data, type, row) {
                    return (data * 100).toFixed(2).toString().replace('.', ',') + "%"; 
                }},
            ]
        });
    });
    //render: $.fn.dataTable.render.number( '', ',', 2, %)
    @parent
@endsection