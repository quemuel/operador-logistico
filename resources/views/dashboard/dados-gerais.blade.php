<div class="kt-portlet kt-portlet--height-fluid  kt-portlet--last">
    <div class="kt-portlet__head" style="border-bottom:0">
        <div class="kt-portlet__head-label pt-4">
            <h3 class="kt-portlet__head-title">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"/>
                        <path d="M2.56066017,10.6819805 L4.68198052,8.56066017 C5.26776695,7.97487373 6.21751442,7.97487373 6.80330086,8.56066017 L8.9246212,10.6819805 C9.51040764,11.267767 9.51040764,12.2175144 8.9246212,12.8033009 L6.80330086,14.9246212 C6.21751442,15.5104076 5.26776695,15.5104076 4.68198052,14.9246212 L2.56066017,12.8033009 C1.97487373,12.2175144 1.97487373,11.267767 2.56066017,10.6819805 Z M14.5606602,10.6819805 L16.6819805,8.56066017 C17.267767,7.97487373 18.2175144,7.97487373 18.8033009,8.56066017 L20.9246212,10.6819805 C21.5104076,11.267767 21.5104076,12.2175144 20.9246212,12.8033009 L18.8033009,14.9246212 C18.2175144,15.5104076 17.267767,15.5104076 16.6819805,14.9246212 L14.5606602,12.8033009 C13.9748737,12.2175144 13.9748737,11.267767 14.5606602,10.6819805 Z" fill="#000000" opacity="0.3"/>
                        <path d="M8.56066017,16.6819805 L10.6819805,14.5606602 C11.267767,13.9748737 12.2175144,13.9748737 12.8033009,14.5606602 L14.9246212,16.6819805 C15.5104076,17.267767 15.5104076,18.2175144 14.9246212,18.8033009 L12.8033009,20.9246212 C12.2175144,21.5104076 11.267767,21.5104076 10.6819805,20.9246212 L8.56066017,18.8033009 C7.97487373,18.2175144 7.97487373,17.267767 8.56066017,16.6819805 Z M8.56066017,4.68198052 L10.6819805,2.56066017 C11.267767,1.97487373 12.2175144,1.97487373 12.8033009,2.56066017 L14.9246212,4.68198052 C15.5104076,5.26776695 15.5104076,6.21751442 14.9246212,6.80330086 L12.8033009,8.9246212 C12.2175144,9.51040764 11.267767,9.51040764 10.6819805,8.9246212 L8.56066017,6.80330086 C7.97487373,6.21751442 7.97487373,5.26776695 8.56066017,4.68198052 Z" fill="#000000"/>
                    </g>
                </svg>
                Dados Gerais
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body pt-0">
        <div class="kt-widget16">
            <div class="kt-widget16__items">
                <div class="kt-widget16__item">
                    <span class="kt-widget16__date kt-font-bold">
                        Tempo Online
                    </span>
                    <span class="kt-widget16__price  kt-font-success">
                        <?php echo number_format(floatval($praca->dadosPraca->tempo_online)*100, 2, ",", ".") ?>%
                    </span>
                </div>
                <div class="kt-widget16__item">
                    <span class="kt-widget16__date">
                        Qtd Ofertada
                    </span>
                    <span class="kt-widget16__price  kt-font-brand">
                        <?php echo number_format(intval($praca->dadosPraca->qtd_ofertadas), 0, ",", ".") ?>
                    </span>
                </div>
                <div class="kt-widget16__item">
                    <span class="kt-widget16__date">
                        Qtd Aceitas
                    </span>
                    <span class="kt-widget16__price  kt-font-brand">
                        <?php echo number_format(intval($praca->dadosPraca->qtd_aceitas), 0, ",", ".") ?>
                    </span>
                </div>
                <div class="kt-widget16__item">
                    <span class="kt-widget16__date">
                        Qtd Completas
                    </span>
                    <span class="kt-widget16__price  kt-font-brand">
                        <?php echo number_format(intval($praca->dadosPraca->qtd_completadas), 0, ",", ".") ?>
                    </span>
                </div>
                <div class="kt-widget16__item">
                    <span class="kt-widget16__date">
                        Qtd Canc
                    </span>
                    <span class="kt-widget16__price kt-font-brand">
                        <?php echo number_format(intval($praca->dadosPraca->qtd_canceladas), 0, ",", ".") ?>
                    </span>
                </div>
                <div class="kt-widget16__item">
                    <span class="kt-widget16__date">
                        Qtd Rejeite
                    </span>
                    <span class="kt-widget16__price kt-font-brand">
                        <?php echo number_format(intval($praca->dadosPraca->qtd_rejeitadas), 0, ",", ".") ?>
                    </span>
                </div>
                <div class="kt-widget16__item">
                    <span class="kt-widget16__date kt-font-bold">
                        Aceite
                    </span>
                    <span class="kt-widget16__price  kt-font-primary">
                        <?php echo number_format(floatval($praca->dadosPraca->aceite)*100, 2, ",", ".") ?>%
                    </span>
                </div>
                <div class="kt-widget16__item">
                    <span class="kt-widget16__date kt-font-bold">
                        Completas
                    </span>
                    <span class="kt-widget16__price  kt-font-success">
                        <?php echo number_format(floatval($praca->dadosPraca->completada)*100, 2, ",", ".") ?>%
                    </span>
                </div>
                <div class="kt-widget16__item">
                    <span class="kt-widget16__date kt-font-bold">
                        Rejeite
                    </span>
                    <span class="kt-widget16__price  kt-font-danger">
                        <?php echo number_format(floatval($praca->dadosPraca->rejeite)*100, 2, ",", ".") ?>%
                    </span>
                </div>
                <div class="kt-widget16__item">
                    <span class="kt-widget16__date kt-font-bold">
                        Cancel
                    </span>
                    <span class="kt-widget16__price  kt-font-warning">
                        <?php echo number_format(floatval($praca->dadosPraca->cancelamento)*100, 2, ",", ".") ?>%
                    </span>
                </div>
            </div>
            <div class="kt-widget16__stats">
                <div class="kt-widget16__visual">
                    <div id="kt_chart_{{$praca->nomePraca}}" style="height: 160px; width: 160px;"></div>
                </div>
                <div class="kt-widget16__legends">
                    @foreach($praca->tempoOnlineModalidade as $key => $modalidade)
                        <div class="kt-widget16__legend">
                            <span class="kt-widget16__bullet <?php echo ($key%2!=0) ? "kt-bg-success" : "kt-bg-brand" ?>"></span>
                            <span class="kt-widget16__stat">
                                {{ number_format(floatval($modalidade->tempo_online)*100, 2, ",", ".") }}% {{ $modalidade->modalidade }}
                            </span>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@section('template_fastload_js') 
    $(document).ready(function () {
        Morris.Donut({
            element: 'kt_chart_{{$praca->nomePraca}}',
            data: <?php echo $praca->tempoOnlineModalidadeJson ?>,
            labelColor: '#a7a7c2',
            resize: true,
            colors: [
                "#2c77f4",
                "#1dc9b7"
            ]
        });
    });
    @parent
@endsection