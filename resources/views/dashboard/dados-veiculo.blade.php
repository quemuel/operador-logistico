<div class="kt-portlet kt-portlet--height-fluid">
    <div class="kt-widget14">
        <div class="kt-widget14__header">
            <h3 class="kt-widget14__title">
                <i class="fa fa-car kt-font-brand"></i>
                Veículo
            </h3>
        </div>
        <div class="kt-widget14__content">
            <div class="table-responsive">
                <table class="table"><div class="kt-widget16__item">
                    <thead class="thead-light">
                        <tr>
                            <th>Veículo</th>
                            <th>Tempo Online</th>
                            <th>Qtd Oferta</th>
                            <th>Qtd Comp.</th>
                            <th>Qtd Canc</th>
                            <th>Qtd Rejeite</th>
                            <th>Rejeite</th>
                            <th>Cancel</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($praca->dadosVeiculo)         
                            @foreach($praca->dadosVeiculo as $key => $veiculo)
                                <tr>
                                    <th scope="row">{{ $veiculo->veiculo }}</th>
                                    <td class="kt-font-success">{{number_format(floatval($veiculo->tempo_online)*100, 2, ",", ".")}}%</td>
                                    <td>{{number_format(intval($veiculo->qtd_ofertadas), 0, ",", ".")}}</td>
                                    <td>{{number_format(intval($veiculo->qtd_completadas), 0, ",", ".")}}</td>
                                    <td>{{number_format(intval($veiculo->qtd_canceladas), 0, ",", ".")}}</td>
                                    <td>{{number_format(intval($veiculo->qtd_rejeitadas), 0, ",", ".")}}</td>
                                    <td class="kt-font-danger">{{number_format(floatval($veiculo->rejeite)*100, 2, ",", ".")}}%</td>
                                    <td class="kt-font-warning">{{number_format(floatval($veiculo->cancelamento)*100, 2, ",", ".")}}%</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8">Sem registros</td>
                            </tr>    
                        @endif  
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>