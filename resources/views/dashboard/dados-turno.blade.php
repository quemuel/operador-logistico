<div class="kt-portlet kt-portlet--height-fluid  kt-portlet--last">
    <div class="kt-portlet__head" style="border-bottom:0">
        <div class="kt-portlet__head-label pt-4">
            <h3 class="kt-portlet__head-title">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"/>
                        <path d="M12,22 C7.02943725,22 3,17.9705627 3,13 C3,8.02943725 7.02943725,4 12,4 C16.9705627,4 21,8.02943725 21,13 C21,17.9705627 16.9705627,22 12,22 Z" fill="#000000" opacity="0.3"/>
                        <path d="M11.9630156,7.5 L12.0475062,7.5 C12.3043819,7.5 12.5194647,7.69464724 12.5450248,7.95024814 L13,12.5 L16.2480695,14.3560397 C16.403857,14.4450611 16.5,14.6107328 16.5,14.7901613 L16.5,15 C16.5,15.2109164 16.3290185,15.3818979 16.1181021,15.3818979 C16.0841582,15.3818979 16.0503659,15.3773725 16.0176181,15.3684413 L11.3986612,14.1087258 C11.1672824,14.0456225 11.0132986,13.8271186 11.0316926,13.5879956 L11.4644883,7.96165175 C11.4845267,7.70115317 11.7017474,7.5 11.9630156,7.5 Z" fill="#000000"/>
                    </g>
                </svg>
                Turno
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body pt-0">
        <div class="row">
            <div class="col-md-12">
                <div id="kt_amcharts_{{$praca->nomePraca}}" style="height: 500px;"></div>
            </div>
        </div>
    </div>
</div>
@section('template_fastload_js') 
    $(document).ready(function () {
        var chart{{$praca->nomePraca}} = AmCharts.makeChart("kt_amcharts_{{$praca->nomePraca}}", {
            "rtl": KTUtil.isRTL(),
            "type": "serial",
            "theme": "light",
            "dataProvider": <?php echo $praca->tempoOnlineTurnoJson ?>,
            "chartScrollbar": {
                "autoGridCount": true
            },
            "valueAxes": [{
                "gridColor": "#FFFFFF",
                "gridAlpha": 0.2,
                "dashLength": 0,
                "minimum": 0,
                "maximum": 100,
                labelsEnabled: false,
                guides: [{
                        value: 0,
                        label: "0%",
                        tickLength: 5,
                        lineAlpha: .15
                    }, {
                        value: 25,
                        label: "25%",
                        tickLength: 5,
                        lineAlpha: .15
                    }, {
                        value: 50,
                        label: "50%",
                        tickLength: 5,
                        lineAlpha: .15
                    }, {
                        value: 75,
                        label: "75%",
                        tickLength: 5,
                        lineAlpha: .15
                    }, {
                        value: 100,
                        label: "100%",
                        tickLength: 5,
                        lineAlpha: .15
                    },
                ]
            }],
            "numberFormatter": {
                "precision": -1,
                "decimalSeparator": ",",
                "thousandsSeparator": "."
            },
            "gridAboveGraphs": false,
            "startDuration": 1,
            "graphs": [{
                "labelPosition": "inside",
                "labelText": "[[value]]%",
                "balloonText": "[[category]]: <b>[[value]]%</b>",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "colorField": 'color',
                "lineColorField": 'color',
                "fillColorsField": 'color',
                "valueField": "tempo_online"
            }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "turno",
            "categoryAxis": {
                "gridPosition": "start",
                "gridAlpha": 0,
                "tickPosition": "start",
                "tickLength": 20
            },
            "export": {
                "enabled": false
            }
        });
        chart{{$praca->nomePraca}}.categoryAxis.labelRotation = 90;
    });
    @parent
@endsection