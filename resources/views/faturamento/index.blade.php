@extends('layouts.app')

@section('template_title')
    Operador Logístico
@endsection

@section('template_linked_css')
    <link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('template_fastload_css')
    #toast-container > div {
        opacity: 1;
    }
@endsection

@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Faturamento
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <a class="kt-subheader__breadcrumbs-link">
                    Listagem                   
                </a>
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    @if ($message = Session::get('error'))
        <div class="alert alert-danger fade show" role="alert">
            <div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
            <div class="alert-text">{{ $message }}</div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>
    @endif
    @if ($message = Session::get('success'))
        <div class="alert alert-success fade show" role="alert">
            <div class="alert-icon"><i class="flaticon2-checkmark"></i></div>
            <div class="alert-text">{{ $message }}</div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <i class="la la-search pr-2"></i> Buscar por período
                        </h3>
                    </div>
                </div>
                <!--begin::Form-->
                <form class="kt-form" action="{{ route('faturamento.index') }}" method="get">
                    <div class="kt-portlet__body">
                        <div class="row">
                            <div class="col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="data_inicial">Data Inicial</label>
                                    <input id="data_inicial" name="dataInicial" required class="form-control" type="date" value="{{$dataInicial}}">
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="data_final">Data Final</label>
                                    <input id="data_final" name="dataFinal" required class="form-control" type="date" value="{{$dataFinal}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn-buscar" class="btn btn-success">Buscar</button>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <i class="flaticon-notepad pr-2"></i> Faturamento
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body pt-3">
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <table class="table table-bordered table-hover table-checkable data-table-dados">
                                <thead>
                                    <tr>
                                        <th>Município</th>
                                        <th>Nome</th>
                                        <th>CPF</th>
                                        <th>Pix</th>
                                        <th>Banco</th>
                                        <th>Agência</th>
                                        <th>Conta</th>
                                        <th>Valor</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>
<div class="modal fade" id="associar-entregador" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{route("faturamento.associar-entregador")}}" method="POST" id="form-associar-entregador">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Associar Entregador aos Dados Bancários</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="kt-portlet">
                        <div class="kt-portlet__body pt-3">
                            <div class="kt-section">
                                <div class="kt-section__content">
                                    <p><strong>Dados do faturamento</strong></p>
                                    <p><strong>Entregador:</strong> <span id="span-entregador"></span></p>
                                    <p><strong>Praça:</strong> <span id="span-praca"></span></p>
                                    <br>
                                    <p>Selecione o entregador que deseja associar.</p>  
                                    <input type="hidden" id="input-entregador" name="entregador">
                                    <input type="hidden" id="input-praca" name="praca">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-12">
                                                <label class="kt-font-bold">Entregador</label><br>
                                                <select required class="form-control kt-select2" id="kt_select2_1" name="dados_bancario_id"></select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Associar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end:: Content -->
@endsection

@section('footer_scripts')
    <script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
@endsection

@section('template_fastload_js') 
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $(".tooltip").tooltip();
        var data =  "<?php echo $dataInicial ?>";
        var data2 =  "<?php echo $dataFinal ?>";
        var tabela = $('.data-table-dados').DataTable({
			responsive: true,
			processing: true,
            serverSide: true,
            "lengthMenu": [[5, 15, 35, 65, 100, -1], [5, 15, 35, 65, 100, "Todos"]],
            "order": [[ 0, "desc" ], [ 1, "asc" ]],
            "pagingType": "full",
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.25/i18n/Portuguese-Brasil.json"
            },
            ajax: "<?php echo route('faturamento.dados', [
                        "dataInicial" => $dataInicial,
                        "dataFinal" => $dataFinal
                    ])?>",
            "columnDefs": [
                /*{ responsivePriority: 1, targets: 0 },
                { responsivePriority: 2, targets: -1 },
                { responsivePriority: 3, targets: -2 },
                { "targets": [2,3,4,5,6], "searchable": false },*/
            ],
            columns: [
                {data: 'praca', name: 'praca', responsivePriority: 1},
                {data: 'entregador', name: 'entregador', responsivePriority: 2},
                {data: 'cpf', className: 'copy', name: 'cpf'},
                {data: 'pix', className: 'copy', name: 'pix', responsivePriority: 3},
                {data: 'banco', name: 'banco'},
                {data: 'agencia', className: 'copy', name: 'agencia'},
                {data: 'conta', className: 'copy', name: 'conta', render: function(data, type, row) {
                    let conta = data.toString();
                    return conta.toString().substr(0, conta.length-1) + "-" + conta.substr(conta.length-1, 1); 
                }},
                {data: 'valor', className: 'copy', name: 'valor', render: function(data, type, row) {
                    return data.toString().replace('.', ','); 
                }},
                {data: 'action', name: 'action', orderable: false},
            ]
        });  
        $('.data-table-dados tbody').on('click', 'td, span.dtr-data', function () {
            //tabela.row( $(this).parents('tr') ).data();
            var data = null;
            if($(this).parent().prop("tagName") == 'LI') {
                var row = $(this).parent().data('dt-row');
                var column = $(this).parent().data('dt-column');
                data = tabela.cell(row,column).data();
            } else {
                data = tabela.cell(this).data();
            }
            if(data !== undefined){
                criandoElementoCopiar(data);
                    toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-center",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "1500",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
                toastr.success("<b>"+data + "</b> copiado!");
            }
        });
               
        $('#kt_select2_1').select2({
            placeholder: "Selecione o Entregador",
            "language": {
                "noResults": function(){
                    return "Não encontrado";
                },
                "searching":function(){return"Buscando…"},
            },
            ajax: {
                url: '{{route("faturamento.select2-dados-ajax")}}',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                            return {
                                text: item.nome + ' (' + item.municipio + ')',
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
    });
    function criandoElementoCopiar(dado) {
        var dummy = document.createElement("input");
        document.body.appendChild(dummy);
        dummy.setAttribute("id", "dummy_id");
        document.getElementById("dummy_id").value=dado;
        dummy.select();
        document.execCommand("copy");
        document.body.removeChild(dummy);
    }
    function carregarModal(elemento) {
        $('#span-entregador').html($(elemento).data('entregador'));
        $('#span-praca').html($(elemento).data('praca'));
        $('#input-entregador').val($(elemento).data('entregador'));
        $('#input-praca').val($(elemento).data('praca'));
        $('#associar-entregador').modal('show');
    }
@endsection