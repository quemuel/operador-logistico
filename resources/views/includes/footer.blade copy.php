<div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-footer__copyright">
            2021&nbsp;&copy;&nbsp;Operador Logístico
        </div>
        <div class="kt-footer__menu">
            <a href="" class="kt-footer__menu-link kt-link">Contato</a>
        </div>
    </div>
</div>