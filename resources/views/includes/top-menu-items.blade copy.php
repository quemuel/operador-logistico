<!-- begin:: Header -->
<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

    <!-- begin: Header Menu -->
    <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
    <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
        <div class="kt-header-logo">
            <a href="index.html">
                <img alt="Logo" src="{{asset('assets/media/logos/logo-12.png')}}" />
            </a>
        </div>
    </div>

    <!-- end: Header Menu -->

    <!-- begin:: Header Topbar -->
    <div class="kt-header__topbar">

        <!--begin: Search -->

        <!--begin: User Bar -->
        @if(auth()->check())
            <div class="kt-header__topbar-item kt-header__topbar-item--user">
                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
                    <div class="kt-header__topbar-user">
                        <span class="kt-header__topbar-welcome kt-hidden-mobile">Olá,</span>
                        <span class="kt-header__topbar-username kt-hidden-mobile">{{auth()->user()->name}}</span>
                        <img alt="Pic" class="kt-radius-100" src="{{asset('assets/media/users/300_25.jpg')}}" />
                    </div>
                </div>
                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
                    <!--begin: Head -->
                    <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                        <div class="kt-user-card__avatar">
                            <img class="kt-hidden" alt="Pic" src="{{asset('assets/media/users/300_25.jpg')}}" />
                            <span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">{{ substr(auth()->user()->name,0,1) }}</span>
                        </div>
                        <div class="kt-user-card__name">
                            {{auth()->user()->name}}
                        </div>
                    </div>
                    <!--end: Head -->

                    <!--begin: Navigation -->
                    <div class="kt-notification">
                        <a class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                <i class="flaticon2-calendar-3 kt-font-success"></i>
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-bold">
                                    Perfil
                                </div>
                                <div class="kt-notification__item-time">
                                    <span class="badge badge-{{ auth()->user()->is_admin == 1 ? 'warning' : 'primary' }} font-weight-bold">{{ auth()->user()->perfilText() }}</span>
                                </div>
                            </div>
                        </a>
                        <div class="kt-notification__custom kt-space-between">
                            <a href="#" target="_blank" class="btn btn-label btn-label-brand btn-sm btn-bold" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Sair</a>
                        </div>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>

                    <!--end: Navigation -->
                </div>
            </div>
        @else
            <div class="kt-header__topbar-item kt-header__topbar-item--user">
                <div class="kt-header__topbar-wrapper" data-offset="0px,0px">
                    <div class="kt-header__topbar-user">
                        <a href="{{route('login')}}" class="btn btn-label-success">Entrar</a>
                    </div>
                </div>
            </div>
        @endif
        <!--end: User Bar -->
    </div>

    <!-- end:: Header Topbar -->
</div>
<!-- end:: Header -->