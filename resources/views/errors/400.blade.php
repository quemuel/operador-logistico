@extends('layouts.app-errors')

@section('template_title')
    400 | Requisição Inválida
@endsection

@section('content')
    <article>
        <body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

            {{-- begin:: Page --}}
            <div class="kt-grid kt-grid--ver kt-grid--root kt-page">
                <div class="kt-grid__item kt-grid__item--fluid kt-grid  kt-error-v1" style="background-image: url({{asset('assets/media/error/bg1.jpg')}});">
                    <div class="kt-error-v1__container">                        
                        <h1 class="kt-error-v1__number">400</h1>
                        <p class="kt-error-v1__desc">
                            <strong>OOPS! Requisição inválida, verifique sua requisição e tente novamente!</strong>
                        </p>
                    </div>
                </div>
            </div>

            {{-- end:: Page --}}

            {{-- begin::Global Config(global config for global JS sciprts) --}}
            <script>
                var KTAppOptions = {
                    "colors": {
                        "state": {
                            "brand": "#2c77f4",
                            "light": "#ffffff",
                            "dark": "#282a3c",
                            "primary": "#5867dd",
                            "success": "#34bfa3",
                            "info": "#36a3f7",
                            "warning": "#ffb822",
                            "danger": "#fd3995"
                        },
                        "base": {
                            "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                            "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                        }
                    }
                };
            </script>

            {{-- end::Global Config --}}

            {{-- begin::Global Theme Bundle(used by all pages) --}}
            <script src="{{asset('assets/plugins/global/plugins.bundle.js')}}" type="text/javascript"></script>
            <script src="{{asset('assets/js/scripts.bundle.js')}}" type="text/javascript"></script>

            {{-- end::Global Theme Bundle --}}
        </body>
    </article>
@endsection