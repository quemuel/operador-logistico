
@extends('layouts.app')

@section('template_title')
    Operador Logístico
@endsection

@section('content')
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Vagas 
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Visualizar
                </a>
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <i class="la la-search pr-2"></i> Visualizar Saldo Vagas
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body"> 
                    <div class="kt-section">
                        <div class="kt-section__info">
                            <div class="row">
                                <div class="col-4">
                                    <label for="inputGroupSelect01">Período Agendamento</label>
                                    <select id="inputGroupSelect01" class="form-control kt-selectpicker kt-input" title="Selecione..." >
                                        @foreach($periodosEscala as  $pe)
                                            <option {{$pe->id == $periodoEscala->id ? "selected" : ""}} value="{{$pe->id}}" >{{$pe->data_inicial->format('d/m/Y')}} à {{$pe->data_final->format('d/m/Y')}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Data</label>
                                        <div class="input-group date">
                                            <input type="text" name="data" class="form-control" readonly placeholder="Selecione a data" id="kt_datepicker_2_modal" />
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="la la-calendar-check-o"></i>
                                                </span>
                                                <a id="btn-acessar-vagas" class="btn btn-outline-success" href="" type="button">Visualizar</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if ($data != null)
                            <div class="kt-section__content">
                                <div class="kt-section__content">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Praça</th>
                                                    <th>Área de Entrega</th>
                                                    <th>Turno</th>
                                                    <th>Qtd Vagas</th>
                                                    <th>Qtd Utilizadas</th>
                                                    <th>Saldo Vagas</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if ($saldoTurnosVagas != null && !$saldoTurnosVagas->isEmpty())
                                                    @foreach ($saldoTurnosVagas as $saldoTurnoVagas)    
                                                        <tr>
                                                            <td scope="row">{{$saldoTurnoVagas->areaEntrega->praca->descricao}}</td>
                                                            <td scope="row">{{$saldoTurnoVagas->areaEntrega->descricao}}</td>
                                                            <td scope="row">{{$saldoTurnoVagas->turno->descricao}}</td>
                                                            <td>{{$saldoTurnoVagas->qtd_vagas}}</td>
                                                            <td>{{$saldoTurnoVagas->qtd_utilizadas}}</td>
                                                            <td>{{$saldoTurnoVagas->saldo_vagas}}</td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="5">Não há vagas registradas</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>
<!-- end:: Content -->
@endsection

@section('template_fastload_js') 
    $(document).ready(function () {
        $('.kt-selectpicker').selectpicker();
        /*$('.kt-selectpicker').selectpicker({
            deselectAllText: 'Nenhum',
            selectAllText: 'Todos',
            countSelectedText: '{0} Itens selecionados'
        });*/
        $("#inputGroupSelect01").on('change', function(){
            window.location.href = "{{ route('vagas.geral') }}/" + $(this).val();
        }); 
        $('#kt_datepicker_2_modal').on('change', function() {
            $('#btn-acessar-vagas').attr({ href: "{{ route('vagas.geral', [$periodoEscala->id]) }}/" + moment($(this).val(), 'DD/MM/YYYY').format('YYYY-MM-DD')});
        });
        var arrows;
        if (KTUtil.isRTL()) {
            arrows = {
                leftArrow: '<i class="la la-angle-right"></i>',
                rightArrow: '<i class="la la-angle-left"></i>'
            }
        } else {
            arrows = {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        }
        $('#kt_datepicker_2_modal').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows,
            language: "pt-BR",
            startDate: moment("{{ $periodoEscala->data_inicial->format('Y-m-d') }}").toDate(),
            endDate: moment("{{ $periodoEscala->data_final->format('Y-m-d') }}").toDate()
        });
        $('#kt_datepicker_2_modal').datepicker('setDate', moment("{{$data}}").toDate());
    });
@endsection