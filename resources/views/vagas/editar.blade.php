@extends('layouts.app')

@section('template_title')
    Operador Logístico
@endsection

@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Turno Vagas 
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Atualize os campos
                </a>
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    @if ($errors->any())
        <div class="alert alert-danger d-flex align-items-center py-4" role="alert">
            <span class="alert-icon icon-help-button lead-3 pr-3"></span>
            <strong>Erros!</strong> 
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
            <span class="c-pointer ml-auto" data-dismiss="alert">
            <span class="fas fa-times lead-1" aria-hidden="true"></span>
            </span>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-danger fade show" role="alert">
            <div class="alert-icon"><i class="la la-close"></i></div>
            <div class="alert-text">{{ $message }}</div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <i class="kt-menu__link-icon flaticon-calendar-with-a-clock-time-tools"></i>  Atualizar Turno Vagas
                        </h3>
                    </div>
                </div>
                <form method="post" class="kt-form" action="{{route('vagas.atualizar', [$periodoEscala->id, $areaEntrega->id])}}">
                    @csrf
                    @method('PUT')
                    <div class="kt-portlet__body">
                        <div class="row">
                            @foreach ($turnosVagas as $turnoVagas)
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>{{$turnoVagas->turno->descricao}}</label>
                                        <input name="turnoVagas[{{$turnoVagas->turno_id}}]" type='text' class="form-control" value="{{ $turnoVagas->qtd_vagas }}" />
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" class="btn btn-primary">Atualizar</button>
                            <a href="{{route('vagas.listagem', [$periodoEscala->id])}}" class="btn btn-secondary">Voltar</a>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>
<!-- end:: Content -->
@endsection
@section('template_fastload_js') 
    $(document).ready(function () {
        $('#kt_daterangepicker_dashboard').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            "locale": {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "Aplicar",
                "cancelLabel": "Cancelar",
                "daysOfWeek": [
                  "Dom",
                  "Seg",
                  "Ter",
                  "Qua",
                  "Qui",
                  "Sex",
                  "Sab"
                ],
                "monthNames": [
                  "Janeiro",	
                  "Fevereiro",
                  "Março",
                  "Abril",
                  "Maio",
                  "Junho",
                  "Julho",
                  "Agosto",
                  "Setembro",
                  "Outubro",
                  "Novembro",
                  "Dezembro"
                ],
                "firstDay": 1
              }
        }, function(start, end, label) {
            $('#kt_daterangepicker_dashboard .form-control').val( start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        });
    });
@endsection