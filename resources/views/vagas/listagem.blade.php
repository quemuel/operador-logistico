@extends('layouts.app')

@section('template_title')
    Operador Logístico
@endsection

@section('template_linked_css')
    <link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('template_fastload_css')
    #toast-container > div {
        opacity: 1;
    }
@endsection

@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Turno Vagas
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <a class="kt-subheader__breadcrumbs-link">
                    Listagem                   
                </a>
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    @if ($message = Session::get('error'))
        <div class="alert alert-danger fade show" role="alert">
            <div class="alert-icon"><i class="la la-close"></i></div>
            <div class="alert-text">{{ $message }}</div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>
    @endif
    @if ($message = Session::get('success'))
        <div class="alert alert-success fade show" role="alert">
            <div class="alert-icon"><i class="la la-check-square"></i></div>
            <div class="alert-text">{{ $message }}</div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <i class="kt-menu__link-icon flaticon-list-3"></i> Turno Vagas
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body pt-3">
                    <div class="kt-section">
                        <div class="kt-section__info">
                            <div class="row">
                                <div class="col-4">
                                    <label for="inputGroupSelect01">Período Agendamento</label>
                                    <select id="inputGroupSelect01" class="form-control kt-selectpicker kt-input" title="Selecione..." >
                                        @foreach($periodosEscala as  $pe)
                                            <option {{$pe->id == $periodoEscala->id ? "selected" : ""}} value="{{$pe->id}}" >{{$pe->data_inicial->format('d/m/Y')}} à {{$pe->data_final->format('d/m/Y')}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="kt-section__content">
                            <table class="table table-bordered table-hover table-checkable data-table-dados">
                                <thead>
                                    <tr>
                                        <th>Praça</th>
                                        <th>Período Agendamento</th>
                                        <th>Area Entrega</th>
                                        <th>Opções</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>
<!-- end:: Content -->
@endsection

@section('footer_scripts')
    <script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
@endsection

@section('template_fastload_js') 
    $(document).ready(function () {
        var tabela = $('.data-table-dados').DataTable({
			responsive: true,
			processing: true,
            serverSide: true,
            "lengthMenu": [[5, 15, 35, 65, 100, -1], [5, 15, 35, 65, 100, "Todos"]],
            "pagingType": "full",
            "searching": false,
            "paging": false,
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.25/i18n/Portuguese-Brasil.json"
            },
            ajax: "<?php echo route('vagas.dados', [$periodoEscala ? $periodoEscala->id : null])?>",
            columns: [
                {data: 'praca', name: 'praca', responsivePriority: 1, orderable: false},
                {data: 'periodoEscala', name: 'periodoEscala', responsivePriority: 2, orderable: false},
                {data: 'areaEntrega', name: 'areaEntrega', responsivePriority: 3, orderable: false},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
        $('.kt-selectpicker').selectpicker();
        $("#inputGroupSelect01").on('change', function(){
            window.location.href = "{{ route('vagas.listagem') }}/" + $(this).val();
        });
    });
@endsection