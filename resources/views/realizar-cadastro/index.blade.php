@extends('layouts.app-guest')

@section('template_title')
    Cadastrar Entregador
@endsection

@section('template_fastload_css')
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}
th
{
    background-color: #ebedf2;
    color: #8c8f9c;
    z-index: 1;
}
th:first-child, td:first-child
{
    position:sticky;
    left:0px; 
}
td:first-child
{
    background-color:grey;
}
@endsection

@section('content')
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Realizar Cadastro 
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Preencha os campos
                </a>
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col">
            <div class="alert alert-solid-danger alert-elevate alert-bold fade show" role="alert">
                <div class="alert-icon pb-2"><i class="fa fa-exclamation-triangle"></i></div>
                <!--alert-light <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>-->
                <div class="alert-text">
                    Para realização do cadastro é necessário que você tenha o numero de Microempreendedor Individual - MEI.
                </div>
                <div class="alert-close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="la la-close"></i></span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <i class="la la-user pr-2"></i> Dados pessoais
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form">
                    <div class="kt-portlet__body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>E-mail</label>
                                    <input type="email" class="form-control" aria-describedby="emailHelp" placeholder="Entre com e-mail">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Nome Completo</label>
                                    <input type="text" class="form-control" placeholder="Entre com seu nome completo">
                                </div>
                            </div>
                        </div>                            
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>MEI</label>
                                    <input class="form-control" type="number" placeholder="0000000000000">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Telefone</label>
                                    <input type="text" class="form-control" placeholder="(00) 00000-0000">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__head" style="border-top: 1px solid #ebedf2;">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                <i class="la la-motorcycle pr-2"></i> Dados do Serviço
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="row">
                            <div class="col-md-12"> 
                                <div class="table-responsive">                               
                                    <table class="table">
                                        <thead class="thead-light">
                                            <tr>
                                                <th>Modalidade</th>
                                                <th class="kt-align-center">Segunda</th>
                                                <th class="kt-align-center">Terça</th>
                                                <th class="kt-align-center">Quarta</th>
                                                <th class="kt-align-center">Quinta</th>
                                                <th class="kt-align-center">Sexta</th>
                                                <th class="kt-align-center">Sabado</th>
                                                <th class="kt-align-center">Domingo</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">MANHÃ DEDICADO AMARALINA 07:00 AS 11:00</th>
                                                <td class="kt-align-center">
                                                    <label class="kt-checkbox kt-checkbox--success">
                                                        <input type="checkbox">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td class="kt-align-center">
                                                    <label class="kt-checkbox kt-checkbox--success">
                                                        <input type="checkbox">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td class="kt-align-center">
                                                    <label class="kt-checkbox kt-checkbox--success">
                                                        <input type="checkbox">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td class="kt-align-center">
                                                    <label class="kt-checkbox kt-checkbox--success">
                                                        <input type="checkbox">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td class="kt-align-center">
                                                    <label class="kt-checkbox kt-checkbox--success">
                                                        <input type="checkbox">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td class="kt-align-center">
                                                    <label class="kt-checkbox kt-checkbox--success">
                                                        <input type="checkbox">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td class="kt-align-center">
                                                    <label class="kt-checkbox kt-checkbox--success">
                                                        <input type="checkbox">
                                                        <span></span>
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">MANHÃ REGULAR 07:00 AS 10:30</th>
                                                <td class="kt-align-center">
                                                    <label class="kt-checkbox kt-checkbox--success">
                                                        <input type="checkbox">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td class="kt-align-center">
                                                    <label class="kt-checkbox kt-checkbox--success">
                                                        <input type="checkbox">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td class="kt-align-center">
                                                    <label class="kt-checkbox kt-checkbox--success">
                                                        <input type="checkbox">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td class="kt-align-center">
                                                    <label class="kt-checkbox kt-checkbox--success">
                                                        <input type="checkbox">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td class="kt-align-center">
                                                    <label class="kt-checkbox kt-checkbox--success">
                                                        <input type="checkbox">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td class="kt-align-center">
                                                    <label class="kt-checkbox kt-checkbox--success">
                                                        <input type="checkbox">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td class="kt-align-center">
                                                    <label class="kt-checkbox kt-checkbox--success">
                                                        <input type="checkbox">
                                                        <span></span>
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">ALMOÇO DEDICADO 11:00 AS 14:00</th>
                                                <td class="kt-align-center">
                                                    <label class="kt-checkbox kt-checkbox--success">
                                                        <input type="checkbox">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td class="kt-align-center">
                                                    <label class="kt-checkbox kt-checkbox--success">
                                                        <input type="checkbox">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td class="kt-align-center">
                                                    <label class="kt-checkbox kt-checkbox--success">
                                                        <input type="checkbox">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td class="kt-align-center">
                                                    <label class="kt-checkbox kt-checkbox--success">
                                                        <input type="checkbox">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td class="kt-align-center">
                                                    <label class="kt-checkbox kt-checkbox--success">
                                                        <input type="checkbox">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td class="kt-align-center">
                                                    <label class="kt-checkbox kt-checkbox--success">
                                                        <input type="checkbox">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td class="kt-align-center">
                                                    <label class="kt-checkbox kt-checkbox--success">
                                                        <input type="checkbox">
                                                        <span></span>
                                                    </label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-md-12">  
                                <div class="form-group">
                                    <label class="kt-font-bold">Áreas de Entrega</label>
                                    <div class="kt-radio-list">
                                        <label class="kt-radio kt-radio--success">
                                            <input type="radio" name="radio3"> Salvador - Sudoeste (OL Dedicado)
                                            <span></span>
                                        </label>
                                        <label class="kt-radio kt-radio--success">
                                            <input type="radio" name="radio3"> Salvador - Amaralina (OL Dedicado)
                                            <span></span>
                                        </label>
                                        <label class="kt-radio kt-radio--success">
                                            <input type="radio" name="radio3"> Salvador - Pituaçu (OL Dedicado)
                                            <span></span>
                                        </label>
                                        <label class="kt-radio kt-radio--success">
                                            <input type="radio" name="radio3"> Salvador - Jaguaribe (OL Dedicado)
                                            <span></span>
                                        </label>
                                        <label class="kt-radio kt-radio--success">
                                            <input type="radio" name="radio3"> Salvador - Vilas (OL Dedicado)
                                            <span></span>
                                        </label>
                                        <label class="kt-radio kt-radio--success">
                                            <input type="radio" name="radio3"> Regular
                                            <span></span>
                                        </label>
                                    </div>
                                </div>     
                            </div>
                        </div>                
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="reset" class="btn btn-success">Cadastrar</button>
                            <a href="{{url("/")}}" class="btn btn-secondary">Voltar</a>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>
<!-- end:: Content -->

@endsection