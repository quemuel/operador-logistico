@extends('layouts.app')

@section('template_title')
    Importar dados
@endsection

@section('content')
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Importar Dados 
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Faça o upload do arquivo
                </a>
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    @if (\Session::has('error'))
        <div class="alert alert-danger">
            <ul>
                <li>{!! \Session::get('error') !!}</li>
            </ul>
        </div>
    @endif
    @if (\Session::has('success'))
        <div class="alert alert-success">
            <ul>
                <li>{!! \Session::get('success') !!}</li>
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <i class="la la-upload pr-2"></i> Importar
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="{{ route('importar-arquivo') }}" method="post" enctype="multipart/form-data">
                    <div class="kt-portlet__body">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-8 col-lg-8">
                                <label for="operador_logico">Operador Lógico</label>
                                <select class="form-control" id="operador_logico" name="operador_logico">
                                    <option selected value="SID MOTOS">Sid Motos</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="data_inicial">Data Inicial</label>
                                    <input id="data_inicial" name="data_inicial" required class="form-control" type="date" value="2011-08-19" id="example-date-input">
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="data_final">Data Final</label>
                                    <input id="data_final" name="data_final" required class="form-control" type="date" value="2011-08-19" id="example-date-input">
                                </div>
                            </div>
                        </div>                    
                        <div class="form-group row">
                            <div class="col-md-4 col-lg-4">
                                <label>Upload de arquivo</label>
                                <input class="form-control" multiple="multiple" required type="file" name="files[]">
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn-importar" class="btn btn-success">Importar</button>
                            <a href="{{url("/")}}" class="btn btn-secondary">Voltar</a>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    <div class="row">
        <div class="col-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <i class="la la-th-list pr-2"></i> Listagem
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">                    
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th colspan="2"><i class="la la-lastfm pr-2"></i> Sid-Motos</th>
                                        </tr>
                                    </thead>
                                    <thead>
                                        <tr class="table-success">
                                            <th>Data</th>
                                            <th>Qtd</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($tbDados as $tbDado)
                                            <tr>
                                                <th scope="row">{{ \Carbon\Carbon::parse($tbDado->data)->format('d/m/Y')}}</th>
                                                <td>{{$tbDado->qtd}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>
<!-- end:: Content -->

@endsection


@section('template_fastload_js') 
    $(document).ready(function () {
        $('#data_inicial').val(moment().subtract(1, 'days').format('YYYY-MM-DD'));
        $('#data_final').val(moment().subtract(1, 'days').format('YYYY-MM-DD'));
        $(".kt-form").submit(function (e) {
            //disable the submit button
            $("#btn-importar").attr("disabled", true);
            return true;
        });
    });
@endsection