@extends('layouts.app')

@section('template_title')
    Operador Logístico
@endsection

@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Dados Bancários 
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <a class="kt-subheader__breadcrumbs-link">
                    @if(base64_decode(request()->get('mn')) == "Salvador")         
                        <span class="badge badge-success"><i class="la la-check-square"></i> Salvador</span>
                    @else
                        <span class="badge badge-primary"><i class="la la-check-square"></i> Aracaju</span>     
                    @endif                    
                </a>
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    @if ($errors->any())
        <div class="alert alert-danger d-flex align-items-center py-4" role="alert">
            <span class="alert-icon icon-help-button lead-3 pr-3"></span>
            <strong>Erros!</strong> 
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
            <span class="c-pointer ml-auto" data-dismiss="alert">
            <span class="fas fa-times lead-1" aria-hidden="true"></span>
            </span>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-danger d-flex align-items-center py-4" role="alert">
            <span class="flaticon-warning pr-3"></span><strong>{{ $message }}</strong>
            <span class="c-pointer ml-auto" data-dismiss="alert">
            <span class="fas fa-times lead-1" aria-hidden="true"></span>
            </span>
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <i class="la la-bank pr-2"></i> Dados bancário
                        </h3>
                    </div>
                </div>
                <form method="post" class="kt-form" action="{{route('financeiro.store', ['mn' => request()->get('mn')])}}">
                    @csrf
                    <div class="kt-portlet__body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Nome completo</label>
                                    <input type="text" required class="form-control" name="nome" value="{{ old('nome') }}" placeholder="Digite seu nome completo">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>CPF</label>
                                    <input type="text" required class="form-control cpf" name="cpf" value="{{ old('cpf') }}" placeholder="000.000.000-00">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Pix</label>
                                    <input type="text" required class="form-control" name="pix" value="{{ old('pix') }}" placeholder="Insira o pix">
                                </div>
                            </div>
                        </div>                            
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Banco</label>
                                    <select required class="form-control kt-select2" id="kt_select2_1" name="banco_id">
                                        <option></option>
                                        @foreach ($bancos as $banco)
                                            @if ($banco->id == old('banco_id')))
                                                <option value="{{$banco->id}}" selected="selected">{{$banco->codigo}}-{{$banco->nome}}</option>
                                            @else
                                                <option value="{{$banco->id}}">{{$banco->codigo}}-{{$banco->nome}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Agencia</label>
                                    <input required class="form-control" type="number" name="agencia" value="{{ old('agencia') }}" placeholder="0000">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Conta com digito</label>
                                    <input type="text" required class="form-control conta" name="conta" value="{{ old('conta') }}" placeholder="000000-0">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" class="btn btn-success">Cadastrar</button>
                            <a href="{{url("/")}}" class="btn btn-secondary">Voltar</a>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>
<!-- end:: Content -->
@endsection
@section('template_fastload_js') 
    $(document).ready(function () {
        $('.cpf').mask('000.000.000-00');
        $('.conta').mask('000000000000-0', {reverse: true});
        $(".kt-form").submit(function() {
            $(".conta").unmask();
        });        
        $('#kt_select2_1').select2({
            placeholder: "Selecione o banco"
        });
    });
@endsection