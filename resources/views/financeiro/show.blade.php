@extends('layouts.app')

@section('template_title')
    Operador Logístico | Detalhes
@endsection

@section('template_linked_css')
<link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Dados Bancário
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <a class="kt-subheader__breadcrumbs-link">
                    Detalhe
                </a>
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-12">
            @if ($message = Session::get('success'))
                <div class="alert alert-success d-flex align-items-center py-4" role="alert">
                    <span class="fa fa-check pr-3"></span><strong>{{ $message }}</strong>
                    <span class="c-pointer ml-auto" data-dismiss="alert">
                    <span class="fas fa-times lead-1" aria-hidden="true"></span>
                    </span>
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="kt-portlet kt-portlet--height-fluid  kt-portlet--last">
                <div class="kt-portlet__head" style="border-bottom:0">
                    <div class="kt-portlet__head-label pt-4">
                        <h3 class="kt-portlet__head-title">
                            <i class="la la-bank pr-2"></i> Dados bancário
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body pt-0">
                    <div class="kt-widget16">
                        <div class="kt-widget16__items">
                            <div class="kt-widget16__item">
                                <span class="kt-widget16__date kt-font-bold">
                                    Nome Completo
                                </span>
                                <span class="kt-widget16__price  kt-font-success">
                                    {{$dadosBancario->nome}}
                                </span>
                            </div>
                            <div class="kt-widget16__item">
                                <span class="kt-widget16__date">
                                    CPF
                                </span>
                                <span class="kt-widget16__price  kt-font-brand">
                                    {{$dadosBancario->cpf}}
                                </span>
                            </div>
                            <div class="kt-widget16__item">
                                <span class="kt-widget16__date">
                                    Pix
                                </span>
                                <span class="kt-widget16__price  kt-font-brand">
                                    {{$dadosBancario->pix}}
                                </span>
                            </div>
                            <div class="kt-widget16__item">
                                <span class="kt-widget16__date">
                                    Banco
                                </span>
                                <span class="kt-widget16__price  kt-font-brand">
                                    {{$dadosBancario->banco->nome}}
                                </span>
                            </div>
                            <div class="kt-widget16__item">
                                <span class="kt-widget16__date">
                                    Agência
                                </span>
                                <span class="kt-widget16__price kt-font-brand">
                                    {{$dadosBancario->agencia}}
                                </span>
                            </div>
                            <div class="kt-widget16__item">
                                <span class="kt-widget16__date">
                                    Conta
                                </span>
                                <span class="kt-widget16__price kt-font-brand">
                                    {{substr($dadosBancario->conta, 0, strlen($dadosBancario->conta) - 1)."-".substr($dadosBancario->conta, strlen($dadosBancario->conta) - 1, 1)}}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Content -->
@endsection