@extends('layouts.app')

@section('template_title')
    Operador Logístico
@endsection

@section('template_linked_css')
    <link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('template_fastload_css')
    #toast-container > div {
        opacity: 1;
    }
@endsection

@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Dados Bancários 
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <a class="kt-subheader__breadcrumbs-link">
                    Listagem                   
                </a>
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    @if ($message = Session::get('error'))
        <div class="alert alert-danger fade show" role="alert">
            <div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
            <div class="alert-text">{{ $message }}</div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>
    @endif
    @if ($message = Session::get('success'))
        <div class="alert alert-success fade show" role="alert">
            <div class="alert-icon"><i class="flaticon2-checkmark"></i></div>
            <div class="alert-text">{{ $message }}</div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <i class="la la-bank pr-2"></i> Dados bancários
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body pt-3">
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <table class="table table-bordered table-hover table-checkable data-table-dados">
                                <thead>
                                    <tr>
                                        <th>Município</th>
                                        <th>Nome</th>
                                        <th>CPF</th>
                                        <th>Pix</th>
                                        <th>Banco</th>
                                        <th>Agência</th>
                                        <th>Conta</th>
                                        <th>Opções</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>
<div class="modal fade" id="confirm-deletion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="" method="POST" id="form-delete">
                @csrf
                @method('DELETE')
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirmar exclusão</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <p>Tem certeza que deseja excluir os dados da conta de <strong id="dados-delecao-text"></strong>?</p>  
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Excluir</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end:: Content -->
@endsection

@section('footer_scripts')
    <script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
@endsection

@section('template_fastload_js') 
    $(document).ready(function () {
        var tabela = $('.data-table-dados').DataTable({
			responsive: true,
			processing: true,
            serverSide: true,
            "lengthMenu": [[5, 15, 35, 65, 100, -1], [5, 15, 35, 65, 100, "Todos"]],
            "order": [[ 0, "desc" ], [ 1, "asc" ]],
            "pagingType": "full",
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.25/i18n/Portuguese-Brasil.json"
            },
            ajax: "<?php echo route('financeiro.dados')?>",
            "columnDefs": [
                /*{ responsivePriority: 1, targets: 0 },
                { responsivePriority: 2, targets: -1 },
                { responsivePriority: 3, targets: -2 },*/
                { "targets": [2,3,4,5,6], "searchable": false },
            ],
            columns: [
                {data: 'municipio', name: 'municipio', responsivePriority: 1},
                {data: 'nome', name: 'nome', responsivePriority: 2},
                {data: 'cpf', name: 'cpf'},
                {data: 'pix', name: 'pix', responsivePriority: 3},
                {data: 'banco', name: 'banco'},
                {data: 'agencia', name: 'agencia'},
                {data: 'conta', name: 'conta', render: function(data, type, row) {
                    let conta = data.toString();
                    return conta.toString().substr(0, conta.length-1) + "-" + conta.substr(conta.length-1, 1); 
                }},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });  
        $('.data-table-dados tbody').on('click', 'td, span.dtr-data', function () {
            //tabela.row( $(this).parents('tr') ).data();
            var data = null;
            if($(this).parent().prop("tagName") == 'LI') {
                var row = $(this).parent().data('dt-row');
                var column = $(this).parent().data('dt-column');
                data = tabela.cell(row,column).data();
            } else {
                data = tabela.cell(this).data();
            }
            if(data !== undefined){
                criandoElementoCopiar(data);
                    toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-center",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "1500",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
                toastr.success("<b>"+data + "</b> copiado!");
            }
        });
        $('.delete').on('click', function() {
        });
    });
    function criandoElementoCopiar(dado) {
        var dummy = document.createElement("input");
        document.body.appendChild(dummy);
        dummy.setAttribute("id", "dummy_id");
        document.getElementById("dummy_id").value=dado;
        dummy.select();
        document.execCommand("copy");
        document.body.removeChild(dummy);
    }
    function proccessDeletion(element) {
        $(element).data('key');
        $('#dados-delecao-text').html($(element).data('name'));
        $('#form-delete').attr('action', $(element).data('url'));
        $('#confirm-deletion').modal('show');
    }
@endsection