<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

	{{-- begin::Head --}}
	<head>
		<base href="../../../">
		<meta charset="utf-8" />
		<title>@hasSection('template_title')@yield('template_title') | @endif {{ config('app.name', Lang::get('titles.app')) }}</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		{{-- CSRF Token --}}
		<meta name="csrf-token" content="{{ csrf_token() }}">

		{{-- begin::Fonts --}}
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

		{{-- Fonts --}}
		@yield('template_linked_fonts')
		{{-- end::Fonts --}}

		{{-- begin::Page Custom Styles(used by this page) --}}
		<link href="{{asset('assets/css/pages/error/error-1.css')}}" rel="stylesheet" type="text/css" />

		{{-- end::Page Custom Styles --}}

		{{-- begin::Global Theme Styles(used by all pages) --}}
		<link href="{{asset('assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/css/style.bundle.css" rel="stylesheet')}}" type="text/css" />
		<link rel="shortcut icon" href="{{asset('assets/media/logos/favicon.ico')}}" />
		@yield('template_linked_css')
		<style type="text/css">
			@yield('template_fastload_css')
		</style>
		{{-- end::Global Theme Styles --}}

		{{-- begin::Layout Skins(used by all pages) --}}

		{{-- end::Layout Skins --}}

		{{-- Scripts --}}
		<script>
			window.Laravel = {!! json_encode([
				'csrfToken' => csrf_token(),
			]) !!};
		</script>
		@yield('head')
	</head>
	@yield('content')