<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="UTF-8" />
		<title>@hasSection('template_title')@yield('template_title') | @endif {{ config('app.name', Lang::get('titles.app')) }}</title>
		<meta name="description" content="Login page example">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		{{-- CSRF Token --}}
		<meta name="csrf-token" content="{{ csrf_token() }}">

		{{--begin::Fonts --}}
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
		{{--end::Fonts --}}

		{{-- Fonts --}}
		@yield('template_linked_fonts')
		{{-- begin::Page Custom Styles(used by this page)  --}}
		<link href="{{asset('assets/css/pages/login/login-1.css')}}" rel="stylesheet" type="text/css" />

		{{-- end::Page Custom Styles  --}}

		{{-- begin::Global Theme Styles(used by all pages)  --}}
		<link href="{{asset('assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />

		{{-- end::Global Theme Styles  --}}

		{{-- begin::Layout Skins(used by all pages)  --}}

		{{-- end::Layout Skins  --}}
		<link rel="shortcut icon" href="{{asset('assets/media/logos/favicon.ico')}}" />
		
		@yield('template_linked_css')

		<style type="text/css">
			@yield('template_fastload_css')
		</style>

		{{-- Scripts --}}
		<script>
			window.Laravel = {!! json_encode([
				'csrfToken' => csrf_token(),
			]) !!};
		</script>

		@yield('head')
	</head>

	{{--  end::Head  --}}

	{{--  begin::Body  --}}
	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

		{{--  begin:: Page  --}}
		<div class="kt-grid kt-grid--ver kt-grid--root kt-page">
			<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v1" id="kt_login">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
					@yield('content')
				</div>
			</div>
		</div>

		{{--  end:: Page  --}}

		{{--  begin::Global Config(global config for global JS sciprts)  --}}
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#2c77f4",
						"light": "#ffffff",
						"dark": "#282a3c",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>

		{{--  end::Global Config  --}}

		{{-- begin::Global Theme Bundle(used by all pages)  --}}
		<script src="{{asset('assets/plugins/global/plugins.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/js/scripts.bundle.js')}}" type="text/javascript"></script>

		{{-- end::Global Theme Bundle  --}}

		{{-- begin::Page Scripts(used by this page)  --}}
		<script src="{{asset('assets/js/pages/custom/login/login-1.js')}}" type="text/javascript"></script>

		{{-- end::Page Scripts  --}}
	</body>

	{{--  end::Body  --}}
</html>