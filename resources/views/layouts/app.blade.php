<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title>@hasSection('template_title')@yield('template_title') | @endif {{ config('app.name', Lang::get('titles.app')) }}</title>
		<meta name="description" content="Sistema para operação logistica " />
		<meta name="keywords" content="ifood, operador logistico, motoby, entrega, sid, motos" />
		
		{{-- CSRF Token --}}
		<meta name="csrf-token" content="{{ csrf_token() }}">
			
		{{-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --}}
		{{--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]--}}

		{{--begin::Fonts --}}
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

		{{--end::Fonts --}}

		{{-- Fonts --}}
		@yield('template_linked_fonts')


		{{--begin::Page Custom Styles(used by this page) --}}
		<link href="{{asset('assets/css/pages/support-center/home-1.css')}}" rel="stylesheet" type="text/css" />

		{{--end::Page Custom Styles --}}

		{{--begin::Global Theme Styles(used by all pages) --}}
		<link href="{{asset('assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />

		<link rel="shortcut icon" href="{{asset('assets/media/logos/favicon.ico')}}" />

		@yield('template_linked_css')

		{!! "<style type='text/css'>" !!} 
			@yield('template_fastload_css') 
		{!! "</style>" !!}	

		{{-- Scripts --}}
		<script>
			window.Laravel = {!! json_encode([
				'csrfToken' => csrf_token(),
			]) !!};
		</script>

		@yield('head')
	</head>

	{{-- begin::Body --}}
	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

		{{-- begin:: Page --}}

		{{-- begin:: Header Mobile --}}
		@include('includes.header-mobile')
		{{-- end:: Header Mobile --}}

		<div class="kt-grid kt-grid--hor kt-grid--root">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

				{{-- begin:: Aside --}}
				<button class="kt-aside-close" id="kt_aside_close_btn"><i class="la la-close"></i></button>
				<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

					{{-- begin:: Aside --}}
					<div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
						<div class="kt-aside__brand-logo">
							<a href="{{ route('dashboard') }}">
								<img alt="Logo" src="{{asset('assets/media/logos/logo-12.png')}}">
							</a>
						</div>
						<div class="kt-aside__brand-tools">
							<button class="kt-aside__brand-aside-toggler" id="kt_aside_toggler"><span></span></button>
						</div>
					</div>
					{{-- end:: Aside --}}

					{{-- begin:: Aside Menu --}}
					@include('includes.aside-menu')
					{{-- end:: Aside Menu --}}
				</div>
				{{-- end:: Aside --}}

				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

					{{-- begin:: Header --}}
					<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

						{{-- begin: Header Menu --}}
						@include('includes.header-menu')						
						{{-- end: Header Menu --}}

						{{-- begin: Header Topbar --}}
						@include('includes.header-topbar')						
						{{-- end: Header Topbar --}}
					</div>
					{{-- end:: Header --}}
					
					<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
						{{-- begin:: Content --}}
						@yield('content')
						{{-- end:: Content --}}
					</div>

					{{-- begin:: Footer --}}
					@include('includes.footer')
					{{-- end:: Footer --}}
				</div>
			</div>
		</div>		
		{{-- end:: Page --}}

		{{-- begin::Scrolltop --}}
		<div id="kt_scrolltop" class="kt-scrolltop">
			<i class="fa fa-arrow-up"></i>
		</div>
		{{-- end::Scrolltop --}}

		{{-- begin::Global Config(global config for global JS sciprts) --}}
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#2c77f4",
						"light": "#ffffff",
						"dark": "#282a3c",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>

		{{-- end::Global Config --}}

		{{--begin::Global Theme Bundle(used by all pages) --}}
		<script src="{{asset('assets/plugins/global/plugins.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/js/scripts.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/plugins/custom/jquery-mask-plugin/jquery.mask.min.js')}}" type="text/javascript"></script>

		{{--end::Global Theme Bundle --}}

		{{--begin::Page Vendors(used by this page) --}}
		<script src="{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
		
		{{--end::Page Vendors --}}

		{{--begin::Page Scripts(used by this page) --}}
		<script src="{{asset('assets/js/pages/dashboard.js')}}" type="text/javascript"></script>
		@yield('footer_scripts')
		{{--end::Page Scripts --}}
		
		<script type="text/javascript">
			@yield('template_fastload_js')
		</script>
	</body>
	{{-- end::Body --}}
</html>
