
@extends('layouts.app')

@section('template_title')
    Operador Logístico
@endsection

@section('content')
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Visualizar Agendamento 
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Preencha os campos
                </a>
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col">
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <div class="kt-widget kt-widget--user-profile-3">
                        <div class="kt-widget__top">
                            <div class="kt-widget__content">
                                <div class="kt-widget__head">
                                    <a href="#" class="kt-widget__username">
                                        Informações de agendamento
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="kt-widget__bottom">
                            <div class="kt-widget__item">
                                <div class="kt-widget__icon">
                                    <i class="flaticon-confetti"></i>
                                </div>
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Praça</span>
                                    <span class="kt-widget__value"><span></span>{{$praca->descricao}}</span>
                                </div>
                            </div>
                            <div class="kt-widget__item">
                                <div class="kt-widget__icon">
                                    <i class="flaticon-pie-chart"></i>
                                </div>
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Área de Entrega</span>
                                    <span class="kt-widget__value"><span></span>{{$areaEntrega->descricao}}{{$areaEntrega->is_dedicado ? " (Dedicado)" : ""}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <i class="la la-user pr-2"></i> Dados pessoais
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form method="POST" class="kt-form" action="{{$escala != null ? route('escala.store-atualizar-agendamento') : route('escala.store-agendar') }}" novalidate="novalidate">
                    @csrf
                    <div class="kt-portlet__body"> 
                        @if ($entregador == null && $escala == null)
                        @else     
                            <div class="row" >
                                <div class="col-md-6">
                                    <div class="kt-widget12">
                                        <div class="kt-widget12__content">
                                            <div class="kt-widget12__item">
                                                <div class="kt-widget12__info">
                                                    <span class="kt-widget12__desc">Nome</span>
                                                    <span class="kt-widget12__value">{{$entregador->nome_completo}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif 
                    </div>
                    @if ($entregador != null)
                        <div class="kt-portlet__head" style="border-top: 1px solid #ebedf2;">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    <i class="kt-menu__link-icon flaticon-calendar-with-a-clock-time-tools"></i> Agendamento
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <div class="row">
                                <div class="col-md-12"> 
                                    <div class="table-responsive">                               
                                        <table class="table">
                                            <thead class="thead-light">
                                                <tr>
                                                    @foreach ($tabelaEscala["cabecario"] as $ctc)
                                                        <th class="{{$tabelaEscala['cabecario'][0] == $ctc ? '' : 'kt-align-center'}}">{{$ctc}}</th>                                                    
                                                    @endforeach
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($tabelaEscala["corpo"] as $ct)
                                                    <tr>
                                                        <th scope="row">{{$ct->turno->descricao}}</th>
                                                        @foreach ($ct->dias as $dia)
                                                            <td class="kt-align-center">
                                                                <label class="kt-checkbox kt-checkbox--bold {{$dia->exibir ? "kt-checkbox--primary" : ""}}">
                                                                    <i class="la {{$dia->ativo ? 'la-check-square text-success font-weight-bold' : 'la-minus-square kt-shape-font-color-2'}} la-2x"></i>
                                                                </label>
                                                            </td>                                                   
                                                        @endforeach
                                                    </tr>                                                   
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>               
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <a href="{{route('escala.por-entregador')}}" class="btn btn-secondary">Voltar</a>
                            </div>
                        </div>
                    @endif   
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>
<!-- end:: Content -->
@endsection