
@extends( Auth::guest() ? 'layouts.app-guest' : 'layouts.app')

@section('template_title')
    Operador Logístico
@endsection

@section('template_linked_css')
    <link href="https://fonts.googleapis.com/css2?family=Orbitron&display=swap" rel="stylesheet">
@endsection

@section('content')
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Agendar 
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Preencha os campos
                </a>
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    @if ($message = Session::get('error'))
        <div class="alert alert-danger fade show" role="alert">
            <div class="alert-icon"><i class="la la-close"></i></div>
            <div class="alert-text">{{ $message }}</div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>
    @endif
    @if ($message = Session::get('success'))
        <div class="alert alert-success fade show" role="alert">
            <div class="alert-icon"><i class="la la-check-square"></i></div>
            <div class="alert-text">{{ $message }}</div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>
    @endif
    @if (!$requisicaoValida["valida"])
        <div class="row">
            <div class="col">
                <div class="alert alert-solid-danger alert-elevate alert-bold fade show" role="alert">
                    <div class="alert-icon pb-2"><i class="fa fa-exclamation-triangle"></i></div>
                    <!--alert-light <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>-->
                    <div class="alert-text">
                        {{$requisicaoValida["mensagem"]}}
                    </div>
                    <div class="alert-close">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="la la-close"></i></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                @if (Auth::guest())
                    <a href="{{route('escala.agendar')}}/{{$periodoEscala->codigo}}/{{$areaEntrega->codigo}}" class="btn btn-secondary">Voltar</a>
                @else
                    <a href="{{route('escala.por-entregador')}}" class="btn btn-secondary">Voltar</a>
                @endif
            </div>
        </div>
    @else
        <div class="row">
            <div class="col">
                <div class="kt-portlet">
                    <div class="kt-portlet__head kt-portlet__head--right kt-portlet__head--noborder  kt-ribbon kt-ribbon--clip kt-ribbon--left kt-ribbon--info">
                        <div class="kt-ribbon__target" style="top: 12px;">
                            <span class="kt-ribbon__inner"></span>
                            <i class="fa fa-clock icon-clock"></i> &nbsp;&nbsp;&nbsp;
                            <h2 class="clock" id="timer"></h2>
                        </div>
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                TEMPO LIMITE PARA AGENDAMENTO
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body kt-portlet__body--fit-top">
                        <br>
                        <div id="myAlert" class="alert alert-danger fade show" role="alert">
                            <div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
                            <div class="alert-text">Seu tempo de agendamento esgotou!</div>
                            <div class="alert-close">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                </button>
                            </div>
                        </div>
                        
                        <div class="alert alert-outline-dark fade show" role="alert">
                            <div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
                            <div class="alert-text">Realize o <code class="font-weight-bold">agendamento de todos os dias</code> da semana!</div>
                            <div class="alert-close">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">

                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                <i class="flaticon-event-calendar-symbol pr-2"></i> Dados agendamento
                            </h3>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form method="POST" class="kt-form" action="{{$escala != null ? route('escala.store-atualizar-agendamento') : route('escala.store-agendar') }}" novalidate="novalidate">
                        @csrf
                        <div class="kt-portlet__body"> 
                            <div class="kt-widget kt-widget--user-profile-3">
                                <div class="kt-widget__bottom">
                                    @if ($entregador != null)
                                        <div class="kt-widget__item">
                                            <div class="kt-widget__icon">
                                                <i class="flaticon-avatar"></i>
                                            </div>
                                            <div class="kt-widget__details">
                                                <span class="kt-widget__title">Nome</span>
                                                <span class="kt-widget__value"><span></span>{{$entregador->nome_completo}}</span>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="kt-widget__item">
                                        <div class="kt-widget__icon">
                                            <i class="flaticon-confetti"></i>
                                        </div>
                                        <div class="kt-widget__details">
                                            <span class="kt-widget__title">Praça</span>
                                            <span class="kt-widget__value"><span></span>{{$praca->descricao}}</span>
                                        </div>
                                    </div>
                                    <div class="kt-widget__item">
                                        <div class="kt-widget__icon">
                                            <i class="flaticon-pie-chart"></i>
                                        </div>
                                        <div class="kt-widget__details">
                                            <span class="kt-widget__title">Área de Entrega</span>
                                            <span class="kt-widget__value"><span></span>{{$areaEntrega->descricao}}{{$areaEntrega->is_dedicado ? " (Dedicado)" : ""}}</span>
                                        </div>
                                    </div>
                                    <div class="kt-widget__item">
                                        <div class="kt-widget__icon">
                                            <i class="flaticon2-time"></i>
                                        </div>
                                        <div class="kt-widget__details">
                                            <span class="kt-widget__title">Período Agendamento</span>
                                            <span class="kt-widget__value"><span></span>{{$periodoEscala->data_inicial->format('d/m/Y') . " à " . $periodoEscala->data_final->format('d/m/Y') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if ($entregador == null && $escala == null)  
                                <br><br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <!-- height: 38px; -->
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" for="inputGroupSelect01">CPF</label>
                                                </div>
                                                <input type="text" name="cpf" class="form-control cpf" value='{{$entregador ? $entregador->cpf : ""}}' placeholder="000.000.000-00">
                                                <input type="hidden" name="codigo" value='{{$periodoEscala->codigo}}'>
                                                <input type="hidden" name="areaEntregaGuid" value='{{$areaEntrega->codigo}}'>
                                                <div class="input-group-append">
                                                    <a class="btn btn-outline-success btn-iniciar" href="" type="button">Iniciar Agendamento</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @else   
                                <input type="hidden" name="cpf" value='{{$entregador ? $entregador->cpf : ""}}'>
                                <input type="hidden" name="codigo" value='{{$periodoEscala->codigo}}'>
                                <input type="hidden" name="areaEntregaGuid" value='{{$areaEntrega->codigo}}'>
                            @endif 
                        </div>
                        @if ($entregador != null)
                            <div class="kt-portlet__head" style="border-top: 1px solid #ebedf2;">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        <i class="kt-menu__link-icon flaticon-calendar-with-a-clock-time-tools"></i> {{$escala != null ? "Atualizar" : "Cadastrar"}} Agendamento
                                    </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="row">
                                    <div class="col-md-12"> 
                                        <div class="table-responsive">                               
                                            <table class="table">
                                                <thead class="thead-light">
                                                    <tr>
                                                        @foreach ($tabelaEscala["cabecario"] as $ctc)
                                                            <th class="{{$tabelaEscala['cabecario'][0] == $ctc ? '' : 'kt-align-center'}}">{{$ctc}}</th>                                                    
                                                        @endforeach
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($tabelaEscala["corpo"] as $ct)
                                                        <tr>
                                                            <th scope="row">{{$ct->turno->descricao}}</th>
                                                            @foreach ($ct->dias as $dia)
                                                                <td class="kt-align-center" title="{{$dia->nome}}">
                                                                    @if ($escala != null)
                                                                        <label class="kt-checkbox kt-checkbox--bold {{$dia->exibir ? "kt-checkbox--primary" : ""}}">
                                                                            <input name="turnoDia[{{$ct->turno->id}}][]" value="{{$dia->id}}" {{$dia->exibir ? "" : "disabled"}} {{$dia->ativo ? "checked" : ""}} type="checkbox">
                                                                            <span></span>
                                                                        </label>
                                                                    @else
                                                                        <label class="kt-checkbox kt-checkbox--bold {{$dia->exibir ? "kt-checkbox--success" : ""}}">
                                                                            <input name="turnoDia[{{$ct->turno->id}}][]" value="{{$dia->id}}" {{$dia->exibir ? "" : "disabled"}} type="checkbox">
                                                                            <span></span>
                                                                        </label>
                                                                    @endif
                                                                </td>                                                   
                                                            @endforeach
                                                        </tr>                                                   
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>               
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <button type="submit" class="btn btn-{{$escala != null ? "primary" : "success"}}">{{$escala != null ? "Atualizar" : "Cadastrar"}}</button>
                                    @if (Auth::guest())
                                        <a href="{{route('escala.agendar')}}/{{$periodoEscala->codigo}}/{{$areaEntrega->codigo}}" class="btn btn-secondary">Voltar</a>
                                    @else
                                        <a href="{{route('escala.por-entregador')}}" class="btn btn-secondary">Voltar</a>
                                    @endif
                                </div>
                            </div>
                        @endif   
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    @endif
</div>
<!-- end:: Content -->
@endsection
@section('template_fastload_js') 
    @if ($requisicaoValida["valida"])
        $(document).ready(function () {
            $('.cpf').mask('000.000.000-00');
            
            $('.cpf').on('keyup', function(){
                $('.btn-iniciar').attr({ href: '{{route('escala.agendar')}}'+'/{{$periodoEscala->codigo}}/{{$areaEntrega->codigo}}/' + $(this).val() });
            });  
                  
        });
    @endif

     

    $('#myAlert').hide();
    function countdown() {
        var tempo = moment("{{ $periodoEscala->data_inicial->format('Y-m-d') }}").subtract(1, 'seconds').diff(moment(), 'seconds');
        tempo = tempo < 0 ? 0 : tempo; 
        //debugger;
        var duracao = moment.duration(tempo, 'seconds')
        , years, months, days, hours, minutes, seconds, horaImprimivel;

        setInterval(function() {      
            duracao = moment.duration(duracao - 1000, 'milliseconds');
            if(duracao <= 0) {
                $("#timer").html("00:00:00");
                $('#myAlert').show();
            } else {
                years = duracao.years();
                months = duracao.months();
                days = duracao.days();
                hours = duracao.hours();
                minutes = duracao.minutes();
                seconds = duracao.seconds();
                if (hours < 10) {
                    hours = "0" + hours;
                    hours = hours.substr(0, 2);
                }
                if (minutes < 10) {
                    minutes = "0" + minutes;
                    minutes = minutes.substr(0, 2);
                }
                if (seconds <= 9) {
                    seconds = "0" + seconds;
                }
                horaImprimivel = '';
                if (years) {
                    horaImprimivel = years + ' anos ';
                }
                if (months) {
                    horaImprimivel += months + ' meses ';
                }
                if (days) {
                    horaImprimivel += days + ' dias ';
                }
                horaImprimivel += hours + ':' + minutes + ':' + seconds;
                //JQuery pra setar o valor
                $("#timer").html(horaImprimivel);
            }
        }, 1000);
    }
    
    // Chama a função ao carregar a tela
    countdown();
@endsection
@section('template_fastload_css') 
    .kt-widget.kt-widget--user-profile-3 .kt-widget__bottom .kt-widget__item {
        padding: 0;
    }
    .kt-widget.kt-widget--user-profile-3 .kt-widget__bottom {
        border-top: 0;
        margin-top: 0;
    }
    .kt-ribbon .kt-ribbon__target {
        padding: 5px 47px;
    }
    .kt-ribbon--info .kt-ribbon__target > .kt-ribbon__inner{
        background-color: #000000;
    }
    .clock {
        /* position: absolute; */
        /* transform: translateX(-50%) translateY(-50%); */
        /* font-size: 60px; */
        color: #17D4FE;
        padding-top: 5px;
        font-family: Orbitron;
        /* letter-spacing: 7px; */
        font-family: 'Orbitron', sans-serif;
    }
    .icon-clock {
        color: #17D4FE;
        font-size: 29px;
    }
@endsection