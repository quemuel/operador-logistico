@extends('layouts.app')

@section('template_title')
    Operador Logístico
@endsection

@section('template_linked_css')
    <link href="{{asset('assets/plugins/custom/kanban/kanban.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('template_fastload_css')
    #toast-container > div {
        opacity: 1;
    }
@endsection

@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Agendamentos 
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <a class="kt-subheader__breadcrumbs-link">
                    Visualizar Por Área                   
                </a>
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    @if ($message = Session::get('error'))
        <div class="alert alert-danger fade show" role="alert">
            <div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
            <div class="alert-text">{{ $message }}</div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>
    @endif
    @if ($message = Session::get('success'))
        <div class="alert alert-success fade show" role="alert">
            <div class="alert-icon"><i class="flaticon2-checkmark"></i></div>
            <div class="alert-text">{{ $message }}</div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>
    @endif
    @if (!$data)
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Agendamento Por Área
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="row" >
                            <div class="col-md-6">
                                <div class="kt-widget12">
                                    <div class="kt-widget12__content">
                                        <div class="kt-widget12__item">
                                            <div class="kt-widget12__info">
                                                <span class="kt-widget12__desc">Area de Entrega</span>
                                                <span class="kt-widget12__value">{{$areaEntrega->descricao}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Data</label>
                                    <div class="input-group date">
                                        <input type="text" name="data" class="form-control" readonly placeholder="Selecione a data" id="kt_datepicker_2_modal" />
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <a id="btn-acessar-escala" href="" class="btn btn-success">Acessar Escala</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Agendamento Por Área
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="row" >
                        <div class="col-md-6">
                            <div class="kt-widget12">
                                <div class="kt-widget12__content">
                                    <div class="kt-widget12__item">
                                        <div class="kt-widget12__info">
                                            <span class="kt-widget12__desc">Area de Entrega</span>
                                            <span class="kt-widget12__value">{{$areaEntrega->descricao}}</span>
                                        </div>
                                        <div class="kt-widget12__info">
                                            <span class="kt-widget12__desc">Data</span>
                                            <span class="kt-widget12__value">{{\Carbon\Carbon::parse($data)->format('d/m/Y')}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="kanban1"></div>
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <a href="{{route('escala.visualizar-por-area', [$praca->id, $periodoEscala->id, $areaEntrega->id])}}" class="btn btn-secondary">Voltar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
<!-- end:: Content -->
@endsection

@section('footer_scripts')
    <script src="{{asset('assets/plugins/custom/kanban/kanban.bundle.js')}}" type="text/javascript"></script>
@endsection

@section('template_fastload_js') 
    $(document).ready(function () {
        $('#kt_datepicker_2_modal').on('change', function() {
            $('#btn-acessar-escala').attr({ href: '{{route('escala.visualizar-por-area', [$praca->id, $periodoEscala->id, $areaEntrega->id])}}/' + moment($(this).val(), 'DD/MM/YYYY').format('YYYY-MM-DD')});
        });
        var arrows;
        if (KTUtil.isRTL()) {
            arrows = {
                leftArrow: '<i class="la la-angle-right"></i>',
                rightArrow: '<i class="la la-angle-left"></i>'
            }
        } else {
            arrows = {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        }
        $('#kt_datepicker_2_modal').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows,
            language: "pt-BR",
            startDate: moment("{{ $periodoEscala->data_inicial->format('Y-m-d') }}").toDate(),
            endDate: moment("{{ $periodoEscala->data_final->format('Y-m-d') }}").toDate()
        });
        var kanban1 = new jKanban({
			element:'#kanban1',
			gutter  : '0',
			boards  : {!!$turnosEntregadoresJson!!}
		});
    });
@endsection