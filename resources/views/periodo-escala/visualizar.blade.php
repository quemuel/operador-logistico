
@extends('layouts.app')

@section('template_title')
    Operador Logístico
@endsection

@section('content')
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Visualizar Período Agendamento 
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Preencha os campos
                </a>
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <i class="la la-search pr-2"></i> Visualizar Período Agendamento
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body"> 
                    <div class="row" >
                        <div class="col-md-12">
                            <div class="kt-widget12">
                                <div class="kt-widget12__content">
                                    <div class="kt-widget12__item">
                                        <div class="kt-widget12__info">
                                            <span class="kt-widget12__value">Período Agendamento</span>
                                            <span class="kt-widget12__desc">{{$periodoEscala->data_inicial->format('d/m/Y') . " à " . $periodoEscala->data_final->format('d/m/Y') }}</span>
                                        </div>
                                    </div>
                                    @foreach ($areasEntrega as $areaEntrega)
                                        @php ($url = route('escala.agendar') . "/{$periodoEscala->codigo}/{$areaEntrega->codigo}")
                                        <div class="kt-widget12__item">
                                            <div class="kt-widget12__info">
                                                <span class="kt-widget12__value">{{$areaEntrega->praca->descricao}} - {{$areaEntrega->descricao}}{{$areaEntrega->is_dedicado ? " (Dedicado)" : ""}}</span>
                                                <span class="kt-widget12__desc"><button type="button" value="{{$url}}" class="btn-copy btn btn-success btn-elevate btn-icon btn-sm"><i class="la la-copy"></i></button> {{$url}}</span>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <a href="{{route('periodoEscala.index')}}" class="btn btn-secondary">Voltar</a>
                    </div>
                </div>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>
<!-- end:: Content -->
@endsection
@section('template_fastload_js') 
    $(document).ready(function () {
        $('.btn-copy').on('click', function () {
            criandoElementoCopiar($(this).val());
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-center",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "1500",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            toastr.success("Url copiada com sucesso!");
        });
    });
    function criandoElementoCopiar(dado) {
        var dummy = document.createElement("input");
        document.body.appendChild(dummy);
        dummy.setAttribute("id", "dummy_id");
        document.getElementById("dummy_id").value=dado;
        dummy.select();
        document.execCommand("copy");
        document.body.removeChild(dummy);
    }
@endsection